package nurunabiyev.android.traddictive.feed;


import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import nurunabiyev.android.traddictive.Const;
import nurunabiyev.android.traddictive.R;
import nurunabiyev.android.traddictive.objects.Notification;
import nurunabiyev.android.traddictive.objects.User;
import nurunabiyev.android.traddictive.profile.ProfileActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class PeopleListFragment extends Fragment
{
    private static final String TAG = "peoplelistfragment";

    private static final String ARG_MY_UID_PARAM = "param1";
    private static final String ARG_OTHER_UID_PARAM = "param2";
    private static final String ARG_WHAT_TO_DISPLAY_PARAM = "param3";
    private static final String ARG_BUNDLE_PARAM = "param4";
    // view
    private RecyclerView mRecyclerView;
    private PeopleAdapter adapter;
    private TextView mNoItemsTV;
    private SwipeRefreshLayout mListSRL;
    private View mainView;
    // user
    private String whatToDisplay;
    private String myUid;
    private String otherUid;
    private Bundle mBundle;
    private User mUser;
    private DatabaseReference mDatabaseReference;
    private StorageReference mStorageReference;

    public PeopleListFragment()
    {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        mainView = inflater.inflate(R.layout.fragment_feed_list, container, false);

        mNoItemsTV = (TextView) mainView.findViewById(R.id.no_feed_items_found);
        mListSRL = (SwipeRefreshLayout) mainView.findViewById(R.id.feed_srl);
        mRecyclerView = (RecyclerView) mainView.findViewById(R.id.feed_rv);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        mNoItemsTV.setVisibility(View.VISIBLE);

        return mainView;
    }

    public static PeopleListFragment newInstance(String myUidParam, String otherUidParam,
                                                 String whatToDisplay, Bundle newBundle)
    {
        PeopleListFragment fragment = new PeopleListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_MY_UID_PARAM, myUidParam);
        args.putString(ARG_OTHER_UID_PARAM, otherUidParam);
        args.putString(ARG_WHAT_TO_DISPLAY_PARAM, whatToDisplay);
        args.putBundle(ARG_BUNDLE_PARAM, newBundle);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
        {
            myUid = getArguments().getString(ARG_MY_UID_PARAM);
            otherUid = getArguments().getString(ARG_OTHER_UID_PARAM);
            whatToDisplay = getArguments().getString(ARG_WHAT_TO_DISPLAY_PARAM);
            mBundle = getArguments().getBundle(ARG_BUNDLE_PARAM);
        }
    }

    private void initializeUser()
    {
        mDatabaseReference = FirebaseDatabase.getInstance().getReference();
        mStorageReference = FirebaseStorage.getInstance().getReference();

        if (otherUid == null)    // we do not need user if it's null
        {
            showItems();
            return;
        }

        mDatabaseReference.child(Const.USERS).child(otherUid).addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                mUser = dataSnapshot.getValue(User.class);

                showItems();
            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {
            }
        });
    }

    @Override
    public void onResume()
    {
        super.onResume();
        initializeUser();
    }

    private void showItems()
    {
        // prepare swipe refresh layout
        mListSRL.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener()
        {
            @Override
            public void onRefresh()
            {
                ConnectivityManager conMgr = (ConnectivityManager) getActivity().getApplication().getSystemService(Context.CONNECTIVITY_SERVICE);
                {
                    NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                    if (netInfo != null)
                    {
                        //new PeopleListTask().execute();
                        wireRecycler();

                    } else
                        Toast.makeText(getContext(), getString(R.string.error_internet), Toast.LENGTH_SHORT).show();
                }
                mListSRL.setRefreshing(false);
            }
        });
        mListSRL.setColorSchemeResources(android.R.color.holo_blue_dark, android.R.color.holo_red_dark);

        //new PeopleListTask().execute();
        wireRecycler();
    }

    private void wireRecycler()
    {
        final List<String> people = new ArrayList<>();
        boolean showHangoutMembers = false;

        if (whatToDisplay.equals(Const.PARAM_FRIENDS_LIST))
        {
            for (Map.Entry<String, String> friend : mUser.getFriendsList().entrySet())
                people.add(friend.getValue());

        } else if (whatToDisplay.equals(Const.PARAM_HANGOUT_MEMBERS_LIST))
        {
            // otherUid is null
            showHangoutMembers = true;
            String hangoutKey = mBundle.getString(Const.PARAM_HANGOUT_KEY);

            //mListSRL.setEnabled(false);

            // getting list of members
            mDatabaseReference.child(Const.HANGOUTS).child(hangoutKey)
                    .child(Const.HANGOUT_MEMBERS).addListenerForSingleValueEvent(new ValueEventListener()
            {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot)
                {
                    for (DataSnapshot memberSnapshot : dataSnapshot.getChildren())
                    {
                        // adding me to first place
                        if (memberSnapshot.getKey().equals(myUid))
                            people.add(0, memberSnapshot.getKey());
                        else
                            people.add(memberSnapshot.getKey());
                    }

                    adapter = new PeopleAdapter(people);
                    mNoItemsTV.setVisibility(View.GONE);
                    mRecyclerView.setAdapter(adapter);

                    if (people.size() == 0)
                    {
                        mNoItemsTV.setText("nothing found");
                        mNoItemsTV.setVisibility(View.VISIBLE);
                    } else
                        mListSRL.setVisibility(View.VISIBLE);
                }

                @Override
                public void onCancelled(DatabaseError databaseError)
                {

                }
            });
        } else if (whatToDisplay.equals(Const.PARAM_HANGOUT_APPLICANT_LIST))
        {
            showHangoutMembers = true;
            String hangoutKey = mBundle.getString(Const.PARAM_HANGOUT_KEY);

            if (!mBundle.getString(Const.PARAM_HANGOUT_HOST_ID).equals(myUid))
            {
                mNoItemsTV.setText("only host can see applicants");
                mNoItemsTV.setVisibility(View.VISIBLE);
                return;
            }

            // getting list of members
            mDatabaseReference.child(Const.HANGOUTS).child(hangoutKey)
                    .child(Const.HANGOUT_APPLICANTS).addListenerForSingleValueEvent(new ValueEventListener()
            {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot)
                {
                    for (DataSnapshot memberSnapshot : dataSnapshot.getChildren())
                        people.add(memberSnapshot.getKey());

                    adapter = new PeopleAdapter(people);
                    mNoItemsTV.setVisibility(View.GONE);
                    mRecyclerView.setAdapter(adapter);

                    if (people.size() == 0)
                    {
                        mNoItemsTV.setText("nothing found");
                        mNoItemsTV.setVisibility(View.VISIBLE);
                    } else
                        mListSRL.setVisibility(View.VISIBLE);
                }

                @Override
                public void onCancelled(DatabaseError databaseError)
                {

                }
            });
        }

        // searching from bundle
        else if (whatToDisplay.equals(Const.PARAM_SEARCH_PEOPLE))
        {
            showHangoutMembers = true;

            if (mBundle != null)
            {
                mDatabaseReference.child(Const.USERS).addListenerForSingleValueEvent(new ValueEventListener()
                {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot)
                    {
                        for (DataSnapshot userSnap : dataSnapshot.getChildren())
                        {
                            User currentUser = userSnap.getValue(User.class);


                            if (currentUser.getFullName().toLowerCase()
                                    .contains(mBundle.getString(Const.PARAM_SEARCH_REQUEST).toLowerCase()))
                                people.add(currentUser.getUserId());

                            adapter = new PeopleAdapter(people);
                            mNoItemsTV.setVisibility(View.GONE);
                            mRecyclerView.setAdapter(adapter);

                            if (people.size() == 0)
                            {
                                mNoItemsTV.setText("nothing found");
                                mNoItemsTV.setVisibility(View.VISIBLE);
                            } else
                                mListSRL.setVisibility(View.VISIBLE);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError)
                    {

                    }
                });
            } else
            {
                mNoItemsTV.setText("nothing found");
                mNoItemsTV.setVisibility(View.VISIBLE);
            }

        } else if (whatToDisplay.equals(Const.PARAM_INVITE_PEOPLE))
        {
            for (Map.Entry<String, String> friend : mUser.getFriendsList().entrySet())
                people.add(friend.getValue());
        }

        // displaying in recycler view
        if (!showHangoutMembers)
        {
            adapter = new PeopleAdapter(people);

            mNoItemsTV.setVisibility(View.GONE);
            mRecyclerView.setAdapter(adapter);

            if (people.size() == 0)
            {
                mNoItemsTV.setText("nothing found");
                mNoItemsTV.setVisibility(View.VISIBLE);
            } else
                mListSRL.setVisibility(View.VISIBLE);
        }
    }

    private class PeopleHolder extends RecyclerView.ViewHolder
    {
        private User mCurrentUser;
        private CardView userCV;
        private CircleImageView userCIV;
        private TextView userNameTV;
        private Button actionButton;
        private TextView userDepartmentTV;
        private RelativeLayout actionButtonRL;

        private PeopleHolder(View itemView)
        {
            super(itemView);

            userCV = (CardView) itemView.findViewById(R.id.item_person_cv);
            userCIV = (CircleImageView) itemView.findViewById(R.id.user_list_civ);
            userNameTV = (TextView) itemView.findViewById(R.id.user_list_tv);
            actionButton = (Button) itemView.findViewById(R.id.person_item_button);
            userDepartmentTV = (TextView) itemView.findViewById(R.id.user_list_department_tv);
            actionButtonRL = (RelativeLayout) itemView.findViewById(R.id.person_button_relative_l);
        }

        private void bindPerson(final String personId)
        {
            // download and set user info
            mDatabaseReference.child(Const.USERS).child(personId).addListenerForSingleValueEvent(new ValueEventListener()
            {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot)
                {

                    mCurrentUser = dataSnapshot.getValue(User.class);

                    // get user's name
                    final String fullName = dataSnapshot.child(Const.FULL_NAME).getValue(String.class);
                    userNameTV.setText(fullName);

                    // get user's department
                    String department = dataSnapshot.child(Const.DEPARTMENT).getValue(String.class);
                    userDepartmentTV.setText(department);

                    // downloading and setting photo to image view
                    if (dataSnapshot.child(Const.PROFILE_PHOTO_ID).getValue(String.class) != null)
                    {
                        // get user's photo
                        String profilePhotoId = Const.PHOTO_THUMBNAIL + dataSnapshot
                                .child(Const.PROFILE_PHOTO_ID).getValue(String.class);

                        StorageReference storageReference = mStorageReference.child(Const.USERS).child(personId).child(Const.PHOTOS_LISTS)
                                .child(profilePhotoId);
                        if (getContext() != null)
                        {
                            Glide.with(getContext())
                                    .using(new FirebaseImageLoader())
                                    .load(storageReference)
                                    .into(userCIV);
                        }
                    } else
                        userCIV.setImageDrawable(itemView.getResources().getDrawable(R.drawable.default_avatar));

                    // if it is the list of people in hangout applicants -> display button to approve
                    whenHangoutApplicantsOrInvitation();


                    // click listener
                    userCV.setOnClickListener(new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View v)
                        {
                            Intent intent = new Intent(getContext(), ProfileActivity.class);
                            intent.putExtra(Const.FULL_NAME, fullName);
                            intent.putExtra(Const.UID, myUid);
                            intent.putExtra(Const.OTHER_UID, personId);
                            startActivity(intent);
                        }
                    });
                }

                @Override
                public void onCancelled(DatabaseError databaseError)
                {
                }
            });


        }

        private void whenHangoutApplicantsOrInvitation()
        {
            // show approve button in "members and applicants"
            if (whatToDisplay.equals(Const.PARAM_HANGOUT_APPLICANT_LIST))
            {
                final String hangoutKey = mBundle.getString(Const.PARAM_HANGOUT_KEY);

                actionButton.setVisibility(View.VISIBLE);
                actionButtonRL.setVisibility(View.VISIBLE);
                actionButton.setText(getString(R.string.approve));

                actionButton.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View view)
                    {


                        // add him to member list
                        mDatabaseReference.child(Const.HANGOUTS).child(hangoutKey).child(Const.HANGOUT_MEMBERS)
                                .child(mCurrentUser.getUserId()).setValue("1");

                        // add this hangout to his hangout list
                        mDatabaseReference.child(Const.USERS).child(mCurrentUser.getUserId()).child(Const.HANGOUTS_LIST)
                                .child(hangoutKey).setValue("1");

                        mListSRL.setEnabled(false); // so that person cannot refresh and approve user again

                        actionButton.setText("approved");
                        actionButton.setTextColor(R.color.member);
                        actionButton.setEnabled(false);

                        // remove him from approval list
                        mDatabaseReference.child(Const.HANGOUTS).child(hangoutKey)
                                .child(Const.HANGOUT_APPLICANTS).child(mCurrentUser.getUserId()).setValue(null);
                    }
                });
            }

            // show "invite" button in "invite friends"
            if (whatToDisplay.equals(Const.PARAM_INVITE_PEOPLE))
            {
                final String hangoutKey = mBundle.getString(Const.PARAM_HANGOUT_KEY);

                // setting button
                mDatabaseReference.child(Const.HANGOUTS)
                        .child(hangoutKey).addListenerForSingleValueEvent(new ValueEventListener()
                {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot)
                    {
                        boolean isInvited = false;
                        boolean alreadyMember = false;

                        for (DataSnapshot memberSnap : dataSnapshot.child(Const.HANGOUT_MEMBERS).getChildren())
                        {
                            if (memberSnap.getKey().equals(mCurrentUser.getUserId()))
                            {
                                actionButton.setVisibility(View.VISIBLE);
                                actionButtonRL.setVisibility(View.VISIBLE);
                                actionButton.setText("already a member");
                                actionButton.setTextColor(R.color.member);
                                actionButton.setEnabled(false);
                                alreadyMember = true;
                                break;
                            }
                        }

                        if(!alreadyMember)
                        {
                            for (DataSnapshot invitedFriendSnap : dataSnapshot.child(Const.INVITED_FRIENDS).getChildren())
                            {
                                if (invitedFriendSnap.getKey().equals(mCurrentUser.getUserId()))
                                {
                                    actionButton.setVisibility(View.VISIBLE);
                                    actionButtonRL.setVisibility(View.VISIBLE);
                                    actionButton.setText("sent");
                                    actionButton.setTextColor(R.color.member);
                                    actionButton.setEnabled(false);
                                    isInvited = true;
                                    break;
                                }
                            }
                        }

                        if (!alreadyMember && !isInvited)
                        {
                            actionButton.setVisibility(View.VISIBLE);
                            actionButtonRL.setVisibility(View.VISIBLE);
                            actionButton.setText(getString(R.string.invite));
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError)
                    {

                    }
                });

                actionButton.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View view)
                    {

                        String hangoutKey = mBundle.getString(Const.PARAM_HANGOUT_KEY);

                        // add him to invites list
                        mDatabaseReference.child(Const.HANGOUTS).child(hangoutKey).child(Const.INVITED_FRIENDS)
                                .child(mCurrentUser.getUserId()).setValue("1");

                        // create and send him notification
                        Notification newNotif = new Notification(mUser.getFullName(), myUid, Const.NOTIF_TYPE_HANGOUT_INVITE);
                        newNotif.setSenderProfilePhotoId(mUser.getProfilePhotoId()); // todo v2 wrong! do not send profile photo id
                        Object ts = ServerValue.TIMESTAMP; // long
                        newNotif.setTimeSent(ts);
                        newNotif.setHangoutKey(hangoutKey);

                        mDatabaseReference.child(Const.USERS).child(mCurrentUser.getUserId())
                                .child(Const.NOTIFICATION_MANAGER).child(Const.NOTIF_HANGOUT_INVITE).push()
                                .setValue(newNotif);

                        mListSRL.setEnabled(false); // so that person cannot refresh and approve user again
                        actionButton.setText("sent");
                        actionButton.setTextColor(R.color.member);
                        actionButton.setEnabled(false);
                    }
                });
            } else
            {
                actionButton.setVisibility(View.GONE);
                actionButtonRL.setVisibility(View.GONE);
            }
        }

    }

    private class PeopleAdapter extends RecyclerView.Adapter<PeopleHolder>
    {

        private List<String> mPeople;
        private int lastPosition = -1;

        private PeopleAdapter(List<String> people)
        {
            mPeople = people;
        }

        @Override
        public PeopleHolder onCreateViewHolder(ViewGroup viewGroup, int i)
        {
            LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
            View view = inflater.inflate(R.layout.list_item_person, viewGroup, false);

            return new PeopleHolder(view);
        }

        @Override
        public void onBindViewHolder(PeopleHolder peopleHolder, int position)
        {
            String personId = mPeople.get(position);
            peopleHolder.bindPerson(personId);

            setAnimation(peopleHolder.userCV, position);
        }

        private void setAnimation(View viewToAnimate, int position)
        {
            // If the bound view wasn't previously displayed on screen, it's animated
            if (position > lastPosition)
            {
                viewToAnimate.animate().cancel();
                viewToAnimate.setTranslationY(100);
                viewToAnimate.setAlpha(0);
                viewToAnimate.animate().alpha(1.0f).translationY(0).setDuration(250).setStartDelay(position * 2);
            }
        }

        @Override
        public int getItemCount()
        {
            return mPeople.size();
        }
    }

}
