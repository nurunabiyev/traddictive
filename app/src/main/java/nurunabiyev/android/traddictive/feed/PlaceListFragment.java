package nurunabiyev.android.traddictive.feed;


import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionMenu;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import nurunabiyev.android.traddictive.Const;
import nurunabiyev.android.traddictive.R;
import nurunabiyev.android.traddictive.objects.places.Building;
import nurunabiyev.android.traddictive.objects.places.CafeShop;
import nurunabiyev.android.traddictive.objects.places.Dormitory;
import nurunabiyev.android.traddictive.objects.places.MainBuilding;
import nurunabiyev.android.traddictive.objects.places.Rating;

import static android.content.Context.MODE_PRIVATE;
import static nurunabiyev.android.traddictive.Const.PREFS_NAME;

public class PlaceListFragment extends Fragment
{
    private static final String ARG_PLACE_TYPE = "param1";
    private static final String ARG_MY_UID = "param2";

    // views
    private RecyclerView mRecyclerView;
    @BindView(R.id.no_feed_items_found) TextView mNoItemsTV;
    @BindView(R.id.feed_srl) SwipeRefreshLayout mListSRL;
    private BuildingAdapter mBuildingAdapter;
    // user
    private DatabaseReference mDatabaseReference;
    private String mPlaceType;
    private String myUid;
    private SharedPreferences prefs;


    public PlaceListFragment()
    {
        // Required empty public constructor
    }

    public static PlaceListFragment newInstance(String placeType, String uid)
    {
        PlaceListFragment fragment = new PlaceListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PLACE_TYPE, placeType);
        args.putString(ARG_MY_UID, uid);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
        {
            mPlaceType = getArguments().getString(ARG_PLACE_TYPE);
            myUid = getArguments().getString(ARG_MY_UID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        mDatabaseReference = FirebaseDatabase.getInstance().getReference();

        View view = inflater.inflate(R.layout.fragment_feed_list, container, false);
        ButterKnife.bind(this, view);
        prefs = getContext().getSharedPreferences(PREFS_NAME, MODE_PRIVATE);

        mListSRL.setEnabled(false); // no srl needed in places

        mRecyclerView = (RecyclerView) view.findViewById(R.id.feed_rv);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        // ordinary downloading all places
        if (mPlaceType.equals(Const.PLACES_CAFE_SHOPS))
            downloadCafes();
        else if (mPlaceType.equals(Const.PLACES_DORMITORIES))
            downloadDorms();
        else if (mPlaceType.equals(Const.PLACES_MAIN_BUILDINGS))
            downloadMain();

        // hiding fab on scroll
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener()
        {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy)
            {
                if (dy > 0)
                    ((FloatingActionMenu) getActivity().findViewById(R.id.float_menu)).hideMenu(true);
                else if (dy < 0)
                    ((FloatingActionMenu) getActivity().findViewById(R.id.float_menu)).showMenu(true);
            }
        });

        return view;
    }


    private void downloadMain()
    {
        mDatabaseReference.child(Const.PLACES)
                .child(Const.PLACES_MAIN_BUILDINGS).addListenerForSingleValueEvent(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                List<MainBuilding> mainBuildings = new ArrayList<>();

                for (DataSnapshot mainSnap : dataSnapshot.getChildren())
                {
                    MainBuilding mainBuilding = mainSnap.getValue(MainBuilding.class);
                    mainBuildings.add(mainBuilding);
                }

                Collections.sort(mainBuildings, new Comparator<MainBuilding>()
                {
                    @Override
                    public int compare(MainBuilding o1, MainBuilding o2)
                    {
                        boolean alphabet = prefs.getBoolean(Const.SHARED_PLACE_ALPHABET, true);
                        boolean rating = prefs.getBoolean(Const.SHARED_PLACE_RATING, false);
                        
                        if(!alphabet && rating)
                            return Double.toString(o2.getScore())
                                    .compareTo(Double.toString(o1.getScore()));
                        else
                            return o1.getName().compareToIgnoreCase(o2.getName());
                    }
                });

                mNoItemsTV.setVisibility(View.GONE);

                mBuildingAdapter = new BuildingAdapter(mainBuildings);
                mRecyclerView.setAdapter(mBuildingAdapter);

                if (mainBuildings.size() == 0)
                {
                    mNoItemsTV.setText(getString(R.string.nothing_found));
                    mNoItemsTV.setVisibility(View.VISIBLE);
                } else
                    mListSRL.setVisibility(View.VISIBLE);
            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {
            }
        });
    }

    private void downloadDorms()
    {
        mDatabaseReference.child(Const.PLACES)
                .child(Const.PLACES_DORMITORIES).addListenerForSingleValueEvent(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                List<Dormitory> dormitories = new ArrayList<>();

                for (DataSnapshot dormSnap : dataSnapshot.getChildren())
                {
                    Dormitory dormitory = dormSnap.getValue(Dormitory.class);
                    dormitories.add(dormitory);
                }

                Collections.sort(dormitories, new Comparator<Dormitory>()
                {
                    @Override
                    public int compare(Dormitory o1, Dormitory o2)
                    {
                        boolean alphabet = prefs.getBoolean(Const.SHARED_PLACE_ALPHABET, true);
                        boolean rating = prefs.getBoolean(Const.SHARED_PLACE_RATING, false);

                        if(!alphabet && rating)
                            return Double.toString(o2.getScore())
                                    .compareTo(Double.toString(o1.getScore()));
                        else
                            return o1.getName().compareToIgnoreCase(o2.getName());
                    }
                });

                mNoItemsTV.setVisibility(View.GONE);

                mBuildingAdapter = new BuildingAdapter(dormitories);
                mRecyclerView.setAdapter(mBuildingAdapter);

                if (dormitories.size() == 0)
                {
                    mNoItemsTV.setText(getString(R.string.nothing_found));
                    mNoItemsTV.setVisibility(View.VISIBLE);
                } else
                    mListSRL.setVisibility(View.VISIBLE);
            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {

            }
        });

    }

    private void downloadCafes()
    {
        mDatabaseReference.child(Const.PLACES)
                .child(Const.PLACES_CAFE_SHOPS).addListenerForSingleValueEvent(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                List<CafeShop> cafeShops = new ArrayList<>();

                for (DataSnapshot cafeSnapshot : dataSnapshot.getChildren())
                {
                    CafeShop cafeShop = cafeSnapshot.getValue(CafeShop.class);
                    cafeShops.add(cafeShop);
                }

                Collections.sort(cafeShops, new Comparator<CafeShop>()
                {
                    @Override
                    public int compare(CafeShop o1, CafeShop o2)
                    {
                        boolean alphabet = prefs.getBoolean(Const.SHARED_PLACE_ALPHABET, true);
                        boolean rating = prefs.getBoolean(Const.SHARED_PLACE_RATING, false);

                        if(!alphabet && rating)
                            return Double.toString(o2.getScore())
                                    .compareTo(Double.toString(o1.getScore()));
                        else
                            return o1.getName().compareToIgnoreCase(o2.getName());
                    }
                });

                mNoItemsTV.setVisibility(View.GONE);

                mBuildingAdapter = new BuildingAdapter(cafeShops);
                mRecyclerView.setAdapter(mBuildingAdapter);

                if (cafeShops.size() == 0)
                {
                    mNoItemsTV.setText(getString(R.string.nothing_found));
                    mNoItemsTV.setVisibility(View.VISIBLE);
                } else
                    mListSRL.setVisibility(View.VISIBLE);
            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {

            }
        });
    }

    private void initializeDatingDialog(final Building place, final String placeType)
    {
        final AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
        alert.setTitle("Rate " + place.getName());
        alert.setMessage("Current rating is " + place.getScore());

        // setting rating bar
        LayoutInflater li = LayoutInflater.from(getContext());
        final View myView = li.inflate(R.layout.rating_view, null);
        final RatingBar ratingBar = (RatingBar) myView.findViewById(R.id.dialog_rb);
        ratingBar.setRating(Float.parseFloat("" + place.getScore()));

        alert.setView(myView);

        // clicking on Update
        alert.setPositiveButton(getString(R.string.update), new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int whichButton)
            {

                boolean ratedBefore = false;

                // validate if user already rated
                for (Map.Entry<String, Rating> ratingEntry : place.getRatings().entrySet())
                {
                    if (ratingEntry.getValue().getRatedPersonId().equals(myUid))
                    {
                        Toast.makeText(getContext(), getString(R.string.you_already_rated), Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                        ratedBefore = true;
                        break;
                    }
                }

                // else rate
                if (!ratedBefore)
                {
                    // create rate
                    Rating newRating = new Rating();
                    newRating.setRatedPersonId(myUid);
                    newRating.setRateScore(ratingBar.getRating());

                    // send rate to rateList of this building
                    mDatabaseReference.child(Const.PLACES).child(placeType).child(place.getKey())
                            .child(Const.RATINGS).push().setValue(newRating);

                    mRecyclerView.setAdapter(null);

                    // download them again
                    if (mPlaceType.equals(Const.PLACES_CAFE_SHOPS))
                        downloadCafes();
                    else if (mPlaceType.equals(Const.PLACES_DORMITORIES))
                        downloadDorms();
                    else if (mPlaceType.equals(Const.PLACES_MAIN_BUILDINGS))
                        downloadMain();
                }

                dialog.dismiss();
            }
        });


        alert.setNeutralButton(getString(R.string.show_location), new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {

                double latitude = place.getLongitude();
                double longitude = place.getLatitude();
                String label = place.getName();
                String uriBegin = "geo:" + latitude + "," + longitude;
                String query = latitude + "," + longitude + "(" + label + ")";
                String encodedQuery = Uri.encode(query);
                String uriString = uriBegin + "?q=" + encodedQuery + "&z=16";
                Uri uri = Uri.parse(uriString);
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });

        alert.show();
    }


    /**
     * Place holders
     */

    private class CafeHolder extends RecyclerView.ViewHolder
    {
        private CafeShop mCafeShop;
        private TextView cafeScoreTV;
        private TextView cafeDescriptionTV;
        private TextView cafeNameTV;
        private TextView cafeTimingsTV;
        private CardView cafeCardView;

        public CafeHolder(View itemView)
        {
            super(itemView);

            cafeScoreTV = (TextView) itemView.findViewById(R.id.building_score_tv);
            cafeDescriptionTV = (TextView) itemView.findViewById(R.id.building_description_contacts_text_view);
            cafeNameTV = (TextView) itemView.findViewById(R.id.building_name_text_view);
            cafeTimingsTV = (TextView) itemView.findViewById(R.id.building_more_text_view);
            cafeCardView = (CardView) itemView.findViewById(R.id.building_card_view);
        }

        public void bindCafe(CafeShop cafeShop)
        {
            mCafeShop = cafeShop;

            final double longitude = mCafeShop.getLongitude();
            final double latitude = mCafeShop.getLatitude();

            cafeScoreTV.setText(String.format("%.2f", mCafeShop.getScore()));
            cafeDescriptionTV.setText(mCafeShop.getDescription());
            cafeNameTV.setText(mCafeShop.getName());
            cafeTimingsTV.setText(mCafeShop.getTimings());
            cafeCardView.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    initializeDatingDialog(mCafeShop, Const.PLACES_CAFE_SHOPS);
                }
            });
        }
    }


    private class DormHolder extends RecyclerView.ViewHolder
    {
        private Dormitory mDormitory;
        private TextView dormScoreTV;
        private TextView dormGenderTV;
        private TextView dormNumberTV;
        private TextView dormRoomTypesTV;
        private CardView dormCardView;

        public DormHolder(View itemView)
        {
            super(itemView);

            dormScoreTV = (TextView) itemView.findViewById(R.id.building_score_tv);
            dormGenderTV = (TextView) itemView.findViewById(R.id.building_description_contacts_text_view);
            dormNumberTV = (TextView) itemView.findViewById(R.id.building_name_text_view);
            dormRoomTypesTV = (TextView) itemView.findViewById(R.id.building_more_text_view);
            dormCardView = (CardView) itemView.findViewById(R.id.building_card_view);
        }

        public void bindDormitory(Dormitory dormitory)
        {
            mDormitory = dormitory;
            final double longitude = mDormitory.getLongitude();
            final double latitude = mDormitory.getLatitude();

            dormScoreTV.setText(String.format("%.2f", mDormitory.getScore()));

            dormNumberTV.setText(String.valueOf("#" + mDormitory.getName()));
            final String gender;
            if (mDormitory.getGender())
                gender = getString(R.string.male);
            else
                gender = getString(R.string.female);
            dormGenderTV.setText(gender);

            /**
             * Quadruple Room = 0 /
             * Triple Room  = 1 /
             * Double Room(bunk beds) = 2 / Double Room(single beds) = 3 / Double Room(Single Beds Shared Kitchen Private Bathroom) = 4
             * Single Room(Standard) = 5 /  Single Room(Private Bathroom) = 6 /
             * Single Room(Shared Kitchen Private Bathroom) = 7 / Suite(Double Occupancy) = 8 / Suite(Single Occupancy) = 9
             */

            String roomTypes = "";
            for (int i = 0; i < mDormitory.getTypeOfRooms().size(); i++)
            {
                if (mDormitory.getTypeOfRooms().get(i) == 0)
                    roomTypes += "Quadruple Room.";
                if (mDormitory.getTypeOfRooms().get(i) == 1)
                    roomTypes += "\nTriple Room.";
                if (mDormitory.getTypeOfRooms().get(i) == 2)
                    roomTypes += "\nDouble Room(bunk beds).";
                if (mDormitory.getTypeOfRooms().get(i) == 3)
                    roomTypes += "\nDouble Room(single beds).";
                if (mDormitory.getTypeOfRooms().get(i) == 4)
                    roomTypes += "\nDouble Room(Single Beds Shared Kitchen Private Bathroom).";
                if (mDormitory.getTypeOfRooms().get(i) == 5)
                    roomTypes += "\nSingle Room(Standard).";
                if (mDormitory.getTypeOfRooms().get(i) == 6)
                    roomTypes += "\nSingle Room(Private Bathroom).";
                if (mDormitory.getTypeOfRooms().get(i) == 7)
                    roomTypes += "\nSingle Room(Shared Kitchen Private Bathroom).";
                if (mDormitory.getTypeOfRooms().get(i) == 8)
                    roomTypes += "\nSuite(Double Occupancy).";
                if (mDormitory.getTypeOfRooms().get(i) == 9)
                    roomTypes += "\nSuite(Single Occupancy).";
            }
            dormRoomTypesTV.setText(roomTypes);

            dormCardView.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {

                    initializeDatingDialog(mDormitory, Const.PLACES_DORMITORIES);

                }
            });
        }
    }


    private class MainBuildingHolder extends RecyclerView.ViewHolder
    {
        private MainBuilding mMainBuilding;
        private TextView mainScoreTV;
        private TextView mainDescriptionTV;
        private TextView mainNameTV;
        private TextView mainTimingsTV;
        private CardView mainCardView;

        public MainBuildingHolder(View itemView)
        {
            super(itemView);

            mainScoreTV = (TextView) itemView.findViewById(R.id.building_score_tv);
            mainDescriptionTV = (TextView) itemView.findViewById(R.id.building_description_contacts_text_view);
            mainNameTV = (TextView) itemView.findViewById(R.id.building_name_text_view);
            mainTimingsTV = (TextView) itemView.findViewById(R.id.building_more_text_view);
            mainCardView = (CardView) itemView.findViewById(R.id.building_card_view);
        }

        public void bindMain(MainBuilding mainBuilding)
        {
            mMainBuilding = mainBuilding;

            final double longitude = mMainBuilding.getLongitude();
            final double latitude = mMainBuilding.getLatitude();

            mainScoreTV.setText(String.format("%.2f", mMainBuilding.getScore()));

            mainDescriptionTV.setText(mMainBuilding.getDescription());
            mainNameTV.setText(mMainBuilding.getName());
            mainTimingsTV.setText(mMainBuilding.getTiming());
            mainCardView.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {

                    initializeDatingDialog(mMainBuilding, Const.PLACES_MAIN_BUILDINGS);
                }
            });
        }
    }


    /**
     * Inner Adapter Class
     */
    private class BuildingAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
    {
        private List mBuildings;
        private int lastPosition = -1;


        public BuildingAdapter(List list)
        {
            mBuildings = list;
        }


        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
        {
            LayoutInflater inflater = LayoutInflater.from(getActivity());
            View view = inflater.inflate(R.layout.list_item_building, parent, false);

            if (mPlaceType.equals(Const.PLACES_CAFE_SHOPS))
                return new CafeHolder(view);
            else if (mPlaceType.equals(Const.PLACES_DORMITORIES))
                return new DormHolder(view);
            else
                return new MainBuildingHolder(view);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int i)
        {
            Building building = (Building) mBuildings.get(i);

            if (mPlaceType.equals(Const.PLACES_CAFE_SHOPS))
            {
                ((CafeHolder) holder).bindCafe((CafeShop) building);
                setAnimation(((CafeHolder) holder).cafeCardView, i);

            } else if (mPlaceType.equals(Const.PLACES_DORMITORIES))
            {
                ((DormHolder) holder).bindDormitory((Dormitory) building);
                setAnimation(((DormHolder) holder).dormCardView, i);
            } else if (mPlaceType.equals(Const.PLACES_MAIN_BUILDINGS))
            {
                ((MainBuildingHolder) holder).bindMain((MainBuilding) building);
                setAnimation(((MainBuildingHolder) holder).mainCardView, i);
            }

        }


        /**
         * Here is the key method to apply the animation
         */
        private void setAnimation(View viewToAnimate, int position)
        {
            // If the bound view wasn't previously displayed on screen, it's animated
            if (position > lastPosition)
            {
                viewToAnimate.animate().cancel();
                viewToAnimate.setTranslationY(100);
                viewToAnimate.setAlpha(0);
                viewToAnimate.animate().alpha(1.0f).translationY(0).setDuration(250).setStartDelay(position * 2);
            }
        }

        @Override
        public int getItemCount()
        {
            return mBuildings.size();
        }
    }

    private class Buildings
    {
        List<Building> mBuildings;

        public Buildings(List<CafeShop> cafeShops, List<MainBuilding> mainBuildings, List<Dormitory> dormitories)
        {
            //mainBuildingObjects(mainBuildings);
            //cafeShopObjects(cafeShops);
            dormitoryObjects(dormitories);
        }

        // initializing each main building and adding to the list
        private void mainBuildingObjects(List<MainBuilding> mainBuildingList)
        {
            MainBuilding m1 = new MainBuilding("G Building", "Main Campus");
            m1.setLocation(39.8687001, 32.7496001);
            m1.setDescription("School of Computer Technology and Office Management");

            MainBuilding m2 = new MainBuilding("T Building", "Main Campus");
            m2.setLocation(39.8682999, 32.7492990);
            m2.setDescription("Lecture Halls");

            MainBuilding m3 = new MainBuilding("H Building", "Main Campus");
            m3.setLocation(39.8684000, 32.7502991);
            m3.setDescription("Faculty of Humanities and Letters");

            MainBuilding m4 = new MainBuilding("A Building", "Main Campus");
            m4.setLocation(39.8679991, 32.7493999);
            m4.setDescription("Faculty of Administrative, Economics and Social Sciences");

            MainBuilding m5 = new MainBuilding("M Building", "Main Campus");
            m5.setLocation(39.8674999, 32.7501999);
            m5.setDescription("Faculty of Business Administration");

            MainBuilding m6 = new MainBuilding("V Building", "Main Campus");
            m6.setLocation(39.8669999, 32.7500001);
            m6.setDescription("Lecture Halls");

            MainBuilding m7 = new MainBuilding("B Building", "Main Campus");
            m7.setLocation(39.8687001, 32.7481990);
            m7.setDescription("Faculty of Law and Computer Center");

            MainBuilding m8 = new MainBuilding("SA Building", "Main Campus");
            m8.setLocation(39.8677001, 32.7482999);
            m8.setDescription("Faculty of Science: Block A");

            MainBuilding m9 = new MainBuilding("SB Building", "Main Campus");
            m9.setLocation(39.8682999, 32.7482999);
            m9.setDescription("Faculty of Science: Block B");

            MainBuilding m10 = new MainBuilding("SN Building", "Main Campus");
            m10.setLocation(39.8677989, 32.7478999);
            m10.setDescription("Nanotechnology Research Center");

            MainBuilding m11 = new MainBuilding("SL Building", "Main Campus");
            m11.setLocation(39.8672001, 32.7485989);
            m11.setDescription("Advanced Research Lab");

            MainBuilding m12 = new MainBuilding("L Building", "Main Campus");
            m12.setLocation(39.8690989, 32.7498991);
            m12.setDescription("School of Applied Languages");

            MainBuilding m13 = new MainBuilding("KM Building", "Main Campus");
            m13.setLocation(39.8700999, 32.7500001);
            m13.setDescription("Bilkent Library");

            MainBuilding m14 = new MainBuilding("EA Building", "Main Campus");
            m14.setLocation(39.8711990, 32.7500990);
            m14.setDescription("Faculty of Engineering");

            MainBuilding m15 = new MainBuilding("EB Building", "Main Campus");
            m15.setLocation(39.8718000, 32.7497999);
            m15.setDescription("Mithat Coruh Auditorium and Classrooms");

            MainBuilding m16 = new MainBuilding("EE Building", "Main Campus");
            m16.setLocation(39.8720991, 32.7507001);
            m16.setDescription("Electric and Electronics Engineering");

            MainBuilding m17 = new MainBuilding("FA Building", "Main Campus");
            m17.setLocation(39.8662989, 32.7497999);
            m17.setDescription("Faculty of Art, Design and Architecture: Block A");

            MainBuilding m18 = new MainBuilding("FB Building", "Main Campus");
            m18.setLocation(39.8666000, 32.7496001);
            m18.setDescription("Faculty of Art, Design and Architecture: Block B");

            MainBuilding m19 = new MainBuilding("FC Building", "Main Campus");
            m19.setLocation(39.8669000, 32.7492990);
            m19.setDescription("Faculty of Art, Design and Architecture: Block C");

            MainBuilding m20 = new MainBuilding("C Building", "East Campus");
            m20.setLocation(39.8713001, 32.7638999);
            m20.setDescription("School of English Language");

            MainBuilding m21 = new MainBuilding("D Building", "East Campus");
            m21.setLocation(39.8705001, 32.7648991);
            m21.setDescription("School of English Language");

            MainBuilding m22 = new MainBuilding("KE Building", "East Campus");
            m22.setLocation(39.8721999, 32.7638999);
            m22.setDescription("East Campus Library");

            MainBuilding m23 = new MainBuilding("N Building", "East Campus");
            m23.setLocation(39.8728000, 32.7633001);
            m23.setDescription("School of English Language: Blocks A & B");

            MainBuilding m24 = new MainBuilding("ODN Building", "East Campus");
            m24.setLocation(39.8756000, 32.7523001);
            m24.setDescription("Bilkent Odeon: Auditorium");

            MainBuilding m25 = new MainBuilding("J Building", "East Campus");
            m25.setLocation(39.8745000, 32.7508999);
            m25.setDescription("Faculty Housing");

            // sports now
            MainBuilding sp1 = new MainBuilding("Sports Center ", "Main Campus");
            sp1.setTimings("Weekdays: 07:30 a.m. - 11:00 p.m. \nWeekends: 09:00 a.m. - 11:00 p.m.");
            sp1.setDescription("290 1325, 290 1993 ");
            sp1.setLocation(39.863697, 32.745606);

            MainBuilding sp2 = new MainBuilding("Main Sports Hall", "Main Campus");
            sp2.setTimings("Weekdays: 09:00 a.m. - 10:00 p.m. \nWeekends: 09:00 a.m. - 06:00 p.m.");
            sp2.setDescription("290 3186");
            sp2.setLocation(39.866613, 32.748507);

            MainBuilding sp3 = new MainBuilding("East Sports Hall", "Main Campus");
            sp3.setTimings("Weekdays: 09:00 a.m. - 11:00 p.m. \nWeekends: 09:00 a.m. - 11:00 p.m.");
            sp3.setDescription("290 5350, 290 5351");
            sp3.setLocation(39.870064, 32.764996);

            mainBuildingList.add(sp1);
            mainBuildingList.add(sp2);
            mainBuildingList.add(sp3);

            // adding main buildings to the list
            mainBuildingList.add(m1);
            mainBuildingList.add(m2);
            mainBuildingList.add(m3);
            mainBuildingList.add(m4);
            mainBuildingList.add(m5);
            mainBuildingList.add(m6);
            mainBuildingList.add(m7);
            mainBuildingList.add(m8);
            mainBuildingList.add(m9);
            mainBuildingList.add(m10);
            mainBuildingList.add(m11);
            mainBuildingList.add(m12);
            mainBuildingList.add(m13);
            mainBuildingList.add(m14);
            mainBuildingList.add(m15);
            mainBuildingList.add(m16);
            mainBuildingList.add(m17);
            mainBuildingList.add(m18);
            mainBuildingList.add(m19);
            mainBuildingList.add(m20);
            mainBuildingList.add(m21);
            mainBuildingList.add(m22);
            mainBuildingList.add(m23);
            mainBuildingList.add(m24);
            mainBuildingList.add(m25);
        }

        // initializing each cafe or shop and adding to the list
        private void cafeShopObjects(List<CafeShop> cafeShops)
        {
            // shops
            CafeShop s1 = new CafeShop("Bus Stop Kiosk ", "Main Campus");
            s1.setTimings("07:00 - 19:00 (Closed on Sundays)");
            s1.setLocation(39.869490, 32.746552);
            s1.setDescription("Drinks and snacks");

            CafeShop s2 = new CafeShop("Meteksan Store", "Main Campus");
            s2.setTimings("08:00 - 23:00");
            s2.setLocation(39.872594, 32.751323);
            s2.setDescription("Super mart - all groceries");

            CafeShop s3 = new CafeShop("Meteksan kirtasiye", "Main Campus");
            s3.setTimings("08:00 - 17:30 (Closed on Sundays)");
            s3.setLocation(39.866279, 32.748490);
            s3.setDescription("Book store and school supplies");

            // restaurants now
            CafeShop r1 = new CafeShop("Bilka ", "Main Campus");
            r1.setTimings("Open 24 hours \n(Fridays are 07:30 - 23:30)");
            r1.setDescription("Tel: 290 4111 - 4191 - 4297");
            r1.setLocation(39.864560, 32.747798);

            CafeShop r2 = new CafeShop("Café In", "Main Campus");
            r2.setTimings("Weekdays: 07:30 - 20:00 \nWeekends: 08:00 - 19:30");
            r2.setDescription("Tel: 290 2080");
            r2.setLocation(39.869921, 32.750540);

            CafeShop r3 = new CafeShop("Café Mozart (EE)", "Main Campus");
            r3.setTimings("Weekdays: 08:00 - 17:30 \nWeekends: Closed");
            r3.setDescription("Tel: 290 3021");
            r3.setLocation(39.872064, 32.750690);

            CafeShop r4 = new CafeShop("Sözeri Pide Hut", "Main Campus");
            r4.setTimings("Weekdays: 11:00 - 22:00 \nWeekends: 11:00 - 22:00");
            r4.setDescription("Tel: 290 1693");
            r4.setLocation(39.865522, 32.744227);

            CafeShop r5 = new CafeShop("Starbucks", "Main Campus");
            r5.setTimings("Weekdays: 07:00 - 20:30 \nWeekends: 10:00 - 17:30");
            r5.setDescription("Tel: 266 3329");
            r5.setLocation(39.867866, 32.749489);

            CafeShop r6 = new CafeShop("Fiero Cafe", "Main Campus");
            r6.setTimings("Weekdays: 07:30 - 18:00 \nWeekends: Closed");
            r6.setDescription("Tel: 290 2824");
            r6.setLocation(39.868093, 32.749603);

            CafeShop r7 = new CafeShop("Express Cafe (FMPA)", "Main Campus");
            r7.setTimings("Weekdays: 08:00 - 18:30 \nWeekends: Closed");
            r7.setDescription("Tel: 290 1958");
            r7.setLocation(39.866204, 32.749346);

            CafeShop r8 = new CafeShop("Kiraç", "Main Campus");
            r8.setTimings("Weekdays: 08:00 - 21:30 \nWeekends: 10:00 - 21:30");
            r8.setDescription("Tel: 290 1650");
            r8.setLocation(39.866126, 32.749316);

            CafeShop r9 = new CafeShop("Kiraç", "Main Campus");
            r9.setTimings("Weekdays: 08:00 - 21:30 \nWeekends: 10:00 - 21:30");
            r9.setDescription("Tel: 290 1650");
            r9.setLocation(39.865974, 32.748446);

            CafeShop r10 = new CafeShop("Coffee Break (FEASS) ", "Main Campus");
            r10.setTimings("Weekdays: 08:00 - 18:00 \nWeekends: Closed");
            r10.setDescription("Tel: 290 2267");
            r10.setLocation(39.868183, 32.749120);

            cafeShops.add(s1);
            cafeShops.add(s2);
            cafeShops.add(s3);

            cafeShops.add(r1);
            cafeShops.add(r2);
            cafeShops.add(r3);
            cafeShops.add(r4);
            cafeShops.add(r5);
            cafeShops.add(r6);
            cafeShops.add(r7);
            cafeShops.add(r8);
            cafeShops.add(r9);
            cafeShops.add(r10);
        }

        // initializing each dormitory and adding to the list
        private void dormitoryObjects(List<Dormitory> dormitories)
        {
            //mBuildings = new ArrayList<>();

            Dormitory d1a = new Dormitory("14A", "Main Campus");
            d1a.setNoFloor(5);
            d1a.setTypeOfRooms(2, 5);
            d1a.setLocation(39.874478, 32.750868);
            d1a.setGender(true);
            Dormitory d1b = new Dormitory("14B", "Main Campus");
            d1b.setNoFloor(5);
            d1b.setTypeOfRooms(2, 5);
            d1b.setLocation(39.874478, 32.750868);
            d1a.setGender(false);

            Dormitory d2 = new Dormitory("15", "Main Campus");
            d2.setNoFloor(5);
            d2.setTypeOfRooms(2, 5);
            d2.setLocation(39.874303, 32.751232);
            d2.setGender(true);

            Dormitory d3 = new Dormitory("50", "Main Campus");
            d3.setNoFloor(9);
            d3.setTypeOfRooms(1);
            d3.setLocation(39.8651, 32.7494);
            d3.setGender(false);

            Dormitory d4 = new Dormitory("51", "Main Campus");
            d4.setNoFloor(9);
            d4.setTypeOfRooms(1);
            d4.setLocation(39.865, 32.7499);
            d4.setGender(false);

            Dormitory d5 = new Dormitory("52", "Main Campus");
            d5.setNoFloor(5);
            d5.setTypeOfRooms(5, 9);
            d5.setLocation(39.8648, 32.7498);
            d5.setGender(true);

            Dormitory d6 = new Dormitory("54", "Main Campus");
            d6.setNoFloor(9);
            d6.setTypeOfRooms(2);
            d6.setLocation(39.8645, 32.7498);
            d6.setGender(true);

            Dormitory d7 = new Dormitory("55", "Main Campus");
            d7.setNoFloor(9);
            d7.setTypeOfRooms(2);
            d7.setLocation(39.8644, 32.75);
            d7.setGender(true);

            Dormitory d8 = new Dormitory("60", "Main Campus");
            d8.setNoFloor(9);
            d8.setTypeOfRooms(2);
            d8.setLocation(39.864, 32.7497);
            d8.setGender(true);

            Dormitory d9 = new Dormitory("61", "Main Campus");
            d9.setNoFloor(9);
            d9.setTypeOfRooms(2);
            d9.setLocation(39.8639, 32.7496);
            d9.setGender(true);

            Dormitory d10 = new Dormitory("62", "Main Campus");
            d10.setNoFloor(9);
            d10.setTypeOfRooms(2);
            d10.setLocation(39.8637, 32.7498);
            d10.setGender(true);

            Dormitory d11 = new Dormitory("63", "Main Campus");
            d11.setNoFloor(9);
            d11.setTypeOfRooms(2);
            d11.setLocation(39.8634990, 32.7496001);
            d11.setGender(true);

            Dormitory d12 = new Dormitory("64", "Main Campus");
            d12.setNoFloor(5);
            d12.setTypeOfRooms(5, 9);
            d12.setLocation(39.8633999, 32.7490998);
            d12.setGender(true);

            Dormitory d13 = new Dormitory("69", "Main Campus");
            d13.setNoFloor(7);
            d13.setTypeOfRooms(4, 6, 7, 8, 9);
            d13.setLocation(39.8645001, 32.7490998);
            d13.setGender(false);

            Dormitory d14 = new Dormitory("70", "Main Campus");
            d14.setNoFloor(7);
            d14.setTypeOfRooms(4, 6, 7, 8, 9);
            d14.setLocation(39.8640000, 32.7492001);
            d14.setGender(true);

            Dormitory d15 = new Dormitory("71", "Main Campus");
            d15.setNoFloor(9);
            d15.setTypeOfRooms(0, 1);
            d15.setLocation(39.8643001, 32.7485989);
            d15.setGender(false);

            Dormitory d16 = new Dormitory("72", "Main Campus");
            d16.setNoFloor(9);
            d16.setTypeOfRooms(0);
            d16.setLocation(39.8640999, 32.7485989);
            d16.setGender(true);

            Dormitory d17 = new Dormitory("73", "Main Campus");
            d17.setNoFloor(9);
            d17.setTypeOfRooms(1);
            d17.setLocation(39.8638001, 32.7485989);
            d17.setGender(true);

            Dormitory d18 = new Dormitory("74", "Main Campus");
            d18.setNoFloor(9);
            d18.setTypeOfRooms(1);
            d18.setLocation(39.8636001, 32.7485000);
            d18.setGender(true);

            Dormitory d19a = new Dormitory("75A", "Main Campus");
            d19a.setGender(true);
            d19a.setNoFloor(6);
            d19a.setTypeOfRooms(5);
            d19a.setLocation(39.8638999, 32.7477001);
            Dormitory d19b = new Dormitory("75B", "Main Campus");
            d19b.setGender(false);
            d19b.setNoFloor(6);
            d19b.setTypeOfRooms(5);
            d19b.setLocation(39.8638999, 32.7477001);

            Dormitory d20 = new Dormitory("76", "Main Campus");
            d20.setNoFloor(6);
            d20.setTypeOfRooms(2, 3, 5);
            d20.setLocation(39.8645001, 32.7476002);
            d20.setGender(false);

            Dormitory d21a = new Dormitory("77A", "Main Campus");
            d21a.setTypeOfRooms(0, 1, 2, 3);
            d21a.setNoFloor(5);
            d21a.setLocation(39.8644000, 32.7466000);
            d21a.setGender(true);
            Dormitory d21b = new Dormitory("77B", "Main Campus");
            d21b.setTypeOfRooms(0, 1, 2, 3);
            d21b.setNoFloor(5);
            d21b.setLocation(39.8644000, 32.7466000);
            d21b.setGender(false);

            Dormitory d22a = new Dormitory("78A", "Main Campus");
            d22a.setTypeOfRooms(0, 1, 2, 3);
            d22a.setNoFloor(5);
            d22a.setLocation(39.8650999, 32.7461001);
            d22a.setGender(true);
            Dormitory d22b = new Dormitory("78B", "Main Campus");
            d22b.setTypeOfRooms(0, 1, 2, 3);
            d22b.setNoFloor(5);
            d22b.setLocation(39.8650999, 32.7461001);
            d22b.setGender(false);

            // east campus
            Dormitory d24 = new Dormitory("90", "East Campus");
            d24.setTypeOfRooms(0, 1, 2, 3, 5, 6);
            d24.setNoFloor(6);
            d24.setLocation(39.8687001, 32.7642999);
            d24.setGender(false);

            Dormitory d25 = new Dormitory("91", "East Campus");
            d25.setNoFloor(6);
            d25.setTypeOfRooms(0, 1, 2, 3, 5, 6);
            d25.setLocation(39.8690989, 32.7637990);
            d25.setGender(true);

            Dormitory d26 = new Dormitory("92", "East Campus");
            d26.setNoFloor(4);
            d26.setTypeOfRooms(3, 8);
            d26.setLocation(39.8695999, 32.7633990);
            d26.setGender(true);

            Dormitory d27 = new Dormitory("93", "East Campus");
            d27.setNoFloor(4);
            d27.setTypeOfRooms(3, 8);
            d27.setLocation(39.8702000, 32.7631989);
            d27.setGender(false);

            // adding dormitories to the list
            dormitories.add(d1a);
            dormitories.add(d1b);
            dormitories.add(d2);
            dormitories.add(d3);
            dormitories.add(d4);
            dormitories.add(d5);
            dormitories.add(d6);
            dormitories.add(d7);
            dormitories.add(d8);
            dormitories.add(d9);
            dormitories.add(d10);
            dormitories.add(d11);
            dormitories.add(d12);
            dormitories.add(d13);
            dormitories.add(d14);
            dormitories.add(d15);
            dormitories.add(d16);
            dormitories.add(d17);
            dormitories.add(d18);
            dormitories.add(d19a);
            dormitories.add(d19b);
            dormitories.add(d20);
            dormitories.add(d21a);
            dormitories.add(d21b);
            dormitories.add(d22a);
            dormitories.add(d22b);
            dormitories.add(d24);
            dormitories.add(d25);
            dormitories.add(d26);
            dormitories.add(d27);
        }
    }

}
