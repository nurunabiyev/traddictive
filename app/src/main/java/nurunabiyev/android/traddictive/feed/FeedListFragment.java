package nurunabiyev.android.traddictive.feed;


import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionMenu;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import nurunabiyev.android.traddictive.Const;
import nurunabiyev.android.traddictive.R;
import nurunabiyev.android.traddictive.hangout.HangoutRecycler;
import nurunabiyev.android.traddictive.objects.Hangout;
import nurunabiyev.android.traddictive.objects.Post;
import nurunabiyev.android.traddictive.objects.Product;
import nurunabiyev.android.traddictive.objects.User;

import static android.content.Context.MODE_PRIVATE;
import static nurunabiyev.android.traddictive.Const.FEED_BRUMORS;
import static nurunabiyev.android.traddictive.Const.PREFS_NAME;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FeedListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FeedListFragment extends Fragment
{
    private static final String TAG = "FeedListFragment";
    private static final String ARG_UID_PARAM = "param1";
    private static final String ARG_OTHER_UID_PARAM = "param3";
    private static final String ARG_CONTENT_PARAM = "param2";
    private static final String ARG_BUNDLE_PARAM = "param4";
    // view
    private RecyclerView mRecyclerView;
    private TextView mNoItemsTV;
    private SwipeRefreshLayout mListSRL;
    private View mainView;
    // user
    private String myUidParam;
    private String otherUidParam;
    private String mWhatToDisplay;
    private Bundle mBundle;
    private User mUser;
    private DatabaseReference mDatabaseReference;
    private boolean showBrumors;    // will be false after resume
    // shared prefs
    private SharedPreferences prefs;
    long currentVisiblePosition = 0;


    public FeedListFragment()
    {
        // Required empty public constructor
    }

    public static FeedListFragment newInstance(String myUidParam, String whatToDisplay,
                                               String otherUidParam, Bundle searchParam)
    {
        FeedListFragment fragment = new FeedListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_UID_PARAM, myUidParam);
        args.putString(ARG_OTHER_UID_PARAM, otherUidParam);
        args.putString(ARG_CONTENT_PARAM, whatToDisplay);
        args.putBundle(ARG_BUNDLE_PARAM, searchParam);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        mainView = inflater.inflate(R.layout.fragment_feed_list, container, false);

        mNoItemsTV = (TextView) mainView.findViewById(R.id.no_feed_items_found);
        mListSRL = (SwipeRefreshLayout) mainView.findViewById(R.id.feed_srl);
        mRecyclerView = (RecyclerView) mainView.findViewById(R.id.feed_rv);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        prefs = getContext().getSharedPreferences(PREFS_NAME, MODE_PRIVATE);

        showBrumors = true;
        mNoItemsTV.setVisibility(View.VISIBLE);

        //initializeUser();

        return mainView;
    }

    private void hideFabOnScroll()
    {
        // hiding fab on scroll
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener()
        {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy)
            {
                if (dy > 0)
                    ((FloatingActionMenu) getActivity().findViewById(R.id.float_menu)).hideMenu(true);
                else if (dy < 0)
                    ((FloatingActionMenu) getActivity().findViewById(R.id.float_menu)).showMenu(true);
            }
        });
    }

    private void initializeUser()
    {
        mDatabaseReference = FirebaseDatabase.getInstance().getReference();

        mDatabaseReference.child(Const.USERS).child(myUidParam).addListenerForSingleValueEvent(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                mUser = dataSnapshot.getValue(User.class);

                showItems();
            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {
            }
        });
    }


    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
        {
            myUidParam = getArguments().getString(ARG_UID_PARAM);
            otherUidParam = getArguments().getString(ARG_OTHER_UID_PARAM);
            mWhatToDisplay = getArguments().getString(ARG_CONTENT_PARAM);
            mBundle = getArguments().getBundle(ARG_BUNDLE_PARAM);
        }
    }

    @Override
    public void onResume()
    {
        super.onResume();
//        if (!mWhatToDisplay.equals(FEED_BRUMORS) todo if likes are done
//                || showBrumors)
            initializeUser();

    }


    @Override
    public void onPause()
    {
        super.onPause();

        currentVisiblePosition = ((LinearLayoutManager)
                mRecyclerView.getLayoutManager()).findFirstCompletelyVisibleItemPosition();
    }

    private void showItems()
    {
        // prepare swipe refresh layout
        mListSRL.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener()
        {
            @Override
            public void onRefresh()
            {
                ConnectivityManager conMgr = (ConnectivityManager) getActivity()
                        .getApplication().getSystemService(Context.CONNECTIVITY_SERVICE);
                {
                    NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                    if (netInfo != null)

                        executeAndDisplay();

                    else
                        Toast.makeText(getContext(), getString(R.string.error_internet), Toast.LENGTH_SHORT).show();
                }
                mListSRL.setRefreshing(false);
            }
        });
        mListSRL.setColorSchemeResources(android.R.color.holo_blue_dark, android.R.color.holo_red_dark);

        executeAndDisplay();
    }

    private void executeAndDisplay()
    {
        if (mWhatToDisplay.equals(FEED_BRUMORS))
            new BrumorsTask().execute();
//        if (mWhatToDisplay.equals(Const.FEED_STORIES)) todo v2
//            new FriendsStoriesTask().execute();
        if (mWhatToDisplay.equals(Const.FEED_HANGOUTS_ALL)
                || mWhatToDisplay.equals(Const.FEED_HANGOUTS_POPULAR)
                || mWhatToDisplay.equals(Const.FEED_HANGOUTS_JOINED)
                || mWhatToDisplay.equals(Const.SEARCH_HANGOUT))
            new HangoutsTask().execute();
        if (mWhatToDisplay.equals(Const.FEED_BUYKENT_EDUCATION)
                || mWhatToDisplay.equals(Const.FEED_BUYKENT_TECH)
                || mWhatToDisplay.equals(Const.FEED_BUYKENT_OTHER)
                || mWhatToDisplay.equals(Const.FEED_BUYKENT)
                || mWhatToDisplay.equals(Const.SEARCH_BUYKENT))
            new BuykentTask().execute();
    }



    private class BrumorsTask extends AsyncTask<Void, Void, Void>
    {
        @Override
        protected Void doInBackground(final Void... params)
        {
            showBrumors = false;
            final List<Post> brumorList = new ArrayList<>();
            final PostRecycler.PostAdapter[] adapter = new PostRecycler.PostAdapter[1];

            //final String[] languages = getContext().getResources().getStringArray(R.array.brumor_languages_array);
            final String[] languages = {"International (English Language only)", "Turkish"};
            final boolean trBrumors = prefs.getBoolean(Const.SHARED_BRUMORS_TURKISH, true);
            final boolean enBrumors = prefs.getBoolean(Const.SHARED_BRUMORS_ENGLISH, true);
            final boolean friendsPosts = prefs.getBoolean(Const.SHARED_FRIENDS_POSTS, true);

            // showing my friends' posts
            if (friendsPosts)
                for (Map.Entry<String, String> friendMap : mUser.getFriendsList().entrySet())
                {
                    mDatabaseReference.child(Const.USERS).child(friendMap.getValue())
                            .child(Const.POSTS_LISTS).addListenerForSingleValueEvent(new ValueEventListener()
                    {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot)
                        {
                            for (DataSnapshot postsSnap : dataSnapshot.getChildren())
                            {
                                Post post = postsSnap.getValue(Post.class);

                                if (!(post != null && post.isAnonymous()))
                                    brumorList.add(post);
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError)
                        {

                        }
                    });
                }


            // showing whole bilkent's brumors
            mDatabaseReference.child(Const.BRUMORS).addListenerForSingleValueEvent(new ValueEventListener()
            {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot)
                {
                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren())
                    {
                        Post everyPost = postSnapshot.getValue(Post.class);

                        if (enBrumors)
                            if (everyPost.getLanguage().equals(languages[0]))    // eng
                                brumorList.add(everyPost);
                        if (trBrumors)
                            if (everyPost.getLanguage().equals(languages[1]))    // tr
                                brumorList.add(everyPost);
                    }

                    Collections.sort(brumorList, new Comparator<Post>()
                    {
                        @Override
                        public int compare(Post p1, Post p2)
                        {
                            return p2.getTimeSent().toString()
                                    .compareTo(p1.getTimeSent().toString());
                        }
                    });

                    mNoItemsTV.setVisibility(View.GONE);

                    adapter[0] = new PostRecycler(myUidParam, myUidParam).new PostAdapter(brumorList);

                    mRecyclerView.setAdapter(adapter[0]);

                    if (brumorList.size() == 0)
                    {
                        mNoItemsTV.setText("nothing found");
                        mNoItemsTV.setVisibility(View.VISIBLE);
                    } else
                        mListSRL.setVisibility(View.VISIBLE);

                    ((LinearLayoutManager) mRecyclerView.getLayoutManager()).scrollToPosition((int) currentVisiblePosition);
                    currentVisiblePosition = 0;
                }

                @Override
                public void onCancelled(DatabaseError databaseError)
                {
                }
            });

            return null;
        }

        @Override
        protected void onPostExecute(Void result)
        {
        }
    }


    /**
     * Showing secrets, todo v2 bugs: have to touch the screen to display secrets,
     * todo v2      have to touch the secret to redraw likes
     */
    private class FriendsStoriesTask extends AsyncTask<Void, Void, Void>
    {
        @Override
        protected Void doInBackground(final Void... params)
        {
            final List<Post> secrets = new ArrayList<>();
            final PostRecycler.PostAdapter[] adapter = new PostRecycler.PostAdapter[1];

            // adding my secrets
            for (Map.Entry<String, Post> post : mUser.getPostsList().entrySet())
                if (post.getValue().getType().equals(Const.POST_TYPE_SECRET))
                    secrets.add(post.getValue());

            for (Map.Entry<String, String> friendId : mUser.getFriendsList().entrySet())
            {
                // get each friend's story post
                mDatabaseReference.child(Const.USERS).child(friendId.getValue())
                        .child(Const.POSTS_LISTS).addListenerForSingleValueEvent(new ValueEventListener()
                {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot)
                    {

                        for (DataSnapshot postSnapshot : dataSnapshot.getChildren())
                        {
                            Post everyPost = postSnapshot.getValue(Post.class);

                            if (everyPost.getType().equals(Const.POST_TYPE_SECRET))
                                secrets.add(everyPost);
                        }

                        // sorting
                        Collections.sort(secrets, new Comparator<Post>()
                        {
                            @Override
                            public int compare(Post p1, Post p2)
                            {
                                return p2.getTimeSent().toString()
                                        .compareTo(p1.getTimeSent().toString());
                            }
                        });

                        mNoItemsTV.setVisibility(View.GONE);

                        adapter[0] = new PostRecycler(myUidParam, myUidParam).new PostAdapter(secrets);
                        mRecyclerView.setAdapter(adapter[0]);

                        if (secrets.size() == 0)
                        {
                            mNoItemsTV.setText("nothing found");
                            mNoItemsTV.setVisibility(View.VISIBLE);
                        } else
                            mListSRL.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError)
                    {
                    }
                });
            }
            return null;
        }
    }

    private class HangoutsTask extends AsyncTask<Void, Void, Void>
    {

        @Override
        protected Void doInBackground(Void... params)
        {
            final List<Hangout> hangouts = new ArrayList<>();
            final HangoutRecycler.HangoutAdapter[] adapter = new HangoutRecycler.HangoutAdapter[1];

            final boolean time = prefs.getBoolean(Const.SHARED_HANGOUT_TIME, true);
            final boolean participantNum = prefs.getBoolean(Const.SHARED_HANGOUT_PEOPLE, false);

            // just displaying hangouts
            if (!mWhatToDisplay.equals(Const.SEARCH_HANGOUT))
            {
                mDatabaseReference.child(Const.HANGOUTS).addListenerForSingleValueEvent(new ValueEventListener()
                {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot)
                    {
                        // all hangouts
                        if (mWhatToDisplay.equals(Const.FEED_HANGOUTS_ALL))
                        {
                            for (DataSnapshot postSnapshot : dataSnapshot.getChildren())
                            {
                                Hangout hangout = postSnapshot.getValue(Hangout.class);
                                if (hangout.isOpenOrClosed() && !hangout.isPopular())
                                    hangouts.add(hangout);
                            }

                            //hideFabOnScroll();

                        } else if (mWhatToDisplay.equals(Const.FEED_HANGOUTS_POPULAR))
                        {
                            for (DataSnapshot postSnapshot : dataSnapshot.getChildren())
                            {
                                Hangout hangout = postSnapshot.getValue(Hangout.class);
                                if (hangout.isPopular() && hangout.isOpenOrClosed())
                                    hangouts.add(hangout);
                            }

                            //hideFabOnScroll();

                        } else if (mWhatToDisplay.equals(Const.FEED_HANGOUTS_JOINED))
                        {
                            for (DataSnapshot postSnapshot : dataSnapshot.getChildren())
                            {
                                Hangout hangout = postSnapshot.getValue(Hangout.class);

                                // displaying joined if it's me (displaying closed too)
                                if (otherUidParam.equals(myUidParam))
                                {
                                    if (hangout.getMembers().containsKey(otherUidParam))
                                        hangouts.add(hangout);
                                } else
                                {
                                    if (hangout.getMembers().containsKey(otherUidParam))
                                    {
                                        if (hangout.isOpenOrClosed())
                                            hangouts.add(hangout);
                                    }
                                }


                            }
                        }

                        mNoItemsTV.setVisibility(View.GONE);

                        // sorting
                        Collections.sort(hangouts, new Comparator<Hangout>()
                        {
                            @Override
                            public int compare(Hangout o1, Hangout o2)
                            {
                                // by members: more -> top
                                if (!time && participantNum)
                                    return Integer.toString(o2.getMembers().size())
                                            .compareTo(Integer.toString(o1.getMembers().size()));
                                    // by time (key): newest first
                                else
                                    return o2.getKey().compareToIgnoreCase(o1.getKey());
                            }
                        });

                        adapter[0] = new HangoutRecycler(myUidParam, otherUidParam).new HangoutAdapter(hangouts);
                        mRecyclerView.setAdapter(adapter[0]);

                        if (hangouts.size() == 0)
                        {
                            mNoItemsTV.setText("nothing found");
                            mNoItemsTV.setVisibility(View.VISIBLE);
                        } else
                            mListSRL.setVisibility(View.VISIBLE);

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError)
                    {
                    }
                });
            }

            // searching
            else
            {
                mDatabaseReference.child(Const.HANGOUTS).addListenerForSingleValueEvent(new ValueEventListener()
                {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot)
                    {
                        // first launch of search activity is null -> display nothing found
                        if (mBundle != null)
                        {
                            String request = mBundle.getString(Const.PARAM_SEARCH_REQUEST);

                            // getting through all hangouts
                            for (DataSnapshot hangoutSnap : dataSnapshot.getChildren())
                            {
                                Hangout currentHangout = hangoutSnap.getValue(Hangout.class);
                                // all searching hangouts should be open
                                if (currentHangout.isOpenOrClosed())
                                {
                                    // searching request in tags
                                    List<String> currentTags = currentHangout.getTags();

                                    for (String currentTag : currentTags)
                                    {
                                        if (currentTag.toLowerCase().contains(request.toLowerCase()))
                                        {
                                            hangouts.add(currentHangout);
                                            break;
                                        }
                                    }

                                }
                            }
                        }

                        mNoItemsTV.setVisibility(View.GONE);

                        adapter[0] = new HangoutRecycler(myUidParam, otherUidParam).new HangoutAdapter(hangouts);
                        mRecyclerView.setAdapter(adapter[0]);

                        if (hangouts.size() == 0)
                        {
                            mNoItemsTV.setText("nothing found");
                            mNoItemsTV.setVisibility(View.VISIBLE);
                        } else
                            mListSRL.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError)
                    {
                    }
                });
            }

            return null;
        }
    }

    private class BuykentTask extends AsyncTask<Void, Void, Void>
    {

        final List<Product> products = new ArrayList<>();
        final BuykentRecycler.ProductAdapter[] adapter = new BuykentRecycler.ProductAdapter[1];

        @Override
        protected Void doInBackground(Void... params)
        {
            // displaying buykent in profile
            if (mWhatToDisplay.equals(Const.FEED_BUYKENT))
            {
                mDatabaseReference.child(Const.BUYKENT).addListenerForSingleValueEvent(new ValueEventListener()
                {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot)
                    {
                        for (DataSnapshot categorySnapshot : dataSnapshot.getChildren())
                        {
                            for (DataSnapshot postSnapshot : categorySnapshot.getChildren())
                            {
                                if (postSnapshot.child(Const.UPLOADER_ID).getValue(String.class)
                                        .equals(otherUidParam))
                                {
                                    Product product = postSnapshot.getValue(Product.class);
                                    products.add(product);
                                }
                            }
                        }

                        mNoItemsTV.setVisibility(View.GONE);

                        adapter[0] = new BuykentRecycler(myUidParam, otherUidParam).new ProductAdapter(products);
                        mRecyclerView.setAdapter(adapter[0]);

                        if (products.size() == 0)
                        {
                            mNoItemsTV.setText("nothing found");
                            mNoItemsTV.setVisibility(View.VISIBLE);
                        } else
                            mListSRL.setVisibility(View.VISIBLE);

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError)
                    {

                    }
                });
            }

            // wiring data for Education fragment
            if (mWhatToDisplay.equals(Const.FEED_BUYKENT_EDUCATION))
            {
                mDatabaseReference.child(Const.BUYKENT)
                        .child(Const.BUYKENT_EDUCATION).addListenerForSingleValueEvent(new ValueEventListener()
                {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot)
                    {
                        for (DataSnapshot postSnapshot : dataSnapshot.getChildren())
                        {
                            Product product = postSnapshot.getValue(Product.class);
                            products.add(product);
                        }

                        mNoItemsTV.setVisibility(View.GONE);

                        adapter[0] = new BuykentRecycler(myUidParam, otherUidParam).new ProductAdapter(products);
                        mRecyclerView.setAdapter(adapter[0]);

                        if (products.size() == 0)
                        {
                            mNoItemsTV.setText("nothing found");
                            mNoItemsTV.setVisibility(View.VISIBLE);
                        } else
                            mListSRL.setVisibility(View.VISIBLE);

                        hideFabOnScroll();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError)
                    {

                    }
                });
            }

            // wiring data for Tech & vehicle fragment
            if (mWhatToDisplay.equals(Const.FEED_BUYKENT_TECH))
            {
                mDatabaseReference.child(Const.BUYKENT)
                        .child(Const.BUYKENT_TECH).addListenerForSingleValueEvent(new ValueEventListener()
                {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot)
                    {
                        for (DataSnapshot postSnapshot : dataSnapshot.getChildren())
                        {
                            Product product = postSnapshot.getValue(Product.class);
                            products.add(product);
                        }

                        mNoItemsTV.setVisibility(View.GONE);

                        adapter[0] = new BuykentRecycler(myUidParam, otherUidParam).new ProductAdapter(products);
                        mRecyclerView.setAdapter(adapter[0]);

                        if (products.size() == 0)
                        {
                            mNoItemsTV.setText("nothing found");
                            mNoItemsTV.setVisibility(View.VISIBLE);
                        } else
                            mListSRL.setVisibility(View.VISIBLE);

                        hideFabOnScroll();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError)
                    {

                    }
                });
            }

            // wiring data for Other fragment
            if (mWhatToDisplay.equals(Const.FEED_BUYKENT_OTHER))
            {
                mDatabaseReference.child(Const.BUYKENT)
                        .child(Const.BUYKENT_OTHER).addListenerForSingleValueEvent(new ValueEventListener()
                {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot)
                    {
                        for (DataSnapshot postSnapshot : dataSnapshot.getChildren())
                        {
                            Product product = postSnapshot.getValue(Product.class);
                            products.add(product);
                        }

                        mNoItemsTV.setVisibility(View.GONE);

                        adapter[0] = new BuykentRecycler(myUidParam, otherUidParam).new ProductAdapter(products);
                        mRecyclerView.setAdapter(adapter[0]);

                        if (products.size() == 0)
                        {
                            mNoItemsTV.setText("nothing found");
                            mNoItemsTV.setVisibility(View.VISIBLE);
                        } else
                            mListSRL.setVisibility(View.VISIBLE);

                        hideFabOnScroll();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError)
                    {

                    }
                });
            }

            // displaying products in search
            if (mWhatToDisplay.equals(Const.SEARCH_BUYKENT))
            {
                // download all buykent titles and check
                mDatabaseReference.child(Const.BUYKENT).addListenerForSingleValueEvent(new ValueEventListener()
                {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot)
                    {

                        if (mBundle != null)
                        {
                            String request = mBundle.getString(Const.PARAM_SEARCH_REQUEST);

                            // downloading Education titles
                            for (DataSnapshot educationSnap : dataSnapshot.child(Const.BUYKENT_EDUCATION).getChildren())
                            {
                                Product currentP = educationSnap.getValue(Product.class);
                                if (currentP.getTitle().toLowerCase().contains(request.toLowerCase()))
                                    products.add(currentP);
                            }

                            // downloading Tech titles
                            for (DataSnapshot techOther : dataSnapshot.child(Const.BUYKENT_TECH).getChildren())
                            {
                                Product currentP = techOther.getValue(Product.class);
                                if (currentP.getTitle().toLowerCase().contains(request.toLowerCase()))
                                    products.add(currentP);
                            }

                            // downloading Other titles
                            for (DataSnapshot otherSnap : dataSnapshot.child(Const.BUYKENT_OTHER).getChildren())
                            {
                                Product currentP = otherSnap.getValue(Product.class);
                                if (currentP.getTitle().toLowerCase().contains(request.toLowerCase()))
                                    products.add(currentP);
                            }

                        }

                        // setting list view

                        mNoItemsTV.setVisibility(View.GONE);

                        adapter[0] = new BuykentRecycler(myUidParam, otherUidParam).new ProductAdapter(products);
                        mRecyclerView.setAdapter(adapter[0]);

                        if (products.size() == 0)
                        {
                            mNoItemsTV.setText("nothing found");
                            mNoItemsTV.setVisibility(View.VISIBLE);
                        } else
                            mListSRL.setVisibility(View.VISIBLE);

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError)
                    {

                    }
                });

            }

            // should not happen:
            return null;
        }
    }

}
