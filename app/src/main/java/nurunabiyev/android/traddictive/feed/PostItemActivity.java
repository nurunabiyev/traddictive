package nurunabiyev.android.traddictive.feed;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import nurunabiyev.android.traddictive.Const;
import nurunabiyev.android.traddictive.R;
import nurunabiyev.android.traddictive.objects.Comment;
import nurunabiyev.android.traddictive.objects.Post;
import nurunabiyev.android.traddictive.objects.User;
import nurunabiyev.android.traddictive.profile.ProfileActivity;

public class PostItemActivity extends AppCompatActivity
{

    private final String TAG = "postactivity";
    // view
    private TextView postedUserNameTV;
    private TextView postedTimeTV;
    private LinearLayout postedUserLL;
    private CircleImageView postedUserCIV;
    private RecyclerView commentsRV;
    private SwipeRefreshLayout mProfileSRL;
    // user
    private String myUid;
    private String postedUserUid; // other uid
    private String postedUserName;
    // post
    private Post mPost;
    private String postKey;
    private String postId;
    private String postForFriendsOrBilkent;
    private String postedUserProfileId;
    // firebase
    private DatabaseReference mDatabaseReference;
    private StorageReference mStorageReference;
    private User mUserMe;
    private User mPostedUser;
    // comments
    private ArrayList<Comment> mComments;
    private CommentAdapter mCommentAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_post);

        initializeView();
    }

    private void initializeView()
    {
        // initialize toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.post_activity_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // change the color of gradient
        if (android.os.Build.VERSION.SDK_INT >= 21)
        {
            Window window = PostItemActivity.this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.editorColorPrimaryDark));
        }

        // get intent stuff
        Intent intent = getIntent();
        myUid = intent.getStringExtra(Const.UID);
        postedUserUid = intent.getStringExtra(Const.OTHER_UID);
        postedUserName = intent.getStringExtra(Const.FULL_NAME);
        postedUserProfileId = intent.getStringExtra(Const.PROFILE_PHOTO_ID);
        postId = intent.getStringExtra(Const.POST_ID);
        postKey = intent.getStringExtra(Const.POST_KEY);
        postForFriendsOrBilkent = intent.getStringExtra(Const.POST_FOR_FRIENDS_OR_BILKENT);

        Log.d(TAG, "postforfriends " + postForFriendsOrBilkent);

        // initialize view
        mProfileSRL = (SwipeRefreshLayout) findViewById(R.id.profile_srl);
        mProfileSRL.setEnabled(false); // todo v2

        ViewStub addonStub = (ViewStub) findViewById(R.id.addon_section_vs);
        addonStub.setLayoutResource(R.layout.section_addon);
        View addonView = addonStub.inflate();
        postedUserNameTV = (TextView) addonView.findViewById(R.id.posted_username_tv);
        postedUserLL = (LinearLayout) addonView.findViewById(R.id.posted_user_ll);
        postedTimeTV = (TextView) addonView.findViewById(R.id.posted_time_tv);
        postedUserCIV = (CircleImageView) addonView.findViewById(R.id.posted_user_civ);

        ViewStub commentStub = (ViewStub) findViewById(R.id.comment_section_vs);
        commentStub.setLayoutResource(R.layout.section_comments);
        View postTypeView = commentStub.inflate();
        commentsRV = (RecyclerView) postTypeView.findViewById(R.id.comments_rv);

        mComments = new ArrayList<>();

        wireDatabase();
    }

    private void wireDatabase()
    {
        mDatabaseReference = FirebaseDatabase.getInstance().getReference();
        mStorageReference = FirebaseStorage.getInstance().getReference();

        whosProfileIsThis();


        // if for friends -> load post from profile
        if (postForFriendsOrBilkent.equals("true"))
        {

            // wiring post info part
            postedUserNameTV.setText(postedUserName);

            if(postedUserProfileId != null)
            {
                StorageReference storageReference = mStorageReference.child(Const.USERS).child(postedUserUid).child(Const.PHOTOS_LISTS)
                        .child(Const.PHOTO_THUMBNAIL + postedUserProfileId);
                Glide.with(getApplicationContext())
                        .using(new FirebaseImageLoader())
                        .load(storageReference)
                        .into(postedUserCIV);
            }
            else
                postedUserCIV.setImageDrawable(getResources().getDrawable(R.drawable.default_avatar));

            // opening profile after clicking on user civ
            postedUserCIV.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    Intent intent = new Intent(PostItemActivity.this, ProfileActivity.class);
                    intent.putExtra(Const.FULL_NAME, postedUserName);
                    intent.putExtra(Const.UID, myUid);
                    intent.putExtra(Const.OTHER_UID, postedUserUid);
                    startActivity(intent);
                }
            });
            // same
            postedUserLL.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    Intent intent = new Intent(PostItemActivity.this, ProfileActivity.class);
                    intent.putExtra(Const.FULL_NAME, postedUserName);
                    intent.putExtra(Const.UID, myUid);
                    intent.putExtra(Const.OTHER_UID, postedUserUid);
                    startActivity(intent);
                }
            });

            mDatabaseReference.child(Const.USERS)
                    .child(postedUserUid).addListenerForSingleValueEvent(new ValueEventListener()
            {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot)
                {
                    mPostedUser = dataSnapshot.getValue(User.class);
                    mPost = mPostedUser.getPostsList().get(postKey);

                    Log.d(TAG, "posted user" + mPostedUser);
                    // date
                    Date sentTimeDate = new Date(Long.parseLong(mPost.getTimeSent().toString()));
                    String dateText = (String) DateFormat.format("d MMMM HH:mm", sentTimeDate);
                    if (mPost.isForFriendsOrBilkent())
                        postedTimeTV.setText(dateText);
                    else
                    {
                        postedTimeTV.setText("by " + mPost.getUploaderName() + " at " + dateText);
                        postedUserCIV.setImageDrawable(getResources().getDrawable(R.drawable.ic_brumor_green));
                        postedUserCIV.setBorderColor(getResources().getColor(R.color.brumor_green));
                    }

                    // loads post itself
                    loadPost();

                    // load comment
                    updateUi();
                }

                @Override
                public void onCancelled(DatabaseError databaseError)
                {
                }
            });
        }


        // if for whole bilkent -> load from brumors
        else
        {
            mDatabaseReference.child(Const.BRUMORS)
                    .child(postKey).addListenerForSingleValueEvent(new ValueEventListener()
            {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot)
                {
                    mPost = dataSnapshot.getValue(Post.class);

                    Date sentTimeDate = new Date(Long.parseLong(mPost.getTimeSent().toString()));
                    String dateText = (String) DateFormat.format("d MMMM HH:mm", sentTimeDate);

                    postedUserNameTV.setText(Const.BRUMOR_POST_NAME + " #" + mPost.getPostCount());
                    // anonymous or not
                    if(!mPost.isAnonymous())
                        postedTimeTV.setText("by " + mPost.getUploaderName() + " at " + dateText);
                    else
                        postedTimeTV.setText("by Anonymous" + " at " + dateText);
                    postedUserCIV.setImageDrawable(getResources().getDrawable(R.drawable.ic_brumor_green));
                    postedUserCIV.setBorderColor(getResources().getColor(R.color.brumor_green));

                    // loads post itself
                    loadPost();

                    // load comment
                    updateUi();
                }

                @Override
                public void onCancelled(DatabaseError databaseError)
                {

                }
            });
        }

    }

    private void updateUi()
    {
        // prepare swipe refresh layout
        mProfileSRL.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener()
        {
            @Override
            public void onRefresh()
            {
                ConnectivityManager conMgr = (ConnectivityManager) getApplication().getSystemService(Context.CONNECTIVITY_SERVICE);
                {
                    NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                    if (netInfo != null)
                    {
                        updateCommentRecycler();
                        mCommentAdapter.notifyDataSetChanged();
                    }
                    else
                        Toast.makeText(PostItemActivity.this, "No internet connection", Toast.LENGTH_SHORT).show();
                }
                mProfileSRL.setRefreshing(false);
            }
        });
        mProfileSRL.setColorSchemeResources(android.R.color.holo_blue_dark, android.R.color.holo_red_dark);

        // the rest is recycler view
        updateCommentRecycler();
    }

    private void updateCommentRecycler()
    {
        DatabaseReference postedUserRef = mDatabaseReference.child(Const.USERS).child(postedUserUid);
        mComments.clear();
        commentsRV.setLayoutManager(new LinearLayoutManager(PostItemActivity.this));

        postedUserRef.addListenerForSingleValueEvent(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                mComments.clear();

                // filling mNotifications with all notifications
                for (Map.Entry<String, Comment> comment : mPost.getComments().entrySet())
                    mComments.add(comment.getValue());

                // sorting comments
                Collections.sort(mComments, new Comparator<Post>()
                {
                    @Override
                    public int compare(Post p1, Post p2)
                    {
                        int p1vote = p1.getLikes().size() + p1.getDislikes().size();
                        int p2vote = p2.getLikes().size() + p2.getDislikes().size();
                        return Integer.toString(p2vote)
                                .compareTo(Integer.toString(p1vote));
                    }
                });

                // setting adapter
                mCommentAdapter = new CommentAdapter(mComments);
                commentsRV.setAdapter(mCommentAdapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {

            }
        });
    }

    private void whosProfileIsThis()
    {
        // display post as my post, todo v2 to delete post, report user, etc
        if (myUid.equals(postedUserUid))
        {

        } else
        {

        }

        mDatabaseReference.child(Const.USERS).child(myUid).addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                mUserMe = dataSnapshot.getValue(User.class);
            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {

            }
        });
    }

    private void loadPost()
    {
        // comment part
        EditText writeCommentET = (EditText) findViewById(R.id.write_comment_et);

        // notify user if his comment anonymous or public
        if (mPost.isAnonymous())
        {
            writeCommentET.setHint(getString(R.string.write_comment_anonymously));
        } else
        {
            writeCommentET.setHint(getString(R.string.write_comment_not_anonymously));
        }

        // change <include layout> regarding the posttype
        ViewStub stub = (ViewStub) findViewById(R.id.post_type_layout);
        String postType = mPost.getType();
        switch (postType)
        {
            case Const.POST_TYPE_PHOTO:
                inflatePostAsPhoto(stub);
                break;
            case Const.POST_TYPE_BRUMOR:
                inflatePostAsBrumor(stub, 0);
                break;
            case Const.POST_TYPE_CHANGE_STATUS:
                inflatePostAsBrumor(stub, 1);
                break;
            case Const.POST_TYPE_CHANGE_RELATION_STATUS:
                inflatePostAsBrumor(stub, 2);
                break;
            case Const.POST_TYPE_HANGOUT:
                inflatePostAsBrumor(stub, 3);
                break;
            // todo v2 for more post types

        }
    }

    private void inflatePostAsPhoto(ViewStub stub)
    {
        stub.setLayoutResource(R.layout.post_type_photo);
        View postTypeView = stub.inflate();

        // initializing photo views
        final ImageView postedPhotoIV = (ImageView) postTypeView.findViewById(R.id.posted_photo_iv);
        final ImageView photoLikeIV = (ImageView) postTypeView.findViewById(R.id.photo_like_iv);
        ImageView photoCommentIV = (ImageView) postTypeView.findViewById(R.id.photo_comment_iv);
        final TextView photoLikesTV = (TextView) postTypeView.findViewById(R.id.photo_likes_tv);
        TextView photoCommentsTV = (TextView) postTypeView.findViewById(R.id.photo_comments_tv);
        TextView postedPhotoTextTV = (TextView) postTypeView.findViewById(R.id.posted_photo_text_tv);

        // setting numbers
        photoLikesTV.setText(((mPost.getLikes() == null) ? "0" : "" + mPost.getLikes().size()));
        photoCommentsTV.setText(((mPost.getComments() == null) ? "0" : "" + mPost.getComments().size()));

        // color like button if needed
        for (Map.Entry<String, String> likedPerson : mPost.getLikes().entrySet())
        {
            if (likedPerson.getValue().equals(myUid))
            {
                photoLikeIV.setColorFilter(ContextCompat.getColor(PostItemActivity.this, R.color.clicked_blue));
                photoLikesTV.setTextColor(ContextCompat.getColor(PostItemActivity.this, R.color.clicked_blue));
                photoLikeIV.setEnabled(false);
                photoLikeIV.setClickable(false);
            }
        }

        postedPhotoTextTV.setText(mPost.getText());

        StorageReference storageReference = mStorageReference.child(Const.USERS).child(postedUserUid).child(Const.PHOTOS_LISTS)
                .child(Const.PHOTO_ORIGINAL + postId);
        Glide.with(getApplicationContext())
                .using(new FirebaseImageLoader())
                .load(storageReference)
                .into(postedPhotoIV);

        // liking photo
        photoLikeIV.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                // changing the color and incrementing likes count
                photoLikeIV.setColorFilter(ContextCompat.getColor(PostItemActivity.this, R.color.clicked_blue));
                photoLikesTV.setTextColor(ContextCompat.getColor(PostItemActivity.this, R.color.clicked_blue));

                int newLikeNumber = Integer.parseInt(photoLikesTV.getText().toString()) + 1;
                photoLikesTV.setText(Integer.toString(newLikeNumber));

                // sending my id to list of liked users to firebase database
                mDatabaseReference.child(Const.USERS).child(postedUserUid).child(Const.POSTS_LISTS)
                        .child(postKey).child(Const.POST_LIKES).push()
                        .setValue(myUid);

                // making like button inactive
                photoLikeIV.setEnabled(false);
                photoLikeIV.setClickable(false);
            }
        });

    }

    private void inflatePostAsBrumor(ViewStub stub, int changeType)
    {
        stub.setLayoutResource(R.layout.post_type_brumor);
        View postTypeView = stub.inflate();

        // initializing brumor views
        final ImageView brumorLikeIV = (ImageView) postTypeView.findViewById(R.id.brumor_like_iv);
        final ImageView brumorDislikeIV = (ImageView) postTypeView.findViewById(R.id.brumor_dislike_iv);
        ImageView brumorCommentIV = (ImageView) postTypeView.findViewById(R.id.photo_comment_iv);
        final TextView brumorLikesTV = (TextView) postTypeView.findViewById(R.id.brumor_likes_tv);
        final TextView brumorDislikesTV = (TextView) postTypeView.findViewById(R.id.brumor_dislikes_tv);
        TextView brumorCommentsTV = (TextView) postTypeView.findViewById(R.id.brumor_comments_tv);
        TextView postedBrumorTextTV = (TextView) postTypeView.findViewById(R.id.posted_brumor_text_tv);

        // setting numbers
        brumorLikesTV.setText(((mPost.getLikes() == null) ? "0" : "" + mPost.getLikes().size()));
        brumorDislikesTV.setText(((mPost.getDislikes() == null) ? "0" : "" + mPost.getDislikes().size()));
        brumorCommentsTV.setText(((mPost.getComments() == null) ? "0" : "" + mPost.getComments().size()));

        if (changeType == 0)
            postedBrumorTextTV.setText(mPost.getText());
        else if (changeType == 1)
        {
            // making name of the sender Bold
            SpannableStringBuilder str = new SpannableStringBuilder(mPost.getText());
            str.setSpan
                    (
                            new android.text.style.StyleSpan(Typeface.ITALIC),
                            0,
                            getString(R.string.has_updated_status).length(),
                            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                    );
            postedBrumorTextTV.setText(str);
        } else if (changeType == 2)
        {
            // making name of the sender Bold
            SpannableStringBuilder str = new SpannableStringBuilder(mPost.getText());
            str.setSpan
                    (
                            new android.text.style.StyleSpan(Typeface.ITALIC),
                            0,
                            getString(R.string.has_updated_relation_status).length(),
                            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                    );
            postedBrumorTextTV.setText(str);
        }
        else if (changeType == 3)
        {
            // making name of the sender Bold
            SpannableStringBuilder str = new SpannableStringBuilder(mPost.getText());
            str.setSpan
                    (
                            new android.text.style.StyleSpan(Typeface.ITALIC),
                            0,
                            getString(R.string.has_created_hangout).length(),
                            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                    );
            postedBrumorTextTV.setText(str);
        }

        // setting like/dislike color
        for (Map.Entry<String, String> likedPerson : mPost.getLikes().entrySet())
        {
            if (likedPerson.getValue().equals(myUid))
            {
                brumorLikeIV.setColorFilter(ContextCompat.getColor(PostItemActivity.this, R.color.clicked_blue));
                brumorLikesTV.setTextColor(ContextCompat.getColor(PostItemActivity.this, R.color.clicked_blue));
                brumorLikeIV.setEnabled(false);
                brumorLikeIV.setClickable(false);
                brumorDislikeIV.setEnabled(false);
                brumorDislikeIV.setClickable(false);
            }
        }

        for (Map.Entry<String, String> dislikedPerson : mPost.getDislikes().entrySet())
        {
            if (dislikedPerson.getValue().equals(myUid))
            {
                brumorDislikeIV.setColorFilter(ContextCompat.getColor(PostItemActivity.this, R.color.clicked_red));
                brumorDislikesTV.setTextColor(ContextCompat.getColor(PostItemActivity.this, R.color.clicked_red));
                brumorDislikeIV.setEnabled(false);
                brumorDislikeIV.setClickable(false);
                brumorLikeIV.setEnabled(false);
                brumorLikeIV.setClickable(false);
            }
        }


        // like/dislike listeners

        // liking comment
        brumorLikeIV.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                // changing the color and statically incrementing likes count
                brumorLikeIV.setColorFilter(ContextCompat.getColor(PostItemActivity.this, R.color.clicked_blue));
                brumorLikesTV.setTextColor(ContextCompat.getColor(PostItemActivity.this, R.color.clicked_blue));

                int newLikeNumber = Integer.parseInt(brumorLikesTV.getText().toString()) + 1;
                brumorLikesTV.setText(Integer.toString(newLikeNumber));

                // sending like to brumors or profile's postList
                if (postForFriendsOrBilkent.equals("false"))
                {
                    mDatabaseReference.child(Const.BRUMORS).child(mPost.getPostKey()).child(Const.POST_LIKES)
                            .push().setValue(myUid);
                } else
                {
                    // sending my id to list of liked users to firebase database
                    mDatabaseReference.child(Const.USERS).child(mPost.getUploaderId()).child(Const.POSTS_LISTS)
                            .child(mPost.getPostKey()).child(Const.POST_LIKES).push()
                            .setValue(myUid);
                }


                // making like button inactive
                brumorDislikeIV.setEnabled(false);
                brumorDislikeIV.setClickable(false);
                brumorLikeIV.setEnabled(false);
                brumorLikeIV.setClickable(false);
            }
        });

        // disliking comment
        brumorDislikeIV.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                // changing the color and statically incrementing likes count
                brumorDislikeIV.setColorFilter(ContextCompat.getColor(PostItemActivity.this, R.color.clicked_red));
                brumorDislikesTV.setTextColor(ContextCompat.getColor(PostItemActivity.this, R.color.clicked_red));

                int newLikeNumber = Integer.parseInt(brumorDislikesTV.getText().toString()) + 1;
                brumorDislikesTV.setText(Integer.toString(newLikeNumber));

                // sending dislike to brumors or profile's postList
                if (postForFriendsOrBilkent.equals("false"))
                {
                    mDatabaseReference.child(Const.BRUMORS).child(mPost.getPostKey()).child(Const.POST_DISLIKES)
                            .push().setValue(myUid);
                } else
                {
                    // sending my id to list of liked users to firebase database
                    mDatabaseReference.child(Const.USERS).child(mPost.getUploaderId()).child(Const.POSTS_LISTS)
                            .child(mPost.getPostKey()).child(Const.POST_DISLIKES).push()
                            .setValue(myUid);
                }

                // making like button inactive
                brumorDislikeIV.setEnabled(false);
                brumorDislikeIV.setClickable(false);
                brumorLikeIV.setEnabled(false);
                brumorLikeIV.setClickable(false);
            }
        });

    }


    // pressing send comment button
    public void sendComment(View view)
    {
        EditText writeCommentET = (EditText) findViewById(R.id.write_comment_et);
        // validate comment
        if (writeCommentET.getText().toString().length() < 2
                || writeCommentET.getLineCount() > 20)
        {
            Toast.makeText(PostItemActivity.this, getString(R.string.error_invalid_text_longshort), Toast.LENGTH_SHORT).show();
        }

        // creating and sending comment to firebase
        else
        {
            Comment newComment = new Comment();
            newComment.setText(writeCommentET.getText().toString());
            newComment.setUploaderId(myUid);
            newComment.setUploaderName(mUserMe.getFullName());
            newComment.setAnonymous(mPost.isAnonymous());
            newComment.setForFriendsOrBilkent(true);
            Object ts = ServerValue.TIMESTAMP;
            newComment.setTimeSent(ts);

            String commentKey = mDatabaseReference.push().getKey();

            newComment.setCommentKey(commentKey);

            // sending comment to brumors or profile's postList
            if (postForFriendsOrBilkent.equals("false"))
            {
                mDatabaseReference.child(Const.BRUMORS).child(postKey).child(Const.COMMENT_LIST)
                        .child(commentKey)
                        .setValue(newComment);
            } else
            {
                mDatabaseReference.child(Const.USERS).child(postedUserUid).child(Const.POSTS_LISTS).child(postKey).child(Const.COMMENT_LIST)
                        .child(commentKey)
                        .setValue(newComment);
            }

            writeCommentET.setText("");
            Toast.makeText(PostItemActivity.this, getString(R.string.posted), Toast.LENGTH_SHORT).show();

            // todo v2 decrement limits

            // adding this comment to array recycler view
            newComment.setTimeSent(System.currentTimeMillis());
            mComments.add(newComment);
            mCommentAdapter.notifyDataSetChanged();
        }
    }


    /**
     * Inner class Holder
     */
    private class CommentHolder extends RecyclerView.ViewHolder
    {
        private Comment mComment;

        private CircleImageView commentedUserCIV;
        private TextView commentedUserNameTV;
        private TextView commentTextTV;
        private TextView commentSentTimeTV;
        private TextView commentLikesTV;
        private TextView commentDislikesTV;
        private ImageView commentLikeIV;
        private ImageView commentDislikeIV;
        private CardView commentCV;


        private CommentHolder(View itemView)
        {
            super(itemView);

            commentedUserCIV = (CircleImageView) itemView.findViewById(R.id.commented_user_civ);
            commentedUserNameTV = (TextView) itemView.findViewById(R.id.commented_username_tv);
            commentTextTV = (TextView) itemView.findViewById(R.id.comment_text_tv);
            commentSentTimeTV = (TextView) itemView.findViewById(R.id.commented_time_tv);
            commentLikesTV = (TextView) itemView.findViewById(R.id.comment_likes_tv);
            commentDislikesTV = (TextView) itemView.findViewById(R.id.comment_dislikes_tv);
            commentLikeIV = (ImageView) itemView.findViewById(R.id.comment_like_iv);
            commentDislikeIV = (ImageView) itemView.findViewById(R.id.comment_dislike_iv);
            commentCV = (CardView) itemView.findViewById(R.id.item_comment_cv);
        }

        private void bindComment(Comment comment)
        {
            mComment = comment;

            if(!mComment.isAnonymous())
            {
                commentedUserNameTV.setText(mComment.getUploaderName());
//todo v2
//                // click listener -> shows likes/dislike
//                commentCV.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        Intent intent = new Intent(PostItemActivity.this, TabbedActivity.class);
//                        Bundle viewPagerSettings = new Bundle();
//                        viewPagerSettings.putInt(Const.PARAM_PAGE_COUNT, 2);
//                        viewPagerSettings.putString(Const.PARAM_PAGE_1, getString(R.string.liked_people));
//                        viewPagerSettings.putString(Const.PARAM_PAGE_2, getString(R.string.disliked_people));
//                        viewPagerSettings.putString(Const.UID, myUid);
//                        viewPagerSettings.putString(Const.OTHER_UID, mComment.getUploaderId());
//                        viewPagerSettings.putString(Const.POST_KEY, mComment.getPostKey());
//                        intent.putExtra(Const.PARAM_BUNDLE, viewPagerSettings);
//                        startActivity(intent);
//                    }
//                });
            }
            else
            {
                commentedUserNameTV.setText("Anonymous");
            }
            commentTextTV.setText(mComment.getText());
            commentLikesTV.setText(((mComment.getLikes() == null) ? "0" : "" + mComment.getLikes().size()));
            commentDislikesTV.setText((mComment.getDislikes() == null) ? "0" : "" + mComment.getDislikes().size());
            // date
            Date sentTimeDate = new Date(Long.parseLong(mComment.getTimeSent().toString()));
            String dateText = (String) DateFormat.format("d MMMM HH:mm", sentTimeDate);
            commentSentTimeTV.setText(dateText);

            // setting like/dislike color
            for (Map.Entry<String, String> likedPerson : mComment.getLikes().entrySet())
            {
                if (likedPerson.getValue().equals(myUid))
                {
                    commentLikeIV.setColorFilter(ContextCompat.getColor(PostItemActivity.this, R.color.clicked_blue));
                    commentLikesTV.setTextColor(ContextCompat.getColor(PostItemActivity.this, R.color.clicked_blue));
                    commentLikeIV.setEnabled(false);
                    commentLikeIV.setClickable(false);
                }
            }

            for (Map.Entry<String, String> dislikedPerson : mComment.getDislikes().entrySet())
            {
                if (dislikedPerson.getValue().equals(myUid))
                {
                    commentDislikeIV.setColorFilter(ContextCompat.getColor(PostItemActivity.this, R.color.clicked_red));
                    commentDislikesTV.setTextColor(ContextCompat.getColor(PostItemActivity.this, R.color.clicked_red));
                    commentDislikeIV.setEnabled(false);
                    commentDislikeIV.setClickable(false);
                }
            }

            // downloading and setting photo to image view

            // get the user who uploaded the comment
            if(!mComment.isAnonymous())
            {
                mDatabaseReference.child(Const.USERS).child(mComment.getUploaderId())
                        .child(Const.PROFILE_PHOTO_ID).addListenerForSingleValueEvent(new ValueEventListener()
                {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot)
                    {
                        String usersProfilePhoto = dataSnapshot.getValue(String.class);

                        if(usersProfilePhoto != null)
                        {
                            StorageReference storageReference = mStorageReference.child(Const.USERS)
                                    .child(mComment.getUploaderId()).child(Const.PHOTOS_LISTS)
                                    .child(Const.PHOTO_THUMBNAIL + usersProfilePhoto);
                            Glide.with(getApplicationContext())
                                    .using(new FirebaseImageLoader())
                                    .load(storageReference)
                                    .into(commentedUserCIV);
                        }
                        else
                            commentedUserCIV.setImageResource(R.drawable.default_avatar);

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError)
                    {

                    }
                });
            }
            else
            {
                commentedUserCIV.setImageDrawable(getResources().getDrawable(R.drawable.default_avatar));
            }

            commentedUserCIV.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    Intent intent = new Intent(PostItemActivity.this, ProfileActivity.class);
                    intent.putExtra(Const.FULL_NAME, mComment.getUploaderName());
                    intent.putExtra(Const.UID, myUid);
                    intent.putExtra(Const.OTHER_UID, mComment.getUploaderId());
                    startActivity(intent);
                }
            });


            /**
             * Buttons part
             */

            // setting color on like/dislike if you did so previously
            for (Map.Entry<String, String> likedPerson : mComment.getLikes().entrySet())
            {
                if (likedPerson.getValue().equals(myUid))
                {
                    commentLikeIV.setColorFilter(ContextCompat.getColor(PostItemActivity.this, R.color.clicked_blue));
                    commentLikesTV.setTextColor(ContextCompat.getColor(PostItemActivity.this, R.color.clicked_blue));
                    setButtonsInactive();

                }
            }
            for (Map.Entry<String, String> dislikedPerson : mComment.getDislikes().entrySet())
            {
                if (dislikedPerson.getValue().equals(myUid))
                {
                    commentDislikeIV.setColorFilter(ContextCompat.getColor(PostItemActivity.this, R.color.clicked_red));
                    commentDislikesTV.setTextColor(ContextCompat.getColor(PostItemActivity.this, R.color.clicked_red));
                    setButtonsInactive();
                }
            }

            // liking comment
            commentLikeIV.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    // changing the color and statically incrementing likes count
                    commentLikeIV.setColorFilter(ContextCompat.getColor(PostItemActivity.this, R.color.clicked_blue));
                    commentLikesTV.setTextColor(ContextCompat.getColor(PostItemActivity.this, R.color.clicked_blue));

                    int newLikeNumber = Integer.parseInt(commentLikesTV.getText().toString()) + 1;
                    commentLikesTV.setText(Integer.toString(newLikeNumber));

                    // sending my id to list of liked users to firebase database
                    if(postForFriendsOrBilkent.equals("false"))
                    {
                        mDatabaseReference.child(Const.BRUMORS).child(postKey).child(Const.COMMENT_LIST)
                                .child(mComment.getCommentKey()).child(Const.POST_LIKES).push()
                                .setValue(myUid);
                    }
                    else
                    {
                        mDatabaseReference.child(Const.USERS).child(postedUserUid).child(Const.POSTS_LISTS)
                                .child(postKey).child(Const.COMMENT_LIST).child(mComment.getCommentKey()).child(Const.POST_LIKES).push()
                                .setValue(myUid);
                    }


                    // making like button inactive
                    setButtonsInactive();

                }
            });
            // disliking comment
            commentDislikeIV.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    // changing the color and statically incrementing likes count
                    commentDislikeIV.setColorFilter(ContextCompat.getColor(PostItemActivity.this, R.color.clicked_red));
                    commentDislikesTV.setTextColor(ContextCompat.getColor(PostItemActivity.this, R.color.clicked_red));

                    int newLikeNumber = Integer.parseInt(commentDislikesTV.getText().toString()) + 1;
                    commentDislikesTV.setText(Integer.toString(newLikeNumber));

                    // sending my id to list of disliked users to firebase database
                    if(postForFriendsOrBilkent.equals("false"))
                    {
                        mDatabaseReference.child(Const.BRUMORS).child(postKey).child(Const.COMMENT_LIST)
                                .child(mComment.getCommentKey()).child(Const.POST_DISLIKES).push()
                                .setValue(myUid);
                    }
                    else
                    {
                        mDatabaseReference.child(Const.USERS).child(postedUserUid).child(Const.POSTS_LISTS)
                                .child(postKey).child(Const.COMMENT_LIST).child(mComment.getCommentKey()).child(Const.POST_DISLIKES).push()
                                .setValue(myUid);
                    }

                    // making like button inactive
                    setButtonsInactive();
                }
            });

        }

        private void setButtonsInactive()
        {
            commentDislikeIV.setEnabled(false);
            commentDislikeIV.setClickable(false);
            commentLikeIV.setEnabled(false);
            commentLikeIV.setClickable(false);
        }
    }

    /**
     * Inner class Adapter
     */
    private class CommentAdapter extends RecyclerView.Adapter<CommentHolder>
    {

        private List<Comment> mComments;
        private int lastPosition = -1;

        private CommentAdapter(List<Comment> comments)
        {
            mComments = comments;
        }

        @Override
        public CommentHolder onCreateViewHolder(ViewGroup parent, int viewType)
        {
            LayoutInflater layoutInflater = LayoutInflater.from(getApplicationContext());
            View view = layoutInflater.inflate(R.layout.list_item_comment, parent, false);

            return new CommentHolder(view);
        }

        @Override
        public void onBindViewHolder(CommentHolder holder, int position)
        {
            Comment comment = mComments.get(position);
            holder.bindComment(comment);

            setAnimation(holder.commentCV, position);
        }

        private void setAnimation(View viewToAnimate, int position)
        {
            // If the bound view wasn't previously displayed on screen, it's animated
            if (position > lastPosition)
            {
                viewToAnimate.animate().cancel();
                viewToAnimate.setTranslationY(100);
                viewToAnimate.setAlpha(0);
                viewToAnimate.animate().alpha(1.0f).translationY(0).setDuration(250).setStartDelay(position * 2);
            }
        }

        @Override
        public int getItemCount()
        {
            return mComments.size();
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();

        if (id == android.R.id.home)
            finish();

        return super.onOptionsItemSelected(item);
    }

}
