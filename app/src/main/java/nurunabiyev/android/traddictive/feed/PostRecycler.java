package nurunabiyev.android.traddictive.feed;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.format.DateFormat;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.Date;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import nurunabiyev.android.traddictive.Const;
import nurunabiyev.android.traddictive.R;
import nurunabiyev.android.traddictive.objects.Post;

/**
 * by Nuru on 22-Jul-17.
 */

public class PostRecycler
{
    // TODO LIKES AND DISLIKES
    private final String TAG = "postrecycler";
    private DatabaseReference mDatabaseRef;
    private StorageReference mStorageRef;
    private String myUid;
    private String otherUid;

    public PostRecycler(String uid, String otherUid)
    {
        mStorageRef = FirebaseStorage.getInstance().getReference();
        mDatabaseRef = FirebaseDatabase.getInstance().getReference();
        myUid = uid;
        this.otherUid = otherUid;
    }

    /**
     * Inner class ViewHolder
     */
    private class PostHolder extends RecyclerView.ViewHolder
    {
        private CardView postCV;
        private Post mPost;
        // addon part
        private CircleImageView postedUserCIV;
        private TextView postedUserNameTV;
        private TextView postedTimeTV;    // also may include "by Name Surname" in brumors

        private ViewStub addonStub;

        private ViewGroup viewGroup;
        private LinearLayout postTypeBrumorLL;
        private LinearLayout postTypePhotoLL;

        // user
        private String profilePhotoId;


        private PostHolder(final View itemView, ViewGroup viewGroup)
        {
            super(itemView);
            // addon part
            postCV = (CardView) itemView.findViewById(R.id.post_item_cv);

            addonStub = (ViewStub) itemView.findViewById(R.id.addon_section_stub);
            addonStub.setLayoutResource(R.layout.section_addon);
            View addonView = addonStub.inflate();
            postedUserNameTV = (TextView) addonView.findViewById(R.id.posted_username_tv);
            postedTimeTV = (TextView) addonView.findViewById(R.id.posted_time_tv);
            postedUserCIV = (CircleImageView) addonView.findViewById(R.id.posted_user_civ);

            this.viewGroup = viewGroup;
            postTypeBrumorLL = itemView.findViewById(R.id.include_type_brumor);
            postTypePhotoLL = itemView.findViewById(R.id.include_type_photo);
        }

        private void bindPhoto(Post photo)
        {
            postTypeBrumorLL.setVisibility(View.GONE);
            postTypePhotoLL.setVisibility(View.VISIBLE);

            mPost = photo;

            // profile photo
            mDatabaseRef.child(Const.USERS).child(mPost.getUploaderId())
                    .child(Const.PROFILE_PHOTO_ID).addListenerForSingleValueEvent(new ValueEventListener()
            {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot)
                {
                    profilePhotoId = dataSnapshot.getValue(String.class);

                    if(profilePhotoId != null)
                    {
                        StorageReference storageReference = mStorageRef.child(Const.USERS)
                                .child(mPost.getUploaderId()).child(Const.PHOTOS_LISTS)
                                .child(Const.PHOTO_THUMBNAIL + dataSnapshot.getValue(String.class));
                        Glide.with(itemView.getContext().getApplicationContext())
                                .using(new FirebaseImageLoader())
                                .load(storageReference)
                                .into(postedUserCIV);
                    }
                    else
                        postedUserCIV.setImageDrawable(itemView.getResources().getDrawable(R.drawable.default_avatar));
                }

                @Override
                public void onCancelled(DatabaseError databaseError)
                {
                }
            });

            postedUserNameTV.setText(mPost.getUploaderName());

            // inflating and initializing photo view
            inflatePostAsPhoto();

            // date
            Date sentTimeDate = new Date(Long.parseLong(mPost.getTimeSent().toString()));
            String dateText = (String) DateFormat.format("d MMMM HH:mm", sentTimeDate);
            postedTimeTV.setText(dateText);

            // open post
            postCV.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    Intent photoActivity = new Intent(itemView.getContext(), PostItemActivity.class);

                    photoActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                    photoActivity.putExtra(Const.UID, myUid);
                    photoActivity.putExtra(Const.OTHER_UID, mPost.getUploaderId());
                    photoActivity.putExtra(Const.POST_ID, mPost.getPostId());
                    photoActivity.putExtra(Const.POST_FOR_FRIENDS_OR_BILKENT, "" + mPost.isForFriendsOrBilkent());
                    photoActivity.putExtra(Const.POST_KEY, mPost.getPostKey());
                    photoActivity.putExtra(Const.FULL_NAME, postedUserNameTV.getText().toString());
                    photoActivity.putExtra(Const.PROFILE_PHOTO_ID, profilePhotoId);
                    itemView.getContext().startActivity(photoActivity);

//                    photoActivity.putExtra(Const.UID, otherUid);
//                    photoActivity.putExtra(Const.OTHER_UID, mPost.getUploaderId());
//                    photoActivity.putExtra(Const.POST_FOR_FRIENDS_OR_BILKENT, "" + mPost.isForFriendsOrBilkent());
//                    photoActivity.putExtra(Const.POST_ID, mPost.getPostId());
//                    photoActivity.putExtra(Const.POST_KEY, mPost.getPostKey());
//                    photoActivity.putExtra(Const.FULL_NAME, mPost.getUploaderName());
//                    photoActivity.putExtra(Const.PROFILE_PHOTO_ID, profilePhotoId);

                    //itemView.getContext().startActivity(photoActivity);
                }
            });
        }

        private void inflatePostAsPhoto()
        {

            if (itemView.getContext() == null)
                return;

            // initializing photo views
            final ImageView postedPhotoIV = (ImageView) itemView.findViewById(R.id.posted_photo_iv);
            final ImageView photoLikeIV = (ImageView) itemView.findViewById(R.id.photo_like_iv);
            ImageView photoCommentIV = (ImageView) itemView.findViewById(R.id.photo_comment_iv);
            final TextView photoLikesTV = (TextView) itemView.findViewById(R.id.photo_likes_tv);
            TextView photoCommentsTV = (TextView) itemView.findViewById(R.id.photo_comments_tv);
            TextView postedPhotoTextTV = (TextView) itemView.findViewById(R.id.posted_photo_text_tv);

            // setting numbers
            photoLikesTV.setText(((mPost.getLikes() == null) ? "0" : "" + mPost.getLikes().size()));
            photoCommentsTV.setText(((mPost.getComments() == null) ? "0" : "" + mPost.getComments().size()));
            postedPhotoTextTV.setText(mPost.getText());

            // color like button if needed
            for (Map.Entry<String, String> likedPerson : mPost.getLikes().entrySet())
            {
                if (likedPerson.getValue().equals(myUid))
                {
                    photoLikeIV.setColorFilter(ContextCompat.getColor(itemView.getContext(), R.color.clicked_blue));
                    photoLikesTV.setTextColor(ContextCompat.getColor(itemView.getContext(), R.color.clicked_blue));
                    photoLikeIV.setEnabled(false);
                    photoLikeIV.setClickable(false);
                }
            }

            // downloading and setting photo to image view
            StorageReference storageReference = mStorageRef.child(Const.USERS)
                    .child(mPost.getUploaderId()).child(Const.PHOTOS_LISTS)
                    .child(Const.PHOTO_ORIGINAL + mPost.getPostId());
            Glide.with(itemView.getContext())
                    .using(new FirebaseImageLoader())
                    .load(storageReference)
                    .into(postedPhotoIV);

            // liking photo
            photoLikeIV.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    // changing the color and incrementing likes count
                    photoLikeIV.setColorFilter(ContextCompat.getColor(itemView.getContext(), R.color.clicked_blue));
                    photoLikesTV.setTextColor(ContextCompat.getColor(itemView.getContext(), R.color.clicked_blue));

                    int newLikeNumber = Integer.parseInt(photoLikesTV.getText().toString()) + 1;
                    photoLikesTV.setText(Integer.toString(newLikeNumber));

                    // sending my id to list of liked users to firebase database
                    mDatabaseRef.child(Const.USERS).child(mPost.getUploaderId()).child(Const.POSTS_LISTS)
                            .child(mPost.getPostKey()).child(Const.POST_LIKES).push()
                            .setValue(myUid);

                    // making like button inactive
                    photoLikeIV.setEnabled(false);
                    photoLikeIV.setClickable(false);
                }
            });
        }

        // change type is needed to change main text to italic.
        // 0 = nothing (as brumor) , 1 - change status, 2 - change relationship status
        // 3 = created hangout
        private void bindBrumor(Post brumor, final int changeType)
        {
            postTypeBrumorLL.setVisibility(View.VISIBLE);
            postTypePhotoLL.setVisibility(View.GONE);

            mPost = brumor;

            // show addon as a brumor or just user's brumor
            Date sentTimeDate = new Date(Long.parseLong(mPost.getTimeSent().toString()));
            String dateText = (String) DateFormat.format("d MMMM HH:mm", sentTimeDate);
            if (mPost.isForFriendsOrBilkent())
            {
                postedUserNameTV.setText(mPost.getUploaderName());
                postedTimeTV.setText(dateText);

                mDatabaseRef.child(Const.USERS).child(mPost.getUploaderId())
                        .child(Const.PROFILE_PHOTO_ID).addListenerForSingleValueEvent(new ValueEventListener()
                {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot)
                    {
                        profilePhotoId = dataSnapshot.getValue(String.class);

                        // downloading and setting photo to image view

                        if(profilePhotoId != null)
                        {
                            StorageReference storageReference = mStorageRef.child(Const.USERS)
                                    .child(mPost.getUploaderId()).child(Const.PHOTOS_LISTS)
                                    .child(Const.PHOTO_THUMBNAIL + dataSnapshot.getValue(String.class));
                            Glide.with(itemView.getContext())
                                    .using(new FirebaseImageLoader())
                                    .load(storageReference)
                                    .into(postedUserCIV);
                        }
                        else
                            postedUserCIV.setImageDrawable(itemView.getResources().getDrawable(R.drawable.default_avatar));
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError)
                    {
                    }
                });
            }
            // whole bilkent
            else
            {
                // get brumor count
                postedUserNameTV.setText(Const.BRUMOR_POST_NAME + " #" + mPost.getPostCount());

                // anonymous or not
                if (!mPost.isAnonymous())
                    postedTimeTV.setText("by " + mPost.getUploaderName() + " at " + dateText);
                else
                    postedTimeTV.setText("by Anonymous" + " at " + dateText);

                postedUserCIV.setImageDrawable(itemView.getResources().getDrawable(R.drawable.ic_brumor_green));
                postedUserCIV.setBorderColor(itemView.getResources().getColor(R.color.brumor_green));
            }

            // inflating and initializing photo view
            inflatePostAsBrumor(changeType);

            // open post
            postCV.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {

                    // story (v2)
                    if (changeType == 4)
                    {
                        // do not open story mode open who watched
                    } else
                    {
                        Intent brumorActivity = new Intent(itemView.getContext(), PostItemActivity.class);
                        brumorActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        brumorActivity.putExtra(Const.UID, myUid);
                        brumorActivity.putExtra(Const.OTHER_UID, mPost.getUploaderId());
                        brumorActivity.putExtra(Const.POST_ID, mPost.getPostId());
                        brumorActivity.putExtra(Const.POST_FOR_FRIENDS_OR_BILKENT, "" + mPost.isForFriendsOrBilkent());
                        brumorActivity.putExtra(Const.POST_KEY, mPost.getPostKey());
                        brumorActivity.putExtra(Const.FULL_NAME, postedUserNameTV.getText().toString());
                        brumorActivity.putExtra(Const.PROFILE_PHOTO_ID, profilePhotoId);
                        itemView.getContext().startActivity(brumorActivity);
                    }

                }
            });
        }

        private void inflatePostAsBrumor(int changeType)
        {

            // initializing brumor views
            final ImageView brumorLikeIV = (ImageView) itemView.findViewById(R.id.brumor_like_iv);
            final ImageView brumorDislikeIV = (ImageView) itemView.findViewById(R.id.brumor_dislike_iv);
            ImageView brumorCommentIV = (ImageView) itemView.findViewById(R.id.brumor_comment_iv);
            final TextView brumorLikesTV = (TextView) itemView.findViewById(R.id.brumor_likes_tv);
            final TextView brumorDislikesTV = (TextView) itemView.findViewById(R.id.brumor_dislikes_tv);
            TextView brumorCommentsTV = (TextView) itemView.findViewById(R.id.brumor_comments_tv);
            TextView postedBrumorTextTV = (TextView) itemView.findViewById(R.id.posted_brumor_text_tv);

            // setting numbers
            brumorLikesTV.setText(((mPost.getLikes() == null) ? "0" : "" + mPost.getLikes().size()));
            brumorDislikesTV.setText(((mPost.getDislikes() == null) ? "0" : "" + mPost.getDislikes().size()));
            brumorCommentsTV.setText(((mPost.getComments() == null) ? "0" : "" + mPost.getComments().size()));

            // setting brumor text depending on type
            if (changeType == 0)
                postedBrumorTextTV.setText(mPost.getText());
            else if (changeType == 1)
            {
                // making name of the sender italic
                SpannableStringBuilder str = new SpannableStringBuilder(mPost.getText());
                str.setSpan
                        (
                                new android.text.style.StyleSpan(Typeface.ITALIC),
                                0,
                                itemView.getResources().getString(R.string.has_updated_status).length(),
                                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                        );
                postedBrumorTextTV.setText(str);
            } else if (changeType == 2)
            {
                // making name of the sender italic
                SpannableStringBuilder str = new SpannableStringBuilder(mPost.getText());
                str.setSpan
                        (
                                new android.text.style.StyleSpan(Typeface.ITALIC),
                                0,
                                itemView.getResources().getString(R.string.has_updated_relation_status).length(),
                                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                        );
                postedBrumorTextTV.setText(str);
            } else if (changeType == 3)
            {
                // making name of the sender italic
                SpannableStringBuilder str = new SpannableStringBuilder(mPost.getText());
                str.setSpan
                        (
                                new android.text.style.StyleSpan(Typeface.ITALIC),
                                0,
                                itemView.getResources().getString(R.string.has_created_hangout).length(),
                                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                        );
                postedBrumorTextTV.setText(str);
            }

            handleLikesDislikes(brumorLikeIV, brumorDislikeIV, brumorLikesTV, brumorDislikesTV);
        }

        private void bindStory(Post story)
        {
            mPost = story;

            // todo V2 make foreground with text

            // if clicked on -> show post
            Date sentTimeDate = new Date(Long.parseLong(mPost.getTimeSent().toString()));
            String dateText = (String) DateFormat.format("d MMMM HH:mm", sentTimeDate);

            postedTimeTV.setText(dateText);
            postedUserCIV.setImageDrawable(itemView.getResources().getDrawable(R.drawable.ic_anon));
            postedUserCIV.setColorFilter(ContextCompat.getColor(itemView.getContext(), R.color.colorPrimary),
                    android.graphics.PorterDuff.Mode.MULTIPLY);
            postedUserCIV.setBorderColor(itemView.getContext().getResources().getColor( R.color.colorPrimaryDark));


            postTypeBrumorLL.setVisibility(View.VISIBLE);
            postTypePhotoLL.setVisibility(View.GONE);

            // initializing brumor views
            final ImageView likeIv = (ImageView) itemView.findViewById(R.id.brumor_like_iv);
            final ImageView dislikeIV = (ImageView) itemView.findViewById(R.id.brumor_dislike_iv);
            ImageView viewIV = (ImageView) itemView.findViewById(R.id.brumor_comment_iv); // icon will be changed
            final TextView likesTV = (TextView) itemView.findViewById(R.id.brumor_likes_tv);
            final TextView dislikesTV = (TextView) itemView.findViewById(R.id.brumor_dislikes_tv);
            TextView viewsTV = (TextView) itemView.findViewById(R.id.brumor_comments_tv);
            TextView secretTextTV = (TextView) itemView.findViewById(R.id.posted_brumor_text_tv);

            // setting numbers
            likesTV.setText(((mPost.getLikes() == null) ? "0" : "" + mPost.getLikes().size()));
            dislikesTV.setText(((mPost.getDislikes() == null) ? "0" : "" + mPost.getDislikes().size()));
            secretTextTV.setText(mPost.getText());

            // if it is not my story
            if (!mPost.getUploaderId().equals(myUid))
            {
                postedUserNameTV.setText("Anonymous Friend");

                // blur it if i haven't seen it
                if (!mPost.getViews().containsValue(myUid))
                    postCV.setForeground(itemView.getResources().getDrawable(R.drawable.loading_photo)); // todo V2 blur

                viewIV.setImageDrawable(new ColorDrawable(Color.TRANSPARENT));
                viewsTV.setText("");

                // incrementing views
                postCV.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        postCV.setForeground(null);

                        if (!mPost.getViews().containsValue(myUid))
                            mDatabaseRef.child(Const.USERS).child(mPost.getUploaderId()).child(Const.POSTS_LISTS)
                                    .child(mPost.getPostKey()).child(Const.POST_VIEWS).push()
                                    .setValue(myUid);

                        handleLikesDislikes(likeIv, dislikeIV, likesTV, dislikesTV);
                    }
                });
            }
            // if it is my story
            else
            {
                // making name of the sender italic
                SpannableStringBuilder str = new SpannableStringBuilder("Me");
                str.setSpan
                        (
                                new StyleSpan(Typeface.BOLD_ITALIC),
                                0,
                                2,
                                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                        );
                postedUserNameTV.setText(str);

                viewsTV.setText(((mPost.getComments() == null) ? "0" : "" + mPost.getViews().size()));
                viewIV.setImageDrawable(itemView.getResources().getDrawable(R.drawable.ic_eye));

                viewIV.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        // todo V2 open list
                        Toast.makeText(viewGroup.getContext(), "" + mPost.getViews().size(), Toast.LENGTH_SHORT).show();
                    }
                });

                handleLikesDislikes(likeIv, dislikeIV, likesTV, dislikesTV);
            }

        }


        // method to handle likes/dislikes
        private void handleLikesDislikes(final ImageView likeIV, final ImageView dislikeIV,
                                         final TextView likesTV, final TextView dislikesTV)
        {
            // setting like/dislike color
            for (Map.Entry<String, String> likedPerson : mPost.getLikes().entrySet())
            {
                if (likedPerson.getValue().equals(myUid))
                {
                    likeIV.setColorFilter(ContextCompat.getColor(itemView.getContext(), R.color.clicked_blue));
                    likesTV.setTextColor(ContextCompat.getColor(itemView.getContext(), R.color.clicked_blue));
                    likeIV.setEnabled(false);
                    likeIV.setClickable(false);
                    dislikeIV.setEnabled(false);
                    dislikeIV.setClickable(false);
                }
            }

            for (Map.Entry<String, String> dislikedPerson : mPost.getDislikes().entrySet())
            {
                if (dislikedPerson.getValue().equals(myUid))
                {
                    dislikeIV.setColorFilter(ContextCompat.getColor(itemView.getContext(), R.color.clicked_red));
                    dislikesTV.setTextColor(ContextCompat.getColor(itemView.getContext(), R.color.clicked_red));
                    dislikeIV.setEnabled(false);
                    dislikeIV.setClickable(false);
                    likeIV.setEnabled(false);
                    likeIV.setClickable(false);
                }
            }


            // like/dislike listeners

            // liking post
            likeIV.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    // changing the color and statically incrementing likes count
                    likeIV.setColorFilter(ContextCompat.getColor(itemView.getContext(), R.color.clicked_blue));
                    likesTV.setTextColor(ContextCompat.getColor(itemView.getContext(), R.color.clicked_blue));

                    int newLikeNumber = Integer.parseInt(likesTV.getText().toString()) + 1;
                    likesTV.setText(Integer.toString(newLikeNumber));

                    // sending my id to list of liked users to firebase database
                    if(mPost.getType().equals(Const.POST_TYPE_BRUMOR) && !mPost.isForFriendsOrBilkent())
                    {
                        mDatabaseRef.child(Const.BRUMORS).child(mPost.getPostKey()).child(Const.POST_LIKES)
                                .push()
                                .setValue(myUid);
                    }
                    else
                    {
                        mDatabaseRef.child(Const.USERS).child(mPost.getUploaderId()).child(Const.POSTS_LISTS)
                                .child(mPost.getPostKey()).child(Const.POST_LIKES).push()
                                .setValue(myUid);
                    }


                    // making like button inactive
                    dislikeIV.setEnabled(false);
                    dislikeIV.setClickable(false);
                    likeIV.setEnabled(false);
                    likeIV.setClickable(false);
                }
            });

            // disliking comment
            dislikeIV.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    // changing the color and statically incrementing likes count
                    dislikeIV.setColorFilter(ContextCompat.getColor(itemView.getContext(), R.color.clicked_red));
                    dislikesTV.setTextColor(ContextCompat.getColor(itemView.getContext(), R.color.clicked_red));

                    int newLikeNumber = Integer.parseInt(dislikesTV.getText().toString()) + 1;
                    dislikesTV.setText(Integer.toString(newLikeNumber));

                    // sending my id to list of disliked users to firebase database
                    if(mPost.getType().equals(Const.POST_TYPE_BRUMOR) && !mPost.isForFriendsOrBilkent())
                    {
                        mDatabaseRef.child(Const.BRUMORS).child(mPost.getPostKey()).child(Const.POST_DISLIKES)
                                .push()
                                .setValue(myUid);
                    }
                    else
                    {
                        mDatabaseRef.child(Const.USERS).child(mPost.getUploaderId()).child(Const.POSTS_LISTS)
                                .child(mPost.getPostKey()).child(Const.POST_DISLIKES).push()
                                .setValue(myUid);
                    }

                    // making like button inactive
                    dislikeIV.setEnabled(false);
                    dislikeIV.setClickable(false);
                    likeIV.setEnabled(false);
                    likeIV.setClickable(false);
                }
            });
        }


    }


    /**
     * Inner class Adapter
     */
    public class PostAdapter extends RecyclerView.Adapter<PostRecycler.PostHolder>
    {

        private List<Post> mPosts;
        private int lastPosition = -1;
        private ViewGroup vg;

        public PostAdapter(List<Post> posts)
        {
            mPosts = posts;
        }

        @Override
        public PostHolder onCreateViewHolder(ViewGroup viewGroup, int viewType)
        {
            LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
            View view = inflater.inflate(R.layout.list_item_post, viewGroup, false);
            vg = viewGroup;
            return new PostHolder(view, viewGroup);
        }

        @Override
        public void onBindViewHolder(PostRecycler.PostHolder holder, int position)
        {
            Post post = mPosts.get(position);

            switch (post.getType())
            {
                case Const.POST_TYPE_PHOTO:
                    holder.bindPhoto(post);
                    break;
                case Const.POST_TYPE_BRUMOR:
                    holder.bindBrumor(post, 0);
                    break;
                case Const.POST_TYPE_CHANGE_STATUS:
                    holder.bindBrumor(post, 1);
                    break;
                case Const.POST_TYPE_CHANGE_RELATION_STATUS:
                    holder.bindBrumor(post, 2);
                    break;
                case Const.POST_TYPE_HANGOUT:
                    holder.bindBrumor(post, 3);
                    break;
                case Const.POST_TYPE_SECRET:
                    holder.bindStory(post);
                    break;
            }

            setAnimation(holder.postCV, position);
        }

        private void setAnimation(View viewToAnimate, int position)
        {
            // If the bound view wasn't previously displayed on screen, it's animated
            if (position > lastPosition)
            {
                Animation animation = AnimationUtils.loadAnimation(vg.getContext(), android.R.anim.fade_out);
                viewToAnimate.startAnimation(animation);
                lastPosition = position;
            }
        }

        @Override
        public int getItemCount()
        {
            return mPosts.size();
        }
    }

}




