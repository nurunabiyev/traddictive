package nurunabiyev.android.traddictive.feed;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.Date;
import java.util.List;

import nurunabiyev.android.traddictive.Const;
import nurunabiyev.android.traddictive.R;
import nurunabiyev.android.traddictive.objects.Product;
import nurunabiyev.android.traddictive.profile.ProfileActivity;

/**
 * Created by HP on 22-Aug-17.
 */

public class BuykentRecycler
{
    private final String TAG = "buykentrecycler";
    private DatabaseReference mDatabaseRef;
    private StorageReference mStorageRef;
    private String myUid;
    private String otherUid;

    public BuykentRecycler(String uid, String otherUid)
    {
        mStorageRef = FirebaseStorage.getInstance().getReference();
        mDatabaseRef = FirebaseDatabase.getInstance().getReference();
        myUid = uid;
        this.otherUid = otherUid;
    }


    /**
     * Inner Holder class
     */
    public class ProductHolder extends RecyclerView.ViewHolder
    {
        private Product mProduct;

        private TextView titleTV;
        private TextView descriptionTV;
        private TextView conditionTV;
        private TextView priceTV;

        private TextView timeSentTV;

        private TextView uploaderNameTV;
        private ImageView contactSeller;
        protected CardView productCV;

        private boolean isOwner;

        public ProductHolder(View itemView)
        {
            super(itemView);

            timeSentTV = (TextView) itemView.findViewById(R.id.product_time_posted_tv);
            titleTV = (TextView) itemView.findViewById(R.id.product_title_tv);
            descriptionTV = (TextView) itemView.findViewById(R.id.product_description_tv);
            conditionTV = (TextView) itemView.findViewById(R.id.product_condition_tv);
            uploaderNameTV = (TextView) itemView.findViewById(R.id.product_uploader_name_tv);
            priceTV = (TextView) itemView.findViewById(R.id.product_price_tv);
            productCV = (CardView) itemView.findViewById(R.id.product_cv);
            contactSeller = (ImageView) itemView.findViewById(R.id.product_contact_iv);
        }

        public void bindProduct(final Product product)
        {
            mProduct = product;

            Date sentTimeDate = new Date(Long.parseLong(mProduct.getTimeSent().toString()));
            String dateText = (String) DateFormat.format("d MMMM HH:mm", sentTimeDate);
            timeSentTV.setText(dateText + " by ");

            titleTV.setText(mProduct.getTitle());
            descriptionTV.setText(mProduct.getDescription());
            conditionTV.setText(String.valueOf("Condition: " + mProduct.getCondition()));
            uploaderNameTV.setText(mProduct.getUploaderName());
            priceTV.setText(String.valueOf(mProduct.getPrice() + " TL"));


            contactSeller.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    // if it is NOT my product -> display "contact menu"
                    if (!mProduct.getUploaderId().equals(myUid))
                        displayContactMenu(v);

                        // if it is my product -> display "delete menu"
                    else if (mProduct.getUploaderId().equals(myUid))
                        displayDeleteMenu(v);
                }
            });

        }

        private void displayDeleteMenu(final View v)
        {
            PopupMenu popup = new PopupMenu(itemView.getContext(), v);//
            popup.getMenuInflater().inflate(R.menu.product_menu_delete, popup.getMenu());
            popup.show();

            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener()
            {
                public boolean onMenuItemClick(MenuItem item)
                {
                    int id = item.getItemId();

                    if(id == R.id.product_delete_product)
                    {
                        // deleting buykent
                        mDatabaseRef.child(Const.BUYKENT).child(mProduct.getCategory())
                                .child(mProduct.getProductKey()).setValue(null);

                        // deleting buykent reference from user
                        mDatabaseRef.child(Const.USERS).child(mProduct.getUploaderId())
                                .child(Const.BUYKENT_LIST).child(mProduct.getProductKey())
                                .setValue(null);

                        Toast.makeText(v.getContext(), "Removed! Please Refresh", Toast.LENGTH_SHORT).show();
                    }

                    return true;
                }
            });
        }

        private void displayContactMenu(final View v)
        {
            PopupMenu popup = new PopupMenu(itemView.getContext(), v);//
            popup.getMenuInflater().inflate(R.menu.product_menu_contact, popup.getMenu());
            popup.show();

            if (mProduct.getUploaderPhone().length() < 3)
            {
                popup.getMenu().getItem(1).setEnabled(false);
            }

            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener()
            {
                public boolean onMenuItemClick(MenuItem item)
                {
                    int id = item.getItemId();

                    // sending email
                    if (id == R.id.product_contact_email)
                    {
                        Intent i = new Intent(Intent.ACTION_SEND);
                        i.setType("message/rfc822");
                        i.putExtra(Intent.EXTRA_EMAIL, new String[]{mProduct.getUploaderEmail()});
                        i.putExtra(Intent.EXTRA_SUBJECT, "Traddictive: Someone wants to buy you product!");
                        i.putExtra(Intent.EXTRA_TEXT,
                                "Dear " + mProduct.getUploaderName() + ",\n\n\n"
                                        + "<Your message here>\n\n\n\nby Traddictive for Android");
                        try
                        {
                            v.getContext().startActivity(Intent.createChooser(i, "Send email..."));
                        } catch (android.content.ActivityNotFoundException ex)
                        {
                            Toast.makeText(v.getContext(), "There are no email clients installed.", Toast.LENGTH_SHORT).show();
                        }
                    }

                    // calling
                    if (id == R.id.product_contact_phone)
                    {

                        Intent i = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + mProduct.getUploaderPhone()));
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        v.getContext().startActivity(i);
                    }

                    // opening profile
                    if (id == R.id.product_view_profile)
                    {

                        Intent intent = new Intent(itemView.getContext(), ProfileActivity.class);
                        intent.putExtra(Const.FULL_NAME, mProduct.getUploaderName());
                        intent.putExtra(Const.UID, myUid);
                        intent.putExtra(Const.OTHER_UID, mProduct.getUploaderId());
                        itemView.getContext().startActivity(intent);
                    }

                    return true;
                }
            });
        }
    }


    /**
     * Inner class Adapter
     */
    public class ProductAdapter extends RecyclerView.Adapter<ProductHolder>
    {
        List<Product> mProducts;
        private int lastPosition = -1;
        private ViewGroup vg;

        public ProductAdapter(List<Product> products)
        {
            mProducts = products;
        }

        @Override
        public ProductHolder onCreateViewHolder(ViewGroup parent, int viewType)
        {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View view = layoutInflater.inflate(R.layout.list_item_product, parent, false);
            vg = parent;
            return new ProductHolder(view);
        }

        @Override
        public void onBindViewHolder(ProductHolder holder, int position)
        {
            Product product = mProducts.get(position);
            holder.bindProduct(product);
            // Here I apply the animation when the view is bound

            setAnimation(holder.productCV, position);
        }

        /**
         * Here is the key method to apply the animation
         */
        private void setAnimation(View viewToAnimate, int position)
        {
            // If the bound view wasn't previously displayed on screen, it's animated
            if (position > lastPosition)
            {
                Animation animation = AnimationUtils.loadAnimation(vg.getContext(), android.R.anim.fade_out);
                viewToAnimate.startAnimation(animation);
                lastPosition = position;
            }
        }

        @Override
        public int getItemCount()
        {
            return mProducts.size();
        }
    }
}
