package nurunabiyev.android.traddictive.feed;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import butterknife.BindView;
import butterknife.ButterKnife;
import nurunabiyev.android.traddictive.Const;
import nurunabiyev.android.traddictive.R;
import nurunabiyev.android.traddictive.hangout.HangoutDetailFragment;
import nurunabiyev.android.traddictive.hangout.HangoutMessageListFragment;

import static nurunabiyev.android.traddictive.Const.PARAM_HANGOUT_APPLICANT_LIST;
import static nurunabiyev.android.traddictive.Const.PARAM_HANGOUT_MEMBERS_LIST;

public class TabbedActivity extends AppCompatActivity
{

    private static final String TAG = "tabbedactivity";
    // view pager settings
    private Bundle mBundle;
    // view
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    private Toolbar toolbar;
    @BindView(R.id.tabbed_search_et)
    EditText searchET;
    private String fragmentToDisplay;
    private String fragmentToSearch;
    // user
    private String myUid;
    private String otherUid;
    private DatabaseReference mDatabaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tabbed);

        mDatabaseReference = FirebaseDatabase.getInstance().getReference();

        initializeView();
    }

    private void initializeView()
    {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ButterKnife.bind(this);

        if (Build.VERSION.SDK_INT >= 21)
        {
            Window window = TabbedActivity.this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.colorPrimaryDark));
        }

        // getting view pager settings
        mBundle = getIntent().getBundleExtra(Const.PARAM_BUNDLE);
        myUid = mBundle.getString(Const.UID);
        otherUid = mBundle.getString(Const.OTHER_UID);

        fragmentToDisplay = mBundle.getString(Const.PARAM_PAGER_TYPE);
        fragmentToSearch = mBundle.getString(Const.PARAM_SEARCH_TYPE);  // will be null if fragmentToDisplay is not PARAM_SEARCH

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        setSearchSettings();
    }

    private void setSearchSettings()
    {
        // sliding viewpager to different page regarding the search type
        if (mBundle.getString(Const.PARAM_PAGER_TYPE).equals(Const.PARAM_SEARCH))
        {
            if (fragmentToSearch.equals(Const.BUYKENT))
            {
                mViewPager.setCurrentItem(1);
                searchET.setHint("Search in BuyKent");
            }

            if (fragmentToSearch.equals(Const.HANGOUTS))
            {
                mViewPager.setCurrentItem(2);
                searchET.setHint("Search in Hangout Tags");
            }
            if(fragmentToSearch.equals(Const.PEOPLE))
            {
                mViewPager.setCurrentItem(0);
                searchET.setHint("Search People");
            }

            // pager listener
            mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener()
            {
                @Override
                public void onPageScrolled(int i, float v, int i1) {}

                @Override
                public void onPageScrollStateChanged(int i) {}

                @Override
                public void onPageSelected(int i)
                {
                    switch (i)
                    {
                        case 0:
                            searchET.setHint("Search People");
                            break;
                        case 1:
                            fragmentToSearch = Const.BUYKENT;
                            searchET.setHint("Search in BuyKent");
                            break;
                        case 2:
                            fragmentToSearch = Const.HANGOUTS;
                            searchET.setHint("Search in Hangout Tags");
                            break;
                    }
                }
            });

            handleEditText();
        }
    }

    private void handleEditText()
    {
        searchET.setVisibility(View.VISIBLE);
        searchET.setFocusableInTouchMode(true);
        searchET.requestFocus();

        // clicking enter
        searchET.setOnKeyListener(new View.OnKeyListener()
        {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event)
            {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER))
                {
                    // Perform action on key press

                    // searching in people
                    if (mViewPager.getCurrentItem() == 0)
                        findPeople(searchET.getText().toString());

                    // searching in buykent
                    else if (mViewPager.getCurrentItem() == 1)
                        findBuykent(searchET.getText().toString());

                    // searching in hangout tags
                    else if (mViewPager.getCurrentItem() == 2)
                        findHangout(searchET.getText().toString());

                    // hide keyboard
                    InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                    View view = getCurrentFocus();
                    if (view == null)
                        view = new View(getApplicationContext());
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

                    return true;
                }
                return false;
            }
        });
    }

    private void findPeople(final String searchText)
    {
        mViewPager.setAdapter(new FragmentStatePagerAdapter(getSupportFragmentManager())
        {
            @Override
            public Fragment getItem(int position)
            {
                // displaying viewpager after clicking on search in main toolbar
                if (fragmentToDisplay.equals(Const.PARAM_SEARCH))
                {
                    Bundle searchInfo = new Bundle();
                    searchInfo.putString(Const.PARAM_SEARCH_REQUEST, searchText);

                    // search people
                    if (position == 0)
                        return PeopleListFragment.newInstance(myUid, otherUid, Const.PARAM_SEARCH_PEOPLE, searchInfo);

                    // search buykent
                    if (position == 1)
                        return FeedListFragment.newInstance(myUid, Const.SEARCH_BUYKENT, myUid, null);

                    // search hangouts
                    if (position == 2)
                        return FeedListFragment.newInstance(myUid, Const.SEARCH_HANGOUT, otherUid, null);
                }

                // this should not happen:
                return null;
            }

            @Override
            public int getCount()
            {
                return 3;
            }

            @Override
            public CharSequence getPageTitle(int position)
            {
                switch (position)
                {
                    case 0:
                        return mBundle.getString(Const.PARAM_PAGE_1);
                    case 1:
                        return mBundle.getString(Const.PARAM_PAGE_2);
                    case 2:
                        return mBundle.getString(Const.PARAM_PAGE_3);
                    case 3:
                        return mBundle.getString(Const.PARAM_PAGE_4);
                }
                return null;
            }
        });

        setSearchSettings();
    }

    private void findHangout(final String request)
    {
        mViewPager.setAdapter(new FragmentStatePagerAdapter(getSupportFragmentManager())
        {
            @Override
            public Fragment getItem(int position)
            {
                // displaying viewpager after clicking on search in main toolbar
                if (fragmentToDisplay.equals(Const.PARAM_SEARCH))
                {
                    Bundle searchInfo = new Bundle();
                    searchInfo.putString(Const.PARAM_SEARCH_REQUEST, request);

                    // search people
                    if (position == 0)
                        return PeopleListFragment.newInstance(myUid, otherUid, Const.PARAM_SEARCH_PEOPLE, null);

                    // search buykent
                    if (position == 1)
                        return FeedListFragment.newInstance(myUid, Const.SEARCH_BUYKENT, myUid, null);

                    // search hangouts
                    if (position == 2)
                        return FeedListFragment.newInstance(myUid, Const.SEARCH_HANGOUT, otherUid, searchInfo);
                }

                // this should not happen:
                return null;
            }

            @Override
            public int getCount()
            {
                return 3;
            }

            @Override
            public CharSequence getPageTitle(int position)
            {
                switch (position)
                {
                    case 0:
                        return mBundle.getString(Const.PARAM_PAGE_1);
                    case 1:
                        return mBundle.getString(Const.PARAM_PAGE_2);
                    case 2:
                        return mBundle.getString(Const.PARAM_PAGE_3);
                    case 3:
                        return mBundle.getString(Const.PARAM_PAGE_4);
                }
                return null;
            }
        });

        setSearchSettings();
    }

    private void findBuykent(final String request)
    {
        mViewPager.setAdapter(new FragmentStatePagerAdapter(getSupportFragmentManager())
        {
            @Override
            public Fragment getItem(int position)
            {
                // displaying viewpager after clicking on search in main toolbar
                if (fragmentToDisplay.equals(Const.PARAM_SEARCH))
                {
                    Bundle searchInfo = new Bundle();
                    searchInfo.putString(Const.PARAM_SEARCH_REQUEST, request);

                    // search people
                    if (position == 0)
                        return PeopleListFragment.newInstance(myUid, otherUid, Const.PARAM_SEARCH_PEOPLE, null);

                    // search buykent
                    if (position == 1)
                        return FeedListFragment.newInstance(myUid, Const.SEARCH_BUYKENT, myUid, searchInfo);

                    // search hangouts
                    if (position == 2)
                        return FeedListFragment.newInstance(myUid, Const.SEARCH_HANGOUT, otherUid, null);
                }

                // this should not happen:
                return null;
            }

            @Override
            public int getCount()
            {
                return 3;
            }

            @Override
            public CharSequence getPageTitle(int position)
            {
                switch (position)
                {
                    case 0:
                        return mBundle.getString(Const.PARAM_PAGE_1);
                    case 1:
                        return mBundle.getString(Const.PARAM_PAGE_2);
                    case 2:
                        return mBundle.getString(Const.PARAM_PAGE_3);
                    case 3:
                        return mBundle.getString(Const.PARAM_PAGE_4);
                }
                return null;
            }
        });

        setSearchSettings();
    }



    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    private class SectionsPagerAdapter extends FragmentStatePagerAdapter
    {

        private SectionsPagerAdapter(FragmentManager fm)
        {
            super(fm);
        }

        @Override
        public Fragment getItem(int position)
        {
            // getItem is called to instantiate the fragment for the given page.
            toolbar.setTitle(getResources().getString(R.string.more_t));

            // displaying viewpager after clicking on profile numbersCV
            if (fragmentToDisplay.equals(Const.PARAM_PROFILE))
            {

                // display first fragment as hangout list
                if (position == 0)
                    return FeedListFragment.newInstance(myUid, Const.FEED_HANGOUTS_JOINED, otherUid, null);

                // display second fragment as buykent list
                if (position == 1)
                    return FeedListFragment.newInstance(myUid, Const.FEED_BUYKENT, otherUid, null);

                // display third fragment as friends list
                if (position == 2)
                    return PeopleListFragment.newInstance(myUid, otherUid, Const.PARAM_FRIENDS_LIST, null);
            }

            // displaying viewpager after clicking on hangout
            else if (fragmentToDisplay.equals(Const.PARAM_HANGOUT_DETAILS))
            {
                // display hangout details
                if (position == 0)
                    return HangoutDetailFragment.newInstance(myUid, mBundle.getString(Const.PARAM_HANGOUT_KEY),
                            mBundle.getString(Const.PARAM_HANGOUT_HOST_ID));

                // display second fragment as host's messages
                if (position == 1)
                    return HangoutMessageListFragment.newInstance
                            (mBundle.getString(Const.PARAM_HANGOUT_KEY), myUid,
                                    true, mBundle.getString(Const.PARAM_HANGOUT_HOST_ID));

                // display third fragment as group messages
                if (position == 2)
                    return HangoutMessageListFragment.newInstance
                            (mBundle.getString(Const.PARAM_HANGOUT_KEY), myUid,
                                    false, mBundle.getString(Const.PARAM_HANGOUT_HOST_ID));
            }

            // displaying viewpager after clicking on search in main toolbar
            else if (fragmentToDisplay.equals(Const.PARAM_SEARCH))
            {
                // search people
                if (position == 0)
                    return PeopleListFragment.newInstance(myUid, otherUid, Const.PARAM_SEARCH_PEOPLE, null);

                // search buykent
                if (position == 1)
                    return FeedListFragment.newInstance(myUid, Const.SEARCH_BUYKENT, myUid, null);

                // display third fragment as friends list
                if (position == 2)
                    return FeedListFragment.newInstance(myUid, Const.SEARCH_HANGOUT, otherUid, null);
            }

            // pressing on "members and applicants"
            else if(fragmentToDisplay.equals(Const.PARAM_HANGOUT_PEOPLE))
            {
                setTitle(getString(R.string.people));

                Bundle bundle = new Bundle();
                bundle.putString(Const.PARAM_HANGOUT_KEY, mBundle.getString(Const.PARAM_HANGOUT_KEY));
                bundle.putString(Const.PARAM_HANGOUT_HOST_ID, mBundle.getString(Const.PARAM_HANGOUT_HOST_ID));

                // applicants
                if (position == 0)
                    return PeopleListFragment.newInstance(myUid, otherUid, PARAM_HANGOUT_MEMBERS_LIST, bundle);

                // members
                if (position == 1)
                    return PeopleListFragment.newInstance(myUid, otherUid, PARAM_HANGOUT_APPLICANT_LIST, bundle);
            }

            // inviting people
            else if(fragmentToDisplay.equals(Const.PARAM_INVITE_PEOPLE))
            {
                setTitle(getString(R.string.invite_friends));
                Bundle bundle = new Bundle();
                bundle.putString(Const.PARAM_HANGOUT_KEY, mBundle.getString(Const.PARAM_HANGOUT_KEY));
                bundle.putString(Const.PARAM_HANGOUT_HOST_ID, mBundle.getString(Const.PARAM_HANGOUT_HOST_ID));

                // search people
                if (position == 0)
                    return PeopleListFragment.newInstance(myUid, otherUid, Const.PARAM_INVITE_PEOPLE, bundle);
            }

            // this should not happen:
            return null;
        }


        @Override
        public int getCount()
        {
            return mBundle.getInt(Const.PARAM_PAGE_COUNT);
        }

        @Override
        public CharSequence getPageTitle(int position)
        {
            switch (position)
            {
                case 0:
                    return mBundle.getString(Const.PARAM_PAGE_1);
                case 1:
                    return mBundle.getString(Const.PARAM_PAGE_2);
                case 2:
                    return mBundle.getString(Const.PARAM_PAGE_3);
                case 3:
                    return mBundle.getString(Const.PARAM_PAGE_4);
            }
            return null;
        }
    }

    // pressing back button
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();
        if (id == android.R.id.home)
            finish();

        return super.onOptionsItemSelected(item);
    }
}
