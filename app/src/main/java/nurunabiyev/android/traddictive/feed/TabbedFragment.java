package nurunabiyev.android.traddictive.feed;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import nurunabiyev.android.traddictive.Const;
import nurunabiyev.android.traddictive.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class TabbedFragment extends Fragment
{
    private static final String TAG = "hangoutfragment";
    private static final String ARG_UID_PARAM = "param1";
    private static final String ARG_BUNDLE_PARAM = "param2";
    // view
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    private View mainView;
    // user
    private String myUid;
    private Bundle mBundle;

    public TabbedFragment()
    {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        mainView = inflater.inflate(R.layout.fragment_tabbed, container, false);
        initializeView();
        return mainView;
    }

    private void initializeView()
    {

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getChildFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) mainView.findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) mainView.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        // opens at all. todo v2 change to popular
        if(mBundle.getString(Const.PARAM_PAGER_TYPE).equals(Const.PAGER_HANGOUT))
            mViewPager.setCurrentItem(1, true);
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
        {
            myUid = getArguments().getString(ARG_UID_PARAM);
            mBundle = getArguments().getBundle(ARG_BUNDLE_PARAM);
        }
    }

    public static TabbedFragment newInstance(String uid, Bundle bundle)
    {
        TabbedFragment fragment = new TabbedFragment();
        Bundle args = new Bundle();
        args.putString(ARG_UID_PARAM, uid);
        args.putBundle(ARG_BUNDLE_PARAM, bundle);
        fragment.setArguments(args);

        return fragment;
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    private class SectionsPagerAdapter extends FragmentPagerAdapter
    {

        private SectionsPagerAdapter(FragmentManager fm)
        {
            super(fm);
        }

        @Override
        public Fragment getItem(int position)
        {
            String mPagerType = mBundle.getString(Const.PARAM_PAGER_TYPE);

            // hangouts
            if(mPagerType.equals(Const.PAGER_HANGOUT))
            {
                // display first fragment
                if (position == 0)
                    return FeedListFragment.newInstance(myUid, Const.FEED_HANGOUTS_POPULAR, myUid, null);

                // display second fragment
                if (position == 1)
                    return FeedListFragment.newInstance(myUid, Const.FEED_HANGOUTS_ALL, myUid, null);

                // display third fragment
                if (position == 2)
                    return FeedListFragment.newInstance(myUid, Const.FEED_HANGOUTS_JOINED, myUid, null);
            }
            else if (mPagerType.equals(Const.PAGER_BUYKENT))
            {
                // display first fragment
                if (position == 0)
                    return FeedListFragment.newInstance(myUid, Const.FEED_BUYKENT_EDUCATION, myUid, null);

                // display second fragment
                if (position == 1)
                    return FeedListFragment.newInstance(myUid, Const.FEED_BUYKENT_TECH, myUid, null);

                // display third fragment
                if (position == 2)
                    return FeedListFragment.newInstance(myUid, Const.FEED_BUYKENT_OTHER, myUid, null);
            }

            // places
            else if (mPagerType.equals(Const.PAGER_PLACES))
            {
                // display first fragment
                if (position == 0)
                    return PlaceListFragment.newInstance(Const.PLACES_CAFE_SHOPS, myUid);

                // display second fragment
                if (position == 1)
                    return PlaceListFragment.newInstance(Const.PLACES_DORMITORIES, myUid);

                // display third fragment
                if (position == 2)
                    return PlaceListFragment.newInstance(Const.PLACES_MAIN_BUILDINGS, myUid);
            }

            // this should not happen:
            return null;
        }

        @Override
        public int getCount()
        {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position)
        {
            switch (position)
            {
                case 0:
                    return mBundle.getString(Const.PARAM_PAGE_1);
                case 1:
                    return mBundle.getString(Const.PARAM_PAGE_2);
                case 2:
                    return mBundle.getString(Const.PARAM_PAGE_3);
            }
            return null;
        }
    }
}
