package nurunabiyev.android.traddictive.feed;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;

import nurunabiyev.android.traddictive.Const;
import nurunabiyev.android.traddictive.R;

public class StoriesActivity extends AppCompatActivity
{

    private final String TAG = "storiesActivity";
    // user
    private String myUid;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stories);

        initializeView();

        FeedListFragment feedListFragment = FeedListFragment.newInstance(myUid, Const.FEED_STORIES, myUid, null);
        FragmentManager manager = getSupportFragmentManager();
        manager.beginTransaction()
                .add(R.id.content_stories_feed_cl, feedListFragment)
                //.replace(R.id.content_stories_feed_cl, feedListFragment)
                .commit();
    }

    private void initializeView()
    {
        myUid = getIntent().getStringExtra(Const.UID);

        Toolbar toolbar = (Toolbar) findViewById(R.id.stories_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // change the color of gradient
        if (android.os.Build.VERSION.SDK_INT >= 21)
        {
            Window window = StoriesActivity.this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.colorPrimaryDark));
        }

    }

    // back
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();

        if (id == android.R.id.home)
        {
            finish();
            overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }
}
