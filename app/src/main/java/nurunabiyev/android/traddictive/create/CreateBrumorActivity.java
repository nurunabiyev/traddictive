package nurunabiyev.android.traddictive.create;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;

import java.util.UUID;

import fr.ganfra.materialspinner.MaterialSpinner;
import nurunabiyev.android.traddictive.Const;
import nurunabiyev.android.traddictive.R;
import nurunabiyev.android.traddictive.objects.Post;
import nurunabiyev.android.traddictive.objects.User;

public class CreateBrumorActivity extends AppCompatActivity
{

    private final String TAG = "postBrumors";
    // view
    private MaterialSpinner brumorLanguageS;
    private EditText brumorET;
    // private CheckBox storyModeCB;
    private Button postBrumorB;
    private RadioGroup whoCanSeeRG;
    private RadioButton wholeBilkentRB;
    private RadioButton friendsOnlyRB;
    private RadioGroup postAsRG;
    private RadioButton anonymouslyRB;
    private RadioButton publiclyRB;
    // firebase
    private DatabaseReference mDatabaseReference;
    private String uId;
    private User mUser;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_brumor);

        initializeView();
        initializeFirebase();
    }

    private void initializeFirebase()
    {
        mDatabaseReference = FirebaseDatabase.getInstance().getReference();

        uId = getIntent().getStringExtra(Const.UID);

        mDatabaseReference.child(Const.USERS).child(uId)
                .addValueEventListener(new ValueEventListener()
                {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot)
                    {
                        mUser = dataSnapshot.getValue(User.class);
                        postBrumor();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError)
                    {
                    }
                });
    }

    private void postBrumor()
    {
        // todo v2 limits
        postBrumorB.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {

                // validating
                if (brumorET.getText().toString().length() < 21
                        || brumorET.getText().toString().length() > 2100)
                {
                    brumorET.setError(getString(R.string.error_invalid_text_longshort));
                    return;
                }

                final Post newBrumor = new Post();
                newBrumor.setType(Const.POST_TYPE_BRUMOR);
                newBrumor.setText(brumorET.getText().toString());
                newBrumor.setPostId(UUID.randomUUID().toString());
                newBrumor.setUploaderId(uId);
                newBrumor.setUploaderName(mUser.getFullName());
                newBrumor.setTimeSent(ServerValue.TIMESTAMP);
                newBrumor.setLanguage(brumorLanguageS.getSelectedItem().toString());    // todo display retaviely the language

                final String newBrumorKey = mDatabaseReference.child(Const.BRUMORS).push().getKey();
                newBrumor.setPostKey(newBrumorKey);


                // if whole bilkent -> post in brumors
                if (wholeBilkentRB.isChecked())
                {
                    if( brumorLanguageS.getSelectedItemPosition() == 0)
                    {
                        Toast.makeText(CreateBrumorActivity.this, getString(R.string.error_select_language), Toast.LENGTH_SHORT).show();
                        return;
                    }

                    newBrumor.setForFriendsOrBilkent(false);
                    newBrumor.setAnonymous(anonymouslyRB.isChecked());

                    // get and set incremented brumor number
                    mDatabaseReference.child(Const.BRUMORS).addListenerForSingleValueEvent(new ValueEventListener()
                    {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot)
                        {
                            newBrumor.setPostCount(Long.parseLong(((dataSnapshot == null) ? "0" : "" + dataSnapshot.getChildrenCount())) + 1);

                            mDatabaseReference.child(Const.BRUMORS).child(newBrumorKey)
                                    .setValue(newBrumor);
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError)
                        {
                        }

                    });
                }

                // if friends only -> post in profile
                else if (friendsOnlyRB.isChecked())
                {
                    newBrumor.setForFriendsOrBilkent(true);
                    newBrumor.setAnonymous(false);
                    newBrumor.setPostKey(newBrumorKey);

                    mDatabaseReference.child(Const.USERS).child(uId).child(Const.POSTS_LISTS).child(newBrumorKey)
                            .setValue(newBrumor);
                }



                Toast.makeText(CreateBrumorActivity.this, getString(R.string.posted), Toast.LENGTH_SHORT).show();
                finish();
            }
        });

    }

    private void initializeView()
    {
        Toolbar toolbar = (Toolbar) findViewById(R.id.post_brumor_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // change the color of gradient
        if (android.os.Build.VERSION.SDK_INT >= 21)
        {
            Window window = CreateBrumorActivity.this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.editorColorPrimaryDark));
        }

        brumorET = (EditText) findViewById(R.id.brumor_et);
        //storyModeCB = (CheckBox) findViewById(R.id.brumor_story_cb);
        postBrumorB = (Button) findViewById(R.id.post_brumor_b);
        whoCanSeeRG = (RadioGroup) findViewById(R.id.who_can_see_brumor_rg);
        postAsRG = (RadioGroup) findViewById(R.id.brumor_post_this_as_rg);
        wholeBilkentRB = (RadioButton) findViewById(R.id.whole_bilkent_rb);
        friendsOnlyRB = (RadioButton) findViewById(R.id.friends_only_rb);
        anonymouslyRB = (RadioButton) findViewById(R.id.anonymously_rb);
        publiclyRB = (RadioButton) findViewById(R.id.publicly_rb);
        brumorLanguageS = (MaterialSpinner) findViewById(R.id.brumor_language_s);

        // setting language spinner
        String[] languageArray = getResources().getStringArray(R.array.brumor_languages_array);
        ArrayAdapter<String> languageAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, languageArray);
        languageAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        brumorLanguageS.setAdapter(languageAdapter);

        // initial setup
        wholeBilkentRB.setChecked(true);
        friendsOnlyRB.setChecked(false);
        anonymouslyRB.setChecked(true);
        publiclyRB.setChecked(false);

        // playing with story mode stuff
        playWithButtons();
    }

    private void playWithButtons()
    {
        wholeBilkentRB.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                brumorLanguageS.setEnabled(true);
                anonymouslyRB.setEnabled(true);
            }
        });

        friendsOnlyRB.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                anonymouslyRB.setChecked(false);
                anonymouslyRB.setEnabled(false);
                publiclyRB.setChecked(true);

                brumorLanguageS.setEnabled(false);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();

        if (id == android.R.id.home)
            finish();

        return super.onOptionsItemSelected(item);
    }

}
