package nurunabiyev.android.traddictive.create;

import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import fr.ganfra.materialspinner.MaterialSpinner;
import mabbas007.tagsedittext.TagsEditText;
import nurunabiyev.android.traddictive.Const;
import nurunabiyev.android.traddictive.R;
import nurunabiyev.android.traddictive.objects.Hangout;
import nurunabiyev.android.traddictive.objects.Post;
import nurunabiyev.android.traddictive.objects.User;

public class CreateHangoutActivity extends AppCompatActivity
{

    private final String TAG = "createHangoutActivity";
    // view
    @BindView(R.id.create_hangout_toolbar)
    Toolbar createHangoutToolbar;
    @BindView(R.id.hangout_title_et)
    EditText hangoutTitleEt;
    @BindView(R.id.hangout_description_et)
    EditText hangoutDescriptionEt;
    @BindView(R.id.tags_et)
    TagsEditText tagsEt;
    @BindView(R.id.hangout_language_s)
    MaterialSpinner hangoutLanguageS;
    @BindView(R.id.hangout_type_open_rb)
    RadioButton hangoutTypeOpenRb;
    @BindView(R.id.hangout_type_closed_rb)
    RadioButton hangoutTypeClosedRb;
    @BindView(R.id.hangout_type_rg)
    RadioGroup hangoutTypeRg;
    @BindView(R.id.entrance_type_free_rb)
    RadioButton entranceTypeFreeRb;
    @BindView(R.id.entrance_type_with_approval_rb)
    RadioButton entranceTypeWithApprovalRb;
    @BindView(R.id.entrance_type_rg)
    RadioGroup entranceTypeRg;

    @BindView(R.id.create_hangout_b)
    AppCompatButton createHangoutB;

    // firebase
    private DatabaseReference mDatabaseReference;
    private String uId;
    private User mUser;
    // data
    private List<String> mTags;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_hangout);
        ButterKnife.bind(this);

        initializeView();
        initializeFirebase();
    }

    private void initializeView()
    {
        setSupportActionBar(createHangoutToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // change the color of gradient
        if (Build.VERSION.SDK_INT >= 21)
        {
            Window window = CreateHangoutActivity.this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.editorColorPrimaryDark));
        }

        mTags = new ArrayList<>();

        tagsEt.setThreshold(1);

        tagsEt.setTagsListener(new TagsEditText.TagsEditListener()
        {
            @Override
            public void onTagsChanged(Collection<String> collection)
            {
                mTags = (List<String>) collection;
            }

            @Override
            public void onEditingFinished()
            {

            }
        });

        // setting language spinner
        String[] languageArray = getResources().getStringArray(R.array.hangout_languages_array);
        ArrayAdapter<String> languageAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, languageArray);
        languageAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        hangoutLanguageS.setAdapter(languageAdapter);

        // radio buttons stuff
        hangoutTypeOpenRb.setChecked(true);
        hangoutTypeClosedRb.setChecked(false);
        entranceTypeWithApprovalRb.setChecked(true);
        entranceTypeFreeRb.setChecked(false);

        hangoutTypeOpenRb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                // if closed
                if (!isChecked)
                {
                    entranceTypeFreeRb.setEnabled(false);
                    entranceTypeWithApprovalRb.setEnabled(false);
                }
                // if open
                else
                {
                    entranceTypeFreeRb.setEnabled(true);
                    entranceTypeWithApprovalRb.setEnabled(true);
                }
            }
        });

    }

    private void initializeFirebase()
    {
        mDatabaseReference = FirebaseDatabase.getInstance().getReference();

        uId = getIntent().getStringExtra(Const.UID);

        mDatabaseReference.child(Const.USERS).child(uId)
                .addValueEventListener(new ValueEventListener()
                {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot)
                    {
                        mUser = dataSnapshot.getValue(User.class);
                        createHangout();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError)
                    {
                    }
                });
    }


    private void createHangout()
    {
        createHangoutB.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if (!isValid())
                    return;
                else
                {

                    String hangoutKey = mDatabaseReference.push().getKey();

                    // creating new hangout
                    Hangout newHangout = new Hangout();
                    newHangout.setTitle(hangoutTitleEt.getText().toString());
                    newHangout.setDescription(hangoutDescriptionEt.getText().toString());
                    newHangout.setLanguage(hangoutLanguageS.getSelectedItem().toString());
                    newHangout.setOpenOrClosed(hangoutTypeOpenRb.isChecked());
                    newHangout.setFreeOrApproval(entranceTypeFreeRb.isChecked());
                    newHangout.setTags(mTags);
                    newHangout.setHost(uId);
                    newHangout.setKey(hangoutKey);
                    newHangout.setPopular(false); // at the time of creation it is false

                    // todo v2 decrement limits
                    // if open
                    if (hangoutTypeOpenRb.isChecked())
                    {
                        // create hangout in Hangouts
                        mDatabaseReference.child(Const.HANGOUTS).child(hangoutKey)
                                .setValue(newHangout);
                        // add host (me) to member list too
                        mDatabaseReference.child(Const.HANGOUTS).child(hangoutKey).child(Const.HANGOUT_MEMBERS)
                                .child(uId).setValue("1");

                        //add hangout key to my hangoutsList
                        mDatabaseReference.child(Const.USERS).child(uId).child(Const.HANGOUTS_LIST)
                                .child(hangoutKey).setValue("1");

                        // create post in my postsList that i have created hangout
                        Post newPost = new Post();
                        newPost.setType(Const.POST_TYPE_HANGOUT);
                        newPost.setText(getString(R.string.has_created_hangout) + "\n\n" + newHangout.getTitle());
                        newPost.setPostKey(hangoutKey);
                        newPost.setUploaderId(uId);
                        newPost.setUploaderName(mUser.getFullName());
                        newPost.setTimeSent(ServerValue.TIMESTAMP);
                        newPost.setAnonymous(false);
                        newPost.setForFriendsOrBilkent(true);

                        mDatabaseReference.child(Const.USERS).child(uId).child(Const.POSTS_LISTS).child(hangoutKey)
                                .setValue(newPost);
                    }


                    // if closed
                    else if (hangoutTypeClosedRb.isChecked())
                    {
                        // create(but do not show) hangout in Hangouts
                        mDatabaseReference.child(Const.HANGOUTS).child(hangoutKey)
                                .setValue(newHangout);

                        // add host (me) to member list too
                        mDatabaseReference.child(Const.HANGOUTS).child(hangoutKey).child(Const.HANGOUT_MEMBERS)
                                .child(uId).setValue("1");

                        // add hangout key to hangoutsList
                        mDatabaseReference.child(Const.USERS).child(uId).child(Const.HANGOUTS_LIST)
                                .child(hangoutKey).setValue("1");
                    }


                    Toast.makeText(getApplicationContext(), getString(R.string.posted), Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();

        if (id == android.R.id.home)
            finish();

        return super.onOptionsItemSelected(item);
    }

    private boolean isValid()
    {
        boolean valid = true;

        if (mTags.size() == 0)
        {
            valid = false;
            hangoutTitleEt.setError(getString(R.string.error_no_tags));
        }

        if (hangoutTitleEt.getText().toString().length() < 10
                || hangoutTitleEt.getText().toString().length() > 150
                || hangoutTitleEt.getText().toString().contains("\uD83D\uDD12"))
        {
            valid = false;
            hangoutTitleEt.setError(getString(R.string.invalid_input));
        }

        if (hangoutDescriptionEt.getText().toString().length() < 50
                || hangoutDescriptionEt.getText().toString().length() > 1000)
        {
            valid = false;
            hangoutDescriptionEt.setError(getString(R.string.error_invalid_text_longshort));
        }

        if (tagsEt.getText().toString().length() < 10
                || tagsEt.getText().toString().length() > 200)
        {
            valid = false;
            tagsEt.setError(getString(R.string.error_invalid_text_longshort));
        } else if (hangoutLanguageS.getSelectedItemPosition() == 0)
        {
            valid = false;
            Toast.makeText(getApplicationContext(), getString(R.string.error_select_language), Toast.LENGTH_SHORT).show();
        }


        if (!valid)
            Toast.makeText(getApplicationContext(), getString(R.string.error_check_all_fields), Toast.LENGTH_SHORT).show();


        return valid;
    }

}
