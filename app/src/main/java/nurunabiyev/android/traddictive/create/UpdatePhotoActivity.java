package nurunabiyev.android.traddictive.create;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.UUID;

import nurunabiyev.android.traddictive.Const;
import nurunabiyev.android.traddictive.R;
import nurunabiyev.android.traddictive.objects.Photo;
import nurunabiyev.android.traddictive.objects.User;

/**
 * Activity opens after pressing fab
 */

public class UpdatePhotoActivity extends AppCompatActivity
{
    public static final String TAG = "postphotoactivity";
    private static final int PICK_IMAGE = 10;
    public static final int MAX_CHARS = 210;
    private static final int MAX_LINES = 10;
    // view
    private Button pickPhotoB;
    private Button postB;
    private EditText photoStatusET;
    private ImageView previewIV;
    private TextView previewTV;
    private Bitmap photo;
    // firebase
    private DatabaseReference mDatabaseReference;
    private StorageReference mStorageReference;
    private FirebaseStorage mStorage;
    private String uId;
    private User mUser;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_photo);

        initializeFirebase();
        initializeView();
    }

    private void initializeFirebase()
    {
        mDatabaseReference = FirebaseDatabase.getInstance().getReference();
        mStorageReference = FirebaseStorage.getInstance().getReference();

        uId = getIntent().getStringExtra(Const.UID);

        mDatabaseReference.child(Const.USERS).child(uId)
                .addValueEventListener(new ValueEventListener()
                {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot)
                    {
                        mUser = dataSnapshot.getValue(User.class);
                        postPhoto();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError)
                    {
                    }
                });
    }


    private void initializeView()
    {
        previewIV = (ImageView) findViewById(R.id.preview_iv);
        previewTV = (TextView) findViewById(R.id.preview_tv);
        pickPhotoB = (Button) findViewById(R.id.pick_photo_b);
        postB = (Button) findViewById(R.id.post_photo_b);
        photoStatusET = (EditText) findViewById(R.id.photo_status_et);

        Toolbar toolbar = (Toolbar) findViewById(R.id.post_photo_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // change the color of gradient
        if (android.os.Build.VERSION.SDK_INT >= 21)
        {
            Window window = UpdatePhotoActivity.this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.editorColorPrimaryDark));
        }

        pickPhotoB.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                // opening intent to select photo
                Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
                getIntent.setType("image/*");

                Intent pickIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                pickIntent.setType("image/*");

                Intent chooserIntent = Intent.createChooser(getIntent, getString(R.string.select_image));
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[]{pickIntent});

                startActivityForResult(chooserIntent, PICK_IMAGE);
            }
        });

    }

    private void postPhoto()
    {
        postB.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                // restrictions on input text
                if (photo == null)
                {
                    Toast.makeText(UpdatePhotoActivity.this, R.string.image_not_picked, Toast.LENGTH_SHORT).show();
                    return;
                }
                if (photoStatusET.getText().toString().length() == 0)
                {
                    Toast.makeText(UpdatePhotoActivity.this, R.string.write_something, Toast.LENGTH_SHORT).show();
                    return;
                }
                if (photoStatusET.getText().toString().length() > MAX_CHARS
                        || photoStatusET.getLineCount() > MAX_LINES)
                {
                    Toast.makeText(UpdatePhotoActivity.this, R.string.char_number_exceeds, Toast.LENGTH_SHORT).show();
                    return;
                }

                final String photoText = photoStatusET.getText().toString();

                Toast.makeText(UpdatePhotoActivity.this, "Starting uploading...", Toast.LENGTH_SHORT).show();

                new AsyncTask<Void, Void, Void>()
                {
                    @Override
                    protected Void doInBackground(Void... params)
                    {

                        // todo v2 decrement limits

                        // compressing & converting
                        ByteArrayOutputStream stream = new ByteArrayOutputStream();

                        // creating random id and sending to firebase storage
                        String imageId = UUID.randomUUID().toString();

                        // creating thumbnail
                        Bitmap thumbPhoto = scaleDown(photo, 225, true);
                        thumbPhoto.compress(Bitmap.CompressFormat.PNG, 100, stream);
                        byte[] thumbByteArray = stream.toByteArray();

                        // sending thumbnail
                        mStorageReference.child(Const.USERS).child(uId).child(Const.PHOTOS_LISTS)
                                .child(Const.PHOTO_THUMBNAIL + imageId)
                                .putBytes(thumbByteArray);


                        // creating preview
                        stream = new ByteArrayOutputStream();
                        Bitmap previewPhoto = scaleDown(photo, 450, true);
                        previewPhoto.compress(Bitmap.CompressFormat.PNG, 100, stream);
                        byte[] previewByteArray = stream.toByteArray();

                        // sending preview
                        mStorageReference.child(Const.USERS).child(uId).child(Const.PHOTOS_LISTS)
                                .child(Const.PHOTO_PREVIEW + imageId)
                                .putBytes(previewByteArray);


                        // creating original
                        stream = new ByteArrayOutputStream();
                        Bitmap originalPhoto = scaleDown(photo, 1000, true);
                        originalPhoto.compress(Bitmap.CompressFormat.PNG, 100, stream);
                        byte[] originalByteArray = stream.toByteArray();

                        // sending original
                        mStorageReference.child(Const.USERS).child(uId).child(Const.PHOTOS_LISTS)
                                .child(Const.PHOTO_ORIGINAL + imageId)
                                .putBytes(originalByteArray)
                                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>()
                                {
                                    @Override
                                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot)
                                    {
                                        Toast.makeText(UpdatePhotoActivity.this, "Your photo has been uploaded", Toast.LENGTH_SHORT).show();
                                    }
                                })
                                .addOnFailureListener(new OnFailureListener()
                                {
                                    @Override
                                    public void onFailure(@NonNull Exception e)
                                    {
                                        Toast.makeText(UpdatePhotoActivity.this, "Could not upload photo", Toast.LENGTH_SHORT).show();
                                    }
                                })
                                .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>()
                                {
                                    @Override
                                    public void onProgress(UploadTask.TaskSnapshot taskSnapshot)
                                    {
                                        @SuppressWarnings("VisibleForTests") long cbt = taskSnapshot.getBytesTransferred();
                                        @SuppressWarnings("VisibleForTests") long tbt = taskSnapshot.getTotalByteCount();
                                        Log.d(TAG, "onProgress: " + (cbt * 100 / tbt));
                                    }
                                });


                        String photoKey = mDatabaseReference.child(Const.USERS).child(uId).child(Const.POSTS_LISTS).push().getKey();

                        // sending image id to user's list of photos
                        Photo newPhoto = new Photo();
                        newPhoto.setPostId(imageId);
                        newPhoto.setPostKey(photoKey);
                        newPhoto.setDislikes(null);
                        newPhoto.setAnonymous(false);
                        newPhoto.setForFriendsOrBilkent(true);
                        newPhoto.setText(photoText);
                        newPhoto.setType(Const.POST_TYPE_PHOTO);
                        newPhoto.setUploaderId(uId);
                        newPhoto.setUploaderName(mUser.getFullName());
                        //newPhoto.setUploaderPhotoId(mUser.getProfilePhotoId());
                        Object timeSent = ServerValue.TIMESTAMP; // long
                        newPhoto.setTimeSent(timeSent);

                        // whole photo as apost

                        mDatabaseReference.child(Const.USERS).child(uId).child(Const.POSTS_LISTS).child(photoKey)
                                .setValue(newPhoto);
                        // preview only - for scroll part
                        mDatabaseReference.child(Const.USERS).child(uId).child(Const.PHOTOS_LISTS).child(photoKey)
                                .setValue(Const.PHOTO_PREVIEW + imageId);

                        // setting profile
                        mDatabaseReference.child(Const.USERS).child(uId).child(Const.PROFILE_PHOTO_ID)
                                .setValue(imageId);

                        return null;
                    }
                }.execute();

                finish();
            }
        });
    }

    // get photo from result
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE && resultCode == Activity.RESULT_OK)
        {
            if (data == null)
            {
                Toast.makeText(UpdatePhotoActivity.this, R.string.image_not_picked, Toast.LENGTH_SHORT).show();
                return;
            }

            // image picked
            try
            {
                // getting photo
                InputStream inputStream = getBaseContext().getContentResolver().openInputStream(data.getData());
                photo = BitmapFactory.decodeStream(inputStream);

                // displaying preview
                previewIV.setImageResource(0);
                Drawable draw = resize(photo);
                previewIV.setImageDrawable(draw);

                previewTV.setText(R.string.photo_preview);
                pickPhotoB.setText(R.string.pick_different_photo);
            } catch (FileNotFoundException e)
            {
                Toast.makeText(UpdatePhotoActivity.this, R.string.image_not_supported, Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();

        if (id == android.R.id.home)
            finish();

        return super.onOptionsItemSelected(item);
    }

    // some images are not displayed, resizing them
    private Drawable resize(Bitmap image)
    {
        Bitmap bitmapResized = Bitmap.createScaledBitmap(image,
                (int) (image.getWidth() * 0.5), (int) (image.getHeight() * 0.5), false);
        return new BitmapDrawable(getResources(), bitmapResized);
    }

    //Needed to resize image for photos
    private Bitmap scaleDown(Bitmap realImage, float maxImageSize, boolean filter)
    {
        float ratio = Math.min(
                (float) maxImageSize / realImage.getWidth(),
                (float) maxImageSize / realImage.getHeight());
        int width = Math.round((float) ratio * realImage.getWidth());
        int height = Math.round((float) ratio * realImage.getHeight());

        Bitmap newBitmap = Bitmap.createScaledBitmap(realImage, width,
                height, filter);
        return newBitmap;
    }
}
