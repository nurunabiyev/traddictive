package nurunabiyev.android.traddictive.create;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import fr.ganfra.materialspinner.MaterialSpinner;
import nurunabiyev.android.traddictive.Const;
import nurunabiyev.android.traddictive.R;
import nurunabiyev.android.traddictive.objects.Product;

public class CreateBuykentActivity extends AppCompatActivity
{
    private final String TAG = "createBuykent";

    // views
    @BindView(R.id.product_name_et)
    EditText mProductNameET;
    @BindView(R.id.product_description_et)
    EditText mProductDescriptionET;
    @BindView(R.id.product_price_et)
    EditText mPriceET;
    @BindView(R.id.user_phone_et)
    EditText mPhoneET;
    @BindView(R.id.post_buykent_b)
    Button mPostButton;
    @BindView(R.id.product_category_s)
    MaterialSpinner mCategoryS;
    @BindView(R.id.product_condition_s)
    MaterialSpinner mConditionS;
    // firebase
    private DatabaseReference mDatabaseReference;
    private String uId;
    private String uName;
    private String uEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_buykent);

        initializeView();
        initializeFirebase();
    }

    private void initializeView()
    {
        Toolbar toolbar = (Toolbar) findViewById(R.id.sell_buykent_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // change the color of gradient
        if (android.os.Build.VERSION.SDK_INT >= 21)
        {
            Window window = CreateBuykentActivity.this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.editorColorPrimaryDark));
        }

        ButterKnife.bind(this);

        // setting product condition spinner
        String[] productConditions = getResources().getStringArray(R.array.product_conditions);
        ArrayAdapter<String> conditionAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, productConditions);
        conditionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mConditionS.setAdapter(conditionAdapter);

        // setting product category spinner
        String[] productCategories = getResources().getStringArray(R.array.product_category);
        ArrayAdapter<String> categoryAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, productCategories);
        categoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mCategoryS.setAdapter(categoryAdapter);
    }


    private void initializeFirebase()
    {
        mDatabaseReference = FirebaseDatabase.getInstance().getReference();
        uId = getIntent().getStringExtra(Const.UID);

        mDatabaseReference.child(Const.USERS).child(uId)
                .addValueEventListener(new ValueEventListener()
                {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot)
                    {
                        uName = dataSnapshot.child(Const.FULL_NAME).getValue(String.class);
                        uEmail = dataSnapshot.child(Const.USER_EMAIL).getValue(String.class);
                        sellInBuykent();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError)
                    {
                    }
                });
    }

    private void sellInBuykent()
    {
        mPostButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (!isValid())
                    return;

                // generating new key
                String newProductKey = mDatabaseReference.push().getKey();

                // creating product
                Product newProduct = new Product();
                newProduct.setTitle(mProductNameET.getText().toString());
                newProduct.setDescription(mProductDescriptionET.getText().toString());
                newProduct.setCondition(mConditionS.getSelectedItem().toString());
                newProduct.setCategory(mCategoryS.getSelectedItem().toString());
                newProduct.setPrice(Integer.parseInt(mPriceET.getText().toString()));

                newProduct.setUploaderName(uName);
                newProduct.setUploaderId(uId);
                newProduct.setUploaderEmail(uEmail);
                newProduct.setUploaderPhone(mPhoneET.getText().toString());

                newProduct.setTimeSent(ServerValue.TIMESTAMP);
                newProduct.setProductKey(newProductKey);

                // posting product
                mDatabaseReference.child(Const.BUYKENT).child(mCategoryS.getSelectedItem().toString())
                        .child(newProductKey).setValue(newProduct);

                // add buykent key to buykent list in my profile
                mDatabaseReference.child(Const.USERS).child(uId).child(Const.BUYKENT_LIST)
                        .child(newProductKey).setValue(newProductKey);


                Toast.makeText(getApplicationContext(), getString(R.string.posted), Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();

        if (id == android.R.id.home)
            finish();

        return super.onOptionsItemSelected(item);
    }

    private boolean isValid()
    {
        boolean isValid = true;

        if (mProductNameET.getText().toString().length() < 10)
        {
            mProductNameET.setError(getString(R.string.error_invalid_text_longshort));
            isValid = false;
        } else if (mProductDescriptionET.getText().toString().length() < 10)
        {
            mProductDescriptionET.setError(getString(R.string.error_invalid_text_longshort));
            isValid = false;
        } else if (mPriceET.getText().toString().length() == 0)
        {
            mPriceET.setError(getString(R.string.error_invalid_text_longshort));
            isValid = false;
        } else if (mCategoryS.getSelectedItemPosition() == 0)
        {
            isValid = false;
            Toast.makeText(getApplicationContext(), getString(R.string.error_select_category), Toast.LENGTH_SHORT).show();
        } else if (mConditionS.getSelectedItemPosition() == 0)
        {
            isValid = false;
            Toast.makeText(getApplicationContext(), getString(R.string.error_select_condition), Toast.LENGTH_SHORT).show();
        }


        return isValid;
    }
}
