package nurunabiyev.android.traddictive.hangout;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import nurunabiyev.android.traddictive.Const;
import nurunabiyev.android.traddictive.R;
import nurunabiyev.android.traddictive.objects.HangoutMessage;

import static nurunabiyev.android.traddictive.R.id.layout_chat_chatbox;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link HangoutMessageListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HangoutMessageListFragment extends Fragment
{
    private static final String ARG_PARAM_KEY = "param1";
    private static final String ARG_PARAM_MYUID = "param2";
    private static final String ARG_PARAM_HOST_OR_GROUP = "param3";
    private static final String ARG_PARAM_HOST_ID = "param4";

    // data & user
    private List<HangoutMessage> mHangoutMessages;
    private DatabaseReference mDatabaseReference;
    private String mUserName;
    private String mHangoutKey;
    private String myUid;
    private String mHostId;
    private boolean mHostOrGroup;
    // view
    private RecyclerView mRecyclerView;
    private MessageAdapter mAdapter;
    private EditText mWriteMsgET;
    private Button mSendMsgB;
    private TextView mNoMessagesFoundTV;
    private View dividerChatView;
    private LinearLayoutManager mLayoutManager;
    @BindView(layout_chat_chatbox)
    LinearLayout chatboxLL;

    public HangoutMessageListFragment()
    {
        // Required empty public constructor
    }

    public static HangoutMessageListFragment newInstance(String keyParam, String myUid,
                                                         boolean hostOrGroup, String hostId)
    {
        HangoutMessageListFragment fragment = new HangoutMessageListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM_KEY, keyParam);
        args.putString(ARG_PARAM_MYUID, myUid);
        args.putString(ARG_PARAM_HOST_ID, hostId);
        args.putBoolean(ARG_PARAM_HOST_OR_GROUP, hostOrGroup);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        mDatabaseReference = FirebaseDatabase.getInstance().getReference();
        mHangoutMessages = new ArrayList<>();

        if (getArguments() != null)
        {
            mHangoutKey = getArguments().getString(ARG_PARAM_KEY);
            myUid = getArguments().getString(ARG_PARAM_MYUID);
            mHostId = getArguments().getString(ARG_PARAM_HOST_ID);
            mHostOrGroup = getArguments().getBoolean(ARG_PARAM_HOST_OR_GROUP);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        // Inflate the layout for this
        View view = inflater.inflate(R.layout.fragment_hangout_message_list, container, false);

        ButterKnife.bind(this, view);

        mWriteMsgET = (EditText) view.findViewById(R.id.hangout_write_message_et);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.hangout_host_messages_rv);
        mSendMsgB = (Button) view.findViewById(R.id.hangout_send_message_b);
        mNoMessagesFoundTV = (TextView) view.findViewById(R.id.no_messages_found);
        dividerChatView = view.findViewById(R.id.divider_chat);

        mLayoutManager = new LinearLayoutManager(getContext());
        mLayoutManager.setReverseLayout(true);
        mRecyclerView.setLayoutManager(mLayoutManager);

        // checking if the user is hangout member
        mDatabaseReference.child(Const.HANGOUTS).child(mHangoutKey)
                .child(Const.HANGOUT_MEMBERS).addListenerForSingleValueEvent(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                boolean iAmMember = false;

                for (DataSnapshot memberSnap : dataSnapshot.getChildren())
                {
                    if (memberSnap.getKey().equals(myUid))
                    {
                        iAmMember = true;
                        break;
                    }
                }

                if (iAmMember)
                    initializeUser();
                else
                    initializeNotAMember();
            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {

            }
        });

        return view;
    }

    private void initializeNotAMember()
    {
        mRecyclerView.setVisibility(View.GONE);
        dividerChatView.setVisibility(View.GONE);
        mSendMsgB.setVisibility(View.GONE);
        mWriteMsgET.setVisibility(View.GONE);
        chatboxLL.setVisibility(View.GONE);
        mNoMessagesFoundTV.setText("join the hangout to see the discussion!");
    }

    private void initializeUser()
    {
        mDatabaseReference.child(Const.USERS)
                .child(myUid).child(Const.FULL_NAME).addListenerForSingleValueEvent(user);
    }

    ValueEventListener user = new ValueEventListener()
    {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot)
        {
            mUserName = dataSnapshot.getValue(String.class);

            // initializing host's messages view
            if (mHostOrGroup)
                initHostMessage();

                // initializing chat messages view
            else
                initChatMessages();
        }

        @Override
        public void onCancelled(DatabaseError databaseError)
        {

        }
    };

    private void initHostMessage()
    {
        mAdapter = new MessageAdapter(mHangoutMessages);
        mRecyclerView.setAdapter(mAdapter);

        mDatabaseReference.child(Const.HANGOUTS).child(mHangoutKey)
                .child(Const.HANGOUT_HOST_MESSAGES).addChildEventListener(new ChildEventListener()
        {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s)
            {
                HangoutMessage currentMessage = dataSnapshot.getValue(HangoutMessage.class);
                mHangoutMessages.add(0, currentMessage);

                //mAdapter.notifyItemInserted(mHangoutMessages.size() - 1);
                mAdapter.notifyDataSetChanged();

                if (mHangoutMessages.size() == 0)
                {
                    mNoMessagesFoundTV.setText("nothing found");
                    mNoMessagesFoundTV.setVisibility(View.VISIBLE);
                } else
                {
                    mRecyclerView.setVisibility(View.VISIBLE);
                    mNoMessagesFoundTV.setVisibility(View.GONE);
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s)
            {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot)
            {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s)
            {

            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {

            }
        });

        if (!mHostId.equals(myUid))
        {
            mSendMsgB.setEnabled(false);
            mSendMsgB.setClickable(false);
            mWriteMsgET.setEnabled(false);
            mWriteMsgET.setClickable(false);
            mWriteMsgET.setHint("Only host can send messages");
            mSendMsgB.setVisibility(View.GONE);
        }

        mSendMsgB.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String newHostMessageKey = mDatabaseReference.push().getKey();

                HangoutMessage newHostMessage = new HangoutMessage();
                newHostMessage.setText(mWriteMsgET.getText().toString());
                newHostMessage.setUploaderId(myUid);
                newHostMessage.setUploaderName(mUserName);
                Object ts = ServerValue.TIMESTAMP;
                newHostMessage.setTimeSent(ts);
                newHostMessage.setMessageKey(newHostMessageKey);

                mDatabaseReference.child(Const.HANGOUTS).child(mHangoutKey).child(Const.HANGOUT_HOST_MESSAGES)
                        .child(newHostMessageKey).setValue(newHostMessage);

                mWriteMsgET.setText("");
            }
        });
    }

    private void initChatMessages()
    {
        mAdapter = new MessageAdapter(mHangoutMessages);
        mRecyclerView.setAdapter(mAdapter);

        if (mHangoutMessages.size() == 0)
        {
            mNoMessagesFoundTV.setText("nothing found");
            mNoMessagesFoundTV.setVisibility(View.VISIBLE);
        } else
        {
            mRecyclerView.setVisibility(View.VISIBLE);
            mNoMessagesFoundTV.setVisibility(View.GONE);
        }

        // downloading 250+ messages
        mDatabaseReference.child(Const.HANGOUTS).child(mHangoutKey).child(Const.HANGOUT_CHAT_MESSAGES)
                .orderByKey().limitToLast(250).addChildEventListener(new ChildEventListener()
        {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s)
            {
                HangoutMessage currentMessage = dataSnapshot.getValue(HangoutMessage.class);
                mHangoutMessages.add(0, currentMessage);

                mAdapter.notifyDataSetChanged();

                if (mHangoutMessages.size() == 0)
                {
                    mNoMessagesFoundTV.setText("nothing found");
                    mNoMessagesFoundTV.setVisibility(View.VISIBLE);
                } else
                {
                    mRecyclerView.setVisibility(View.VISIBLE);
                    mNoMessagesFoundTV.setVisibility(View.GONE);
                }

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s)
            {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot)
            {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s)
            {

            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {

            }
        });


        mSendMsgB.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                mSendMsgB.setClickable(false);

                if (mWriteMsgET.getText().toString().length() == 0)
                    return;

                // check if hangout exists
                mDatabaseReference.child(Const.HANGOUTS).child(mHangoutKey).addListenerForSingleValueEvent(new ValueEventListener()
                {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot)
                    {
                        String key = dataSnapshot.child(Const.HANGOUT_KEY).getValue(String.class);

                        if (key != null)
                        {

                            String newMessageKey = mDatabaseReference.push().getKey();

                            HangoutMessage newMessage = new HangoutMessage();
                            newMessage.setText(mWriteMsgET.getText().toString());
                            newMessage.setUploaderId(myUid);
                            newMessage.setUploaderName(mUserName);
                            Object ts = ServerValue.TIMESTAMP;
                            newMessage.setTimeSent(ts);
                            newMessage.setMessageKey(newMessageKey);

                            mDatabaseReference.child(Const.HANGOUTS).child(mHangoutKey).child(Const.HANGOUT_CHAT_MESSAGES)
                                    .child(newMessageKey).setValue(newMessage);

                            mWriteMsgET.setText("");

                            mSendMsgB.setClickable(true);

                        }

                        // exiting hangout
                        else
                        {
                            getActivity().finish();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError)
                    {

                    }
                });

            }
        });

    }

    /**
     * View holder classes for Host Message, Sent Message & Received Message
     */
    private class HostMessageHolder extends RecyclerView.ViewHolder
    {
        private HangoutMessage mHostMessage;
        // view
        private TextView messageTextTV, timeTextTV;
        private TextView likesTV, dislikesTV;
        private ImageView likeIV, dislikeIV;

        HostMessageHolder(View itemView)
        {
            super(itemView);

            messageTextTV = (TextView) itemView.findViewById(R.id.host_message_tv);
            timeTextTV = (TextView) itemView.findViewById(R.id.host_time_tv);
            likesTV = (TextView) itemView.findViewById(R.id.host_msg_likes_tv);
            dislikesTV = (TextView) itemView.findViewById(R.id.host_msg_dislikes_tv);

            likeIV = (ImageView) itemView.findViewById(R.id.host_msg_like_iv);
            dislikeIV = (ImageView) itemView.findViewById(R.id.host_msg_dislike_iv);
        }

        void bindHostMessage(HangoutMessage hostMessage)
        {
            mHostMessage = hostMessage;

            messageTextTV.setText(hostMessage.getText());
            likesTV.setText(((hostMessage.getLikes() == null) ? "0" : "" + hostMessage.getLikes().size()));
            dislikesTV.setText((hostMessage.getDislikes() == null) ? "0" : "" + hostMessage.getDislikes().size());
            // date
            Date sentTimeDate = new Date(Long.parseLong(hostMessage.getTimeSent().toString()));
            String dateText = (String) DateFormat.format("d MMMM HH:mm", sentTimeDate);
            timeTextTV.setText(dateText);

            likeDislikeListeners();
        }

        private void likeDislikeListeners()
        {
            // setting like/dislike color
            for (Map.Entry<String, String> likedPerson : mHostMessage.getLikes().entrySet())
            {
                if (likedPerson.getValue().equals(myUid))
                {
                    likeIV.setColorFilter(ContextCompat.getColor(getContext(), R.color.clicked_blue));
                    likesTV.setTextColor(ContextCompat.getColor(getContext(), R.color.clicked_blue));
                    setButtonsInactive();
                }
            }

            for (Map.Entry<String, String> dislikedPerson : mHostMessage.getDislikes().entrySet())
            {
                if (dislikedPerson.getValue().equals(myUid))
                {
                    dislikeIV.setColorFilter(ContextCompat.getColor(getContext(), R.color.clicked_red));
                    dislikesTV.setTextColor(ContextCompat.getColor(getContext(), R.color.clicked_red));
                    setButtonsInactive();
                }
            }

            // liking comment
            likeIV.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    // changing the color and statically incrementing likes count
                    likeIV.setColorFilter(ContextCompat.getColor(getContext(), R.color.clicked_blue));
                    likesTV.setTextColor(ContextCompat.getColor(getContext(), R.color.clicked_blue));

                    int newLikeNumber = Integer.parseInt(likesTV.getText().toString()) + 1;
                    likesTV.setText(Integer.toString(newLikeNumber));

                    mDatabaseReference.child(Const.HANGOUTS).child(mHangoutKey).child(Const.HANGOUT_HOST_MESSAGES)
                            .child(mHostMessage.getMessageKey()).child(Const.POST_LIKES).push()
                            .setValue(myUid);

                    // making like button inactive
                    setButtonsInactive();

                }
            });
            // disliking comment
            dislikeIV.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    // changing the color and statically incrementing likes count
                    dislikeIV.setColorFilter(ContextCompat.getColor(getContext(), R.color.clicked_red));
                    dislikesTV.setTextColor(ContextCompat.getColor(getContext(), R.color.clicked_red));

                    int newLikeNumber = Integer.parseInt(dislikesTV.getText().toString()) + 1;
                    dislikesTV.setText(Integer.toString(newLikeNumber));

                    // sending my id to list of disliked users to firebase database
                    mDatabaseReference.child(Const.HANGOUTS).child(mHangoutKey).child(Const.HANGOUT_HOST_MESSAGES)
                            .child(mHostMessage.getMessageKey()).child(Const.POST_DISLIKES).push()
                            .setValue(myUid);

                    // making like button inactive
                    setButtonsInactive();
                }
            });
        }

        private void setButtonsInactive()
        {
            dislikeIV.setEnabled(false);
            dislikeIV.setClickable(false);
            likeIV.setEnabled(false);
            likeIV.setClickable(false);
        }
    }

    private class SentMessageHolder extends RecyclerView.ViewHolder
    {
        TextView messageText, timeText;

        SentMessageHolder(View itemView)
        {
            super(itemView);

            messageText = (TextView) itemView.findViewById(R.id.text_message_body);
            timeText = (TextView) itemView.findViewById(R.id.text_message_time);
        }

        void bind(HangoutMessage message)
        {
            messageText.setText(message.getText());

            Date sentTimeDate = new Date(Long.parseLong(message.getTimeSent().toString()));
            String dateText = (String) DateFormat.format("HH:mm", sentTimeDate);
            timeText.setText(dateText);
        }
    }

    private class ReceivedMessageHolder extends RecyclerView.ViewHolder
    {
        TextView messageText, timeText, nameText;

        ReceivedMessageHolder(View itemView)
        {
            super(itemView);

            messageText = (TextView) itemView.findViewById(R.id.text_message_body);
            timeText = (TextView) itemView.findViewById(R.id.text_message_time);
            nameText = (TextView) itemView.findViewById(R.id.text_message_name);
        }

        void bind(HangoutMessage message)
        {
            messageText.setText(message.getText());
            nameText.setText(message.getUploaderName());

            Date sentTimeDate = new Date(Long.parseLong(message.getTimeSent().toString()));
            String dateText = (String) DateFormat.format("HH:mm", sentTimeDate);
            timeText.setText(dateText);
        }
    }

    private class MessageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
    {
        private static final int VIEW_TYPE_MESSAGE_SENT = 1;
        private static final int VIEW_TYPE_MESSAGE_RECEIVED = 2;
        private static final int VIEW_TYPE_HOST = 3;
        private List<HangoutMessage> hangoutMessages;
        private int lastPosition = -1;

        public MessageAdapter(List<HangoutMessage> hangoutMsgs)
        {
            hangoutMessages = hangoutMsgs;
        }

        @Override
        public int getItemViewType(int position)
        {
            if (mHostOrGroup)
                return VIEW_TYPE_HOST;
            else
            {
                HangoutMessage message = hangoutMessages.get(position);

                // If the current user is the sender of the message
                if (message.getUploaderId().equals(myUid))
                    return VIEW_TYPE_MESSAGE_SENT;
                    // If some other user sent the message
                else
                    return VIEW_TYPE_MESSAGE_RECEIVED;
            }
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
        {
//            LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
//            View view = inflater.inflate(R.layout.list_item_message_host, viewGroup, false);
//            //return new HostMessageHolder(view);
//            return null;

            View view;

            if (viewType == VIEW_TYPE_MESSAGE_SENT)
            {
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_message_sent, parent, false);
                return new SentMessageHolder(view);
            } else if (viewType == VIEW_TYPE_MESSAGE_RECEIVED)
            {
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_message_received, parent, false);
                return new ReceivedMessageHolder(view);
            } else if (viewType == VIEW_TYPE_HOST)
            {
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_message_host, parent, false);
                return new HostMessageHolder(view);
            }

            return null;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int i)
        {
            HangoutMessage message = mHangoutMessages.get(i);

            switch (holder.getItemViewType())
            {
                case VIEW_TYPE_MESSAGE_SENT:
                    ((SentMessageHolder) holder).bind(message);
                    break;
                case VIEW_TYPE_MESSAGE_RECEIVED:
                    ((ReceivedMessageHolder) holder).bind(message);
                    break;
                case VIEW_TYPE_HOST:
                    ((HostMessageHolder) holder).bindHostMessage(message);
            }

        }

//        @Override
//        public void onBindViewHolder(HostMessageHolder messageHolder, int i)
//        {
//            HangoutMessage message = mHangoutMessages.get(i);
//            messageHolder.bindHostMessage(message);
//
//            // setAnimation(hangoutHolder.hangoutCV, i);
//        }


        @Override
        public int getItemCount()
        {
            return hangoutMessages.size();
        }
    }
}
