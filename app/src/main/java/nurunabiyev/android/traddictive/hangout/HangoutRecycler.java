package nurunabiyev.android.traddictive.hangout;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.greenfrvr.hashtagview.HashtagView;

import java.util.Arrays;
import java.util.List;

import nurunabiyev.android.traddictive.Const;
import nurunabiyev.android.traddictive.R;
import nurunabiyev.android.traddictive.feed.TabbedActivity;
import nurunabiyev.android.traddictive.objects.Hangout;

/**
 * Created by HP on 12-Aug-17.
 */

public class HangoutRecycler
{

    private final String TAG = "hangout recycler";
    private DatabaseReference mDatabaseRef;
    private String myUid;
    private String otherUid;

    public HangoutRecycler(String uid, String otherUid)
    {
        mDatabaseRef = FirebaseDatabase.getInstance().getReference();
        myUid = uid;
        this.otherUid = otherUid;
    }


    private class HangoutHolder extends RecyclerView.ViewHolder
    {

        private CardView hangoutCV;
        private Hangout mHangout;
        private TextView titleTV;
        private TextView membersTV;
        private TextView languageTV;
        private HashtagView tagsHV;
        private Button joinB;


        private HangoutHolder(View itemView)
        {
            super(itemView);

            // initialize views
            hangoutCV = (CardView) itemView.findViewById(R.id.hangout_cv);
            titleTV = (TextView) itemView.findViewById(R.id.hangout_title_tv);
            membersTV = (TextView) itemView.findViewById(R.id.hangout_members_tv);
            languageTV = (TextView) itemView.findViewById(R.id.hangout_language_tv);
            tagsHV = (HashtagView) itemView.findViewById(R.id.hangout_tags_hv);
            joinB = (Button) itemView.findViewById(R.id.join_button_b);
        }

        private void bindHangout(Hangout hangout, final int position)
        {
            mHangout = hangout;

            // closed title has 🔒 emoji
            if (mHangout.isOpenOrClosed())
                titleTV.setText(mHangout.getTitle());
            else
                titleTV.setText("\uD83D\uDD12 " + mHangout.getTitle());

            tagsHV.setData(mHangout.getTags());
            membersTV.setText("" + mHangout.getMembers().size());

            // setting language code
            String[] languages = itemView.getContext().getResources()
                    .getStringArray(R.array.hangout_languages_array);
            String[] languageCodes = itemView.getContext().getResources()
                    .getStringArray(R.array.hangout_languages_array_language_codes);
            int languageCodeIndex = Arrays.asList(languages).indexOf(mHangout.getLanguage());
            languageTV.setText(languageCodes[languageCodeIndex]);

            // button colors and text
            if (mHangout.getHost().equals(myUid))
            {
                joinB.setText("host");
                joinB.setTextColor(itemView.getContext().getResources().getColor(R.color.host));
                joinB.setEnabled(false);
            } else if (mHangout.getMembers().containsKey(myUid))
            {
                joinB.setText("member");
                joinB.setTextColor(itemView.getContext().getResources().getColor(R.color.member));
                joinB.setEnabled(false);
            } else if (!mHangout.getMembers().containsKey(myUid))
            {
                if(mHangout.getApplicants().containsKey(myUid))
                {
                    joinB.setText("request sent");
                    joinB.setTextColor(itemView.getContext().getResources().getColor(R.color.member));
                    joinB.setEnabled(false);
                    return;
                }

                joinB.setText("join");
                joinB.setTextColor(itemView.getContext().getResources().getColor(R.color.colorAccent));
            }

            // joining
            joinB.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {

                    // if im not a member -> join (if free entrance) or
                    if (!mHangout.getMembers().containsKey(myUid))
                    {
                        // joining if free entrance
                        if (mHangout.isFreeOrApproval())
                        {
                            mDatabaseRef.child(Const.HANGOUTS).child(mHangout.getKey()).child(Const.HANGOUT_MEMBERS)
                                    .child(myUid).setValue("1");

                            // adding this hangout to my lists
                            mDatabaseRef.child(Const.USERS).child(myUid).child(Const.HANGOUTS_LIST)
                                    .child(mHangout.getKey()).setValue("1");

                            joinB.setText("member");
                            joinB.setTextColor(itemView.getContext().getResources().getColor(R.color.member));
                            joinB.setEnabled(false);

                            membersTV.setText("" + (Integer.parseInt(membersTV.getText().toString()) + 1));

                            Toast.makeText(itemView.getContext(), "You are now a member!", Toast.LENGTH_SHORT).show();
                        }

                        // send request (if with approval)
                        else if(!mHangout.isFreeOrApproval())
                        {
                            mDatabaseRef.child(Const.HANGOUTS).child(mHangout.getKey()).child(Const.HANGOUT_APPLICANTS)
                                    .child(myUid).setValue("1");

                            joinB.setText("request sent");
                            joinB.setTextColor(itemView.getContext().getResources().getColor(R.color.member));
                            joinB.setEnabled(false);
                        }

                    }

                }
            });

            // opening details of the hangout
            hangoutCV.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    Intent intent = new Intent(itemView.getContext(), TabbedActivity.class);
                    Bundle viewPagerSettings = new Bundle();
                    viewPagerSettings.putInt(Const.PARAM_PAGE_COUNT, 3);
                    viewPagerSettings.putString(Const.PARAM_PAGE_1, itemView.getContext().getString(R.string.hangout_details));
                    viewPagerSettings.putString(Const.PARAM_PAGE_2, itemView.getContext().getString(R.string.hangout_hosts_messages));
                    viewPagerSettings.putString(Const.PARAM_PAGE_3, itemView.getContext().getString(R.string.hangout_group_chat));
                    viewPagerSettings.putString(Const.UID, myUid);
                    viewPagerSettings.putString(Const.OTHER_UID, otherUid);
                    viewPagerSettings.putString(Const.PARAM_HANGOUT_KEY, mHangout.getKey());
                    viewPagerSettings.putString(Const.PARAM_HANGOUT_HOST_ID, mHangout.getHost());
                    viewPagerSettings.putString(Const.PARAM_PAGER_TYPE, Const.PARAM_HANGOUT_DETAILS);
                    intent.putExtra(Const.PARAM_BUNDLE, viewPagerSettings);
                    itemView.getContext().startActivity(intent);
                }
            });
            // same shit

            tagsHV.addOnTagClickListener(new HashtagView.TagsClickListener()
            {
                @Override
                public void onItemClicked(Object item)
                {
                    Intent intent = new Intent(itemView.getContext(), TabbedActivity.class);
                    Bundle viewPagerSettings = new Bundle();
                    viewPagerSettings.putInt(Const.PARAM_PAGE_COUNT, 3);
                    viewPagerSettings.putString(Const.PARAM_PAGE_1, itemView.getContext().getString(R.string.hangout_details));
                    viewPagerSettings.putString(Const.PARAM_PAGE_2, itemView.getContext().getString(R.string.hangout_hosts_messages));
                    viewPagerSettings.putString(Const.PARAM_PAGE_3, itemView.getContext().getString(R.string.hangout_group_chat));
                    viewPagerSettings.putString(Const.UID, myUid);
                    viewPagerSettings.putString(Const.OTHER_UID, otherUid);
                    viewPagerSettings.putString(Const.PARAM_HANGOUT_KEY, mHangout.getKey());
                    viewPagerSettings.putString(Const.PARAM_HANGOUT_HOST_ID, mHangout.getHost());
                    viewPagerSettings.putString(Const.PARAM_PAGER_TYPE, Const.PARAM_HANGOUT_DETAILS);
                    intent.putExtra(Const.PARAM_BUNDLE, viewPagerSettings);
                    itemView.getContext().startActivity(intent);
                }
            });


        }
    }

    public class HangoutAdapter extends RecyclerView.Adapter<HangoutHolder>
    {
        private List<Hangout> mHangouts;
        private int lastPosition = -1;
        private ViewGroup vg;

        public HangoutAdapter(List<Hangout> hangouts)
        {
            mHangouts = hangouts;
        }

        @Override
        public HangoutHolder onCreateViewHolder(ViewGroup viewGroup, int i)
        {
            LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
            View view = inflater.inflate(R.layout.list_item_hangout, viewGroup, false);
            vg = viewGroup;
            return new HangoutHolder(view);
        }

        @Override
        public void onBindViewHolder(HangoutHolder hangoutHolder, int i)
        {
            Hangout hangout = mHangouts.get(i);

            hangoutHolder.bindHangout(hangout, i);

            setAnimation(hangoutHolder.hangoutCV, i);
        }

        private void setAnimation(View viewToAnimate, int position)
        {
            // If the bound view wasn't previously displayed on screen, it's animated
            if (position > lastPosition)
            {
                Animation animation = AnimationUtils.loadAnimation(vg.getContext(), android.R.anim.fade_out);
                viewToAnimate.startAnimation(animation);
                lastPosition = position;
            }
        }

        @Override
        public int getItemCount()
        {
            return mHangouts.size();
        }


    }
}

