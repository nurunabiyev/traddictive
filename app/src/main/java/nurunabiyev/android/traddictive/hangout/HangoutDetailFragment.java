package nurunabiyev.android.traddictive.hangout;


import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.greenfrvr.hashtagview.HashtagView;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import nurunabiyev.android.traddictive.Const;
import nurunabiyev.android.traddictive.MoreActivity;
import nurunabiyev.android.traddictive.R;
import nurunabiyev.android.traddictive.feed.TabbedActivity;
import nurunabiyev.android.traddictive.objects.Hangout;
import nurunabiyev.android.traddictive.objects.Notification;
import nurunabiyev.android.traddictive.objects.User;
import nurunabiyev.android.traddictive.profile.ProfileActivity;

import static nurunabiyev.android.traddictive.R.id.hangout_detail_invite_people_b;
import static nurunabiyev.android.traddictive.R.id.hangout_detail_leave_b;
import static nurunabiyev.android.traddictive.R.id.hangout_detail_people_b;
import static nurunabiyev.android.traddictive.R.id.make_hangout_popular_tv;
import static nurunabiyev.android.traddictive.R.id.user_list_civ;
import static nurunabiyev.android.traddictive.R.id.user_list_department_tv;
import static nurunabiyev.android.traddictive.R.id.user_list_tv;
import static nurunabiyev.android.traddictive.R.string.applicants;
import static nurunabiyev.android.traddictive.R.string.members;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link HangoutDetailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HangoutDetailFragment extends Fragment
{
    // the fragment initialization parameters
    private static final String TAG = "hangoutdetailfragment";

    private static final String ARG_PARAM_KEY = "param1";
    private static final String ARG_PARAM_HOST_ID = "param2";
    private static final String ARG_PARAM_MY_ID = "param3";

    private DatabaseReference mDatabaseReference;
    private StorageReference mStorageReference;
    private String mKey;
    private String mHostId;
    private String myUid;
    // view
    private TextView mTitle;
    private TextView mDescription;
    private HashtagView mTags;
    private TextView mLanguage;
    @BindView(user_list_civ)
    CircleImageView hostCIV;
    @BindView(user_list_tv)
    TextView hostNameTV;
    @BindView(user_list_department_tv)
    TextView hostDepartmentTV;
    @BindView(hangout_detail_people_b)
    Button hangoutPeopleB;
    @BindView(hangout_detail_leave_b)
    Button hangoutLeaveB;
    @BindView(make_hangout_popular_tv)
    TextView contactPopularTV;
    @BindView(hangout_detail_invite_people_b)
    Button hangoutInviteB;

    // cvs
    private CardView titleCV;
    private CardView descriptionCV;
    private CardView tagsCV;
    private CardView languageCV;
    private CardView hostCV;
    private CardView membersCV;


    public HangoutDetailFragment()
    {
        // Required empty public constructor
    }

    public static HangoutDetailFragment newInstance(String myUid, String keyParam, String hostId)
    {
        HangoutDetailFragment fragment = new HangoutDetailFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM_KEY, keyParam);
        args.putString(ARG_PARAM_HOST_ID, hostId);
        args.putString(ARG_PARAM_MY_ID, myUid);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
        {
            mKey = getArguments().getString(ARG_PARAM_KEY);
            mHostId = getArguments().getString(ARG_PARAM_HOST_ID);
            myUid = getArguments().getString(ARG_PARAM_MY_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_hangout_details, container, false);

        ButterKnife.bind(this, view);

        mTitle = (TextView) view.findViewById(R.id.hangout_details_title_tv);
        mDescription = (TextView) view.findViewById(R.id.hangout_details_description_tv);
        mLanguage = (TextView) view.findViewById(R.id.hangout_details_language_tv);
        mTags = (HashtagView) view.findViewById(R.id.hangout_details_tags_hv);
        // card views
        titleCV = (CardView) view.findViewById(R.id.hangout_title_cv);
        descriptionCV = (CardView) view.findViewById(R.id.hangout_description_cv);
        tagsCV = (CardView) view.findViewById(R.id.hangout_tags_cv);
        languageCV = (CardView) view.findViewById(R.id.hangout_language_cv);
        hostCV = (CardView) view.findViewById(R.id.hangout_host_cv);
        membersCV = (CardView) view.findViewById(R.id.hangout_members_cv);

        hangoutLeaveB.setVisibility(View.GONE);

        if (myUid.equals(mHostId))
        {
            hangoutLeaveB.setVisibility(View.VISIBLE);
            hangoutLeaveB.setText(getString(R.string.delete_hangout));

            hangoutInviteB.setVisibility(View.VISIBLE);
            hangoutInviteB.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    Intent searchIntent = new Intent(getContext(), TabbedActivity.class);
                    Bundle viewPagerSettings = new Bundle();
                    viewPagerSettings.putInt(Const.PARAM_PAGE_COUNT, 1);
                    viewPagerSettings.putString(Const.PARAM_PAGE_1, getResources().getString(R.string.friends_list));
                    viewPagerSettings.putString(Const.UID, myUid);
                    viewPagerSettings.putString(Const.OTHER_UID, myUid);
                    viewPagerSettings.putString(Const.PARAM_PAGER_TYPE, Const.PARAM_INVITE_PEOPLE);
                    viewPagerSettings.putString(Const.PARAM_HANGOUT_KEY, mKey);
                    viewPagerSettings.putString(Const.PARAM_HANGOUT_HOST_ID, mHostId);

                    searchIntent.putExtra(Const.PARAM_BUNDLE, viewPagerSettings);
                    startActivity(searchIntent);
                }
            });
        }

        contactPopularTV.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent moreIntent = new Intent(getContext(), MoreActivity.class);
                moreIntent.putExtra(Const.UID, myUid);
                startActivity(moreIntent);
            }
        });

        wireHangout();

        return view;
    }

    private void wireHangout()
    {
        mDatabaseReference = FirebaseDatabase.getInstance().getReference();
        mStorageReference = FirebaseStorage.getInstance().getReference();

        mDatabaseReference.child(Const.HANGOUTS).child(mKey).addListenerForSingleValueEvent(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                final Hangout currentHangout = dataSnapshot.getValue(Hangout.class);

                // closed title has 🔒 emoji
                if(currentHangout.isOpenOrClosed())
                    mTitle.setText(currentHangout.getTitle());
                else
                    mTitle.setText("\uD83D\uDD12 " + currentHangout.getTitle());

                mDescription.setText(currentHangout.getDescription());
                mLanguage.setText(currentHangout.getLanguage());
                mTags.setData(currentHangout.getTags());

                // displaying people
                displayHost();

                // approve invitation if it has so
                if(currentHangout.getInvitedFriends().containsKey(myUid))
                {
                    hangoutInviteB.setText("Approve Invitation");
                    hangoutInviteB.setVisibility(View.VISIBLE);

                    hangoutInviteB.setOnClickListener(new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View view)
                        {
                            // delete notification
                            mDatabaseReference.child(Const.USERS).child(myUid).child(Const.NOTIFICATION_MANAGER)
                                    .child(Const.NOTIF_HANGOUT_INVITE).addListenerForSingleValueEvent(new ValueEventListener()
                            {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot)
                                {
                                    for(DataSnapshot invitesSnaps : dataSnapshot.getChildren())
                                    {
                                        Notification currentNotif = invitesSnaps.getValue(Notification.class);
                                        if(currentNotif.getHangoutKey().equals(mKey))
                                        {
                                            mDatabaseReference.child(Const.USERS).child(myUid).child(Const.NOTIFICATION_MANAGER)
                                                    .child(Const.NOTIF_HANGOUT_INVITE).child(invitesSnaps.getKey()).setValue(null);
                                            break;
                                        }
                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {}
                            });


                            // delete this uid from invited
                            mDatabaseReference.child(Const.HANGOUTS).child(mKey).child(Const.INVITED_FRIENDS)
                                    .child(myUid).setValue(null);

                            // make member4r
                            mDatabaseReference.child(Const.HANGOUTS).child(mKey).child(Const.HANGOUT_MEMBERS)
                                    .child(myUid).setValue("1");

                            // add this hangout to his hangout keys
                            mDatabaseReference.child(Const.USERS).child(myUid).child(Const.HANGOUTS_LIST)
                                    .child(mKey).setValue("1");

                            // toast that you are a member
                            Toast.makeText(getContext(), "You are now a member! Refresh to see the messages",
                                    Toast.LENGTH_LONG).show();

                            hangoutInviteB.setVisibility(View.GONE);

                        }
                    });
                }

                hangoutPeopleB.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View view)
                    {
                        displayPeople();
                    }
                });


                // show button on leave to me or show button to delete hangout to host
                if (currentHangout.getMembers().containsKey(myUid))
                {
                    hangoutLeaveB.setVisibility(View.VISIBLE);

                    hangoutLeaveB.setOnClickListener(new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View view)
                        {
                            leaveFunction();
                        }
                    });
                }


                animateViews();
            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {

            }
        });

    }

    private void leaveFunction()
    {
        // remove hangout if i'm the host
        if (mHostId.equals(myUid))
        {
            final AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
            alert.setTitle(getString(R.string.delete_hangout));
            alert.setMessage(getString(R.string.are_you_sure_to_delete));

            alert.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener()
            {
                public void onClick(DialogInterface dialog, int whichButton)
                {
                    dialog.dismiss();
                }
            });

            alert.setPositiveButton(getString(R.string.delete_hangout), new DialogInterface.OnClickListener()
            {
                public void onClick(final DialogInterface dialog, int whichButton)
                {

                    final ProgressDialog pd = ProgressDialog
                            .show(getContext(), null, getResources().getString(R.string.progress_dele_hangout), true);

                    // delete hangout
                    mDatabaseReference.child(Const.HANGOUTS).child(mKey).setValue(null);

                    // delete hangout reference from my hangout list
                    mDatabaseReference.child(Const.USERS).child(myUid)
                            .child(Const.HANGOUTS_LIST).child(mKey).setValue(null);

                    // delete hangout reference in all members
                    mDatabaseReference.child(Const.USERS).addListenerForSingleValueEvent(new ValueEventListener()
                    {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot)
                        {
                            for(DataSnapshot userSnap : dataSnapshot.getChildren())
                            {
                                User currentUser = userSnap.getValue(User.class);

                                if(currentUser.getHangoutsList().containsKey(mKey))
                                {
                                    mDatabaseReference.child(Const.USERS).child(currentUser.getUserId())
                                            .child(Const.HANGOUTS_LIST).child(mKey).setValue(null);
                                }
                            }

                            pd.dismiss();
                            dialog.dismiss();
                            getActivity().finish();
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError)
                        {

                        }
                    });


                }
            });

            alert.show();
        }

        // leave
        else
        {
            final AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
            alert.setTitle(getString(R.string.leave_hangout));
            alert.setMessage(getString(R.string.are_you_sure_to_leave));

            alert.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener()
            {
                public void onClick(DialogInterface dialog, int whichButton)
                {
                    dialog.dismiss();
                }
            });

            alert.setPositiveButton(getString(R.string.leave_hangout), new DialogInterface.OnClickListener()
            {
                public void onClick(DialogInterface dialog, int whichButton)
                {
                    mDatabaseReference.child(Const.HANGOUTS).child(mKey)
                            .child(Const.HANGOUT_MEMBERS).child(myUid).setValue(null);

                    // delete hangout reference from my hangout list
                    mDatabaseReference.child(Const.USERS).child(myUid)
                            .child(Const.HANGOUTS_LIST).child(mKey).setValue(null);


                    dialog.dismiss();
                    getActivity().finish();
                }
            });

            alert.show();
        }

    }

    private void displayHost()
    {
        // host name
        mDatabaseReference.child(Const.USERS).child(mHostId).addListenerForSingleValueEvent(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                final User hostUser = dataSnapshot.getValue(User.class);
                hostNameTV.setText(hostUser.getFullName());
                hostDepartmentTV.setText(hostUser.getDepartment());


                String profilePhotoId = Const.PHOTO_THUMBNAIL + dataSnapshot
                        .child(Const.PROFILE_PHOTO_ID).getValue(String.class);

                if (hostUser.getProfilePhotoId() != null)
                {
                    StorageReference storageReference = mStorageReference.child(Const.USERS).child(mHostId).child(Const.PHOTOS_LISTS)
                            .child(profilePhotoId);
                    if (getActivity() != null)
                        Glide.with(getActivity().getApplicationContext())
                                .using(new FirebaseImageLoader())
                                .load(storageReference)
                                .into(hostCIV);
                } else
                    hostCIV.setImageResource(R.drawable.default_avatar);

                // opening host's profile
                hostCV.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        Intent intent = new Intent(getContext(), ProfileActivity.class);
                        intent.putExtra(Const.FULL_NAME, hostUser.getFullName());
                        intent.putExtra(Const.UID, myUid);
                        intent.putExtra(Const.OTHER_UID, mHostId);
                        startActivity(intent);
                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {

            }
        });
    }

    private void displayPeople()
    {
        Intent intent = new Intent(getContext(), TabbedActivity.class);
        Bundle viewPagerSettings = new Bundle();
        viewPagerSettings.putInt(Const.PARAM_PAGE_COUNT, 2);
        viewPagerSettings.putString(Const.PARAM_PAGE_1, getContext().getString(members));
        viewPagerSettings.putString(Const.PARAM_PAGE_2, getContext().getString(applicants));
        viewPagerSettings.putString(Const.UID, myUid);
        viewPagerSettings.putString(Const.OTHER_UID, myUid);
        viewPagerSettings.putString(Const.PARAM_HANGOUT_KEY, mKey);
        viewPagerSettings.putString(Const.PARAM_HANGOUT_HOST_ID, mHostId);
        viewPagerSettings.putString(Const.PARAM_PAGER_TYPE, Const.PARAM_HANGOUT_PEOPLE);
        intent.putExtra(Const.PARAM_BUNDLE, viewPagerSettings);
        startActivity(intent);
    }

    // method to animate these views when needed
    private void animateViews()
    {
        setAnimation(titleCV, 0);
        setAnimation(tagsCV, 1);
        setAnimation(languageCV, 2);
        setAnimation(descriptionCV, 3);
        setAnimation(hostCV, 4);
        setAnimation(membersCV, 5);
    }

    // general method to animate a view
    private void setAnimation(View viewToAnimate, int position)
    {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > -1)
        {
            viewToAnimate.animate().cancel();
            viewToAnimate.setTranslationY(100);
            viewToAnimate.setAlpha(0);
            viewToAnimate.animate().alpha(1.0f).translationY(0).setDuration(350).setStartDelay(position * 31);
        }
    }

}
