package nurunabiyev.android.traddictive;

import com.stephentuso.welcome.BasicPage;
import com.stephentuso.welcome.ParallaxPage;
import com.stephentuso.welcome.WelcomeActivity;
import com.stephentuso.welcome.WelcomeConfiguration;

/**
 * Welcomes activity
 */

public class WelcomeTraddictive extends WelcomeActivity
{
    @Override
    protected WelcomeConfiguration configuration()
    {
        return new WelcomeConfiguration.Builder(this)

                //.defaultBackgroundColor(R.color.red_background)

                // traddictive
                .page(new BasicPage(R.drawable.rsz_white_logo_transparent_background,
                        "Welcome to Traddictive",
                        "A Social App for Bilkenters from Bilkenters")
                        .background(R.color.red_background)
                        .parallax(true)
                )

                // hangouts
                .page(new ParallaxPage(R.layout.welcome_hangouts,
                        "Hangouts",
                        "Create Hangout Groups for massive parties or just to celebrate your friend's birthday.")
                        .background(R.color.blue_background)
                )

                // brumors
                .page(new ParallaxPage(R.layout.welcome_brumors,
                        "Brumors",
                        "Want to spread a rumor? You can do it either anonymously or not")
                        .background(R.color.welcome_brumors_background)
                )


                // buykent
                .page(new ParallaxPage(R.layout.welcome_buykent,
                        "BuyKent",
                        "Sell, search, and buy items in one place!")
                        .background(R.color.orange_background)
                )

                // places
                .page(new ParallaxPage(R.layout.welcome_places,
                        "Places",
                        "Find any buildings, dormitories, or cafes and shops. You can also rate them and find the one that suits only you!")
                        .background(R.color.purple_background)
                )

                .swipeToDismiss(true)
                //.exitAnimation(android.R.anim.fade_out)
                .canSkip(false)
                .build();


    }
}
