package nurunabiyev.android.traddictive;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import nurunabiyev.android.traddictive.create.CreateAskActivity;
import nurunabiyev.android.traddictive.create.CreateBrumorActivity;
import nurunabiyev.android.traddictive.create.CreateBuykentActivity;
import nurunabiyev.android.traddictive.create.CreateHangoutActivity;
import nurunabiyev.android.traddictive.create.UpdatePhotoActivity;
import nurunabiyev.android.traddictive.feed.AskBilkentFragment;
import nurunabiyev.android.traddictive.feed.FeedListFragment;
import nurunabiyev.android.traddictive.feed.TabbedActivity;
import nurunabiyev.android.traddictive.feed.TabbedFragment;
import nurunabiyev.android.traddictive.objects.User;
import nurunabiyev.android.traddictive.profile.LoginActivity;
import nurunabiyev.android.traddictive.profile.NotificationActivity;
import nurunabiyev.android.traddictive.profile.ProfileActivity;

import static nurunabiyev.android.traddictive.Const.PREFS_NAME;

public class MainFeedActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener
{
    private static final String TAG = "mainactivity";

    // fragment
    private final int FRAGMENT_MAIN = 1;
    //private final int FRAGMENT_BRUMORS = 2;
    private final int FRAGMENT_ASK = 3;
    private final int FRAGMENT_HANGOUTS = 4;
    private final int FRAGMENT_BUYKENT = 5;
    private final int FRAGMENT_PLACES = 6;
    private int fragmentSelected = FRAGMENT_MAIN;
    private SharedPreferences.Editor mEditor;
    private SharedPreferences prefs;
    private FragmentManager manager;
    @BindView(R.id.toolbar_icon_iv) ImageView toolbarIconIV;
    @BindView(R.id.filter_sort_b) ImageView filterSortB;
    private boolean showMainFeed;

    // view
    private FloatingActionMenu fam;
    private DrawerLayout drawer;
    private TextView navFullNameTV;
    private CircleImageView navProfileCIV;
    private ImageView navSearchIV, navNotificationIV;
    private Toolbar toolbar;
    private AppBarLayout mAppBarLayout;

    // user info
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private DatabaseReference rootRef;
    private StorageReference mStorageRef;
    private String fullname;
    private String uid;
    private User mUser;
    private FirebaseUser mFirebaseUser;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        rootRef = FirebaseDatabase.getInstance().getReference();
        mStorageRef = FirebaseStorage.getInstance().getReference();
        setContentView(R.layout.main_all_feed);
        showMainFeed = true;

        ButterKnife.bind(this);

        manager = getSupportFragmentManager();
        prefs = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);

        initializeDrawerAndToolbar();
        initializeUser();
        initializeFABs();
    }


    private void initializeFABs()
    {
        // menu
        fam = (FloatingActionMenu) findViewById(R.id.float_menu);
        fam.setClosedOnTouchOutside(true);

        // fabs
        FloatingActionButton photoFab = (FloatingActionButton) findViewById(R.id.fab_item_photo);
        FloatingActionButton brumorsFab = (FloatingActionButton) findViewById(R.id.fab_item_brumor);
        FloatingActionButton askBilkentFab = (FloatingActionButton) findViewById(R.id.fab_item_ask_bilkent);
        FloatingActionButton hangoutsFab = (FloatingActionButton) findViewById(R.id.fab_item_hangouts);
        FloatingActionButton buykentFab = (FloatingActionButton) findViewById(R.id.fab_item_buykent);

        // click listeners
        photoFab.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                fam.close(false);
                Intent intent = new Intent(MainFeedActivity.this, UpdatePhotoActivity.class);
                intent.putExtra(Const.UID, uid);
                startActivity(intent);

            }
        });
        brumorsFab.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                fam.close(false);
                Intent intent = new Intent(MainFeedActivity.this, CreateBrumorActivity.class);
                intent.putExtra(Const.UID, uid);
                startActivity(intent);
            }
        });
        askBilkentFab.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                fam.close(false);
                Intent intent = new Intent(MainFeedActivity.this, CreateAskActivity.class);
                intent.putExtra(Const.UID, uid);
                startActivity(intent);
            }
        });
        hangoutsFab.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                fam.close(false);
                Intent intent = new Intent(MainFeedActivity.this, CreateHangoutActivity.class);
                intent.putExtra(Const.UID, uid);
                startActivity(intent);
            }
        });
        buykentFab.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                fam.close(false);
                Intent intent = new Intent(MainFeedActivity.this, CreateBuykentActivity.class);
                intent.putExtra(Const.UID, uid);
                startActivity(intent);
            }
        });


        // setting images on fabs
        Bitmap originalIcon;    // converted from drawable to bitmap
        Bitmap resizedIcon;     // resized with good quality
        float displayDensity = getResources().getDisplayMetrics().density;
        int bitmapQuality = 50; // default
        if (displayDensity <= 2)
            bitmapQuality = 52;
        else
            bitmapQuality = 72;

        originalIcon = BitmapFactory.decodeResource(this.getResources(), R.drawable.ic_image);
        resizedIcon = getResizedBitmap(originalIcon, bitmapQuality, bitmapQuality);
        photoFab.setImageBitmap(resizedIcon);

        originalIcon = BitmapFactory.decodeResource(this.getResources(), R.drawable.ic_brumors);
        resizedIcon = getResizedBitmap(originalIcon, bitmapQuality, bitmapQuality);
        brumorsFab.setImageBitmap(resizedIcon);

        originalIcon = BitmapFactory.decodeResource(this.getResources(), R.drawable.ic_askbilkent);
        resizedIcon = getResizedBitmap(originalIcon, bitmapQuality, bitmapQuality);
        askBilkentFab.setImageBitmap(resizedIcon);

        originalIcon = BitmapFactory.decodeResource(this.getResources(), R.drawable.ic_hangouts);
        resizedIcon = getResizedBitmap(originalIcon, bitmapQuality, bitmapQuality);
        hangoutsFab.setImageBitmap(resizedIcon);

        originalIcon = BitmapFactory.decodeResource(this.getResources(), R.drawable.ic_buykent);
        resizedIcon = getResizedBitmap(originalIcon, bitmapQuality, bitmapQuality);
        buykentFab.setImageBitmap(resizedIcon);


    }

    private void initializeDrawerAndToolbar()
    {
        // initializing toolbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        mAppBarLayout = (AppBarLayout) findViewById(R.id.app_bar_layout);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.main_feed_t));

        // initializing drawer
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        // initializing navigation view
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        // init. header and its views
        View header = navigationView.getHeaderView(0);
        navFullNameTV = (TextView) header.findViewById(R.id.nav_full_name);
        navProfileCIV = (CircleImageView) header.findViewById(R.id.nav_profile_image);

        // search and notification
        navSearchIV = (ImageView) header.findViewById(R.id.nav_search_iv);
        navNotificationIV = (ImageView) header.findViewById(R.id.nav_notification_iv);

        navSearchIV.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                Intent searchIntent = new Intent(MainFeedActivity.this, TabbedActivity.class);
                Bundle viewPagerSettings = new Bundle();
                viewPagerSettings.putInt(Const.PARAM_PAGE_COUNT, 3);
                viewPagerSettings.putString(Const.PARAM_PAGE_1, getResources().getString(R.string.people));
                viewPagerSettings.putString(Const.PARAM_PAGE_2, getResources().getString(R.string.buykent_t));
                viewPagerSettings.putString(Const.PARAM_PAGE_3, getResources().getString(R.string.hangouts_t));
                viewPagerSettings.putString(Const.UID, uid);
                viewPagerSettings.putString(Const.OTHER_UID, uid);
                viewPagerSettings.putString(Const.PARAM_PAGER_TYPE, Const.PARAM_SEARCH);
                viewPagerSettings.putString(Const.PARAM_SEARCH_TYPE, Const.PEOPLE);   // which page to focus on
                searchIntent.putExtra(Const.PARAM_BUNDLE, viewPagerSettings);
                startActivity(searchIntent);

            }
        });
        navNotificationIV.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(MainFeedActivity.this, NotificationActivity.class);
                intent.putExtra(Const.UID, uid);
                startActivity(intent);
                //drawer.closeDrawer(GravityCompat.START);
            }
        });

        navProfileCIV.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(MainFeedActivity.this, ProfileActivity.class);
                intent.putExtra(Const.FULL_NAME, fullname);
                intent.putExtra(Const.UID, uid);        // in navDrawer
                intent.putExtra(Const.OTHER_UID, uid);  // both my ids
                startActivity(intent);
                //drawer.closeDrawer(GravityCompat.START);
            }
        });
    }

    private void initializeUser()
    {
        mAuth = FirebaseAuth.getInstance();

        mAuthListener = new FirebaseAuth.AuthStateListener()
        {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth)
            {
                mFirebaseUser = firebaseAuth.getCurrentUser();
                if (mFirebaseUser == null)
                {
                    Intent loginIntent = new Intent(MainFeedActivity.this, LoginActivity.class);
                    startActivity(loginIntent);
                } else if (!mFirebaseUser.isEmailVerified())
                {
                    Toast.makeText(getBaseContext(), R.string.you_have_to_verify, Toast.LENGTH_LONG).show();
                    Intent loginIntent = new Intent(MainFeedActivity.this, LoginActivity.class);
                    startActivity(loginIntent);
                } else
                {
                    ConnectivityManager conMgr = (ConnectivityManager) getApplication().getSystemService(Context.CONNECTIVITY_SERVICE);
                    {
                        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                        if (netInfo != null)
                        {
                            updateUi();
                        } else
                        {
                            Toast.makeText(MainFeedActivity.this, getString(R.string.error_internet), Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    }
                }
            }
        };
    }

    private void updateUi()
    {
        // showing progress dialog until you get all data from firebase database
        final ProgressDialog pd = ProgressDialog
                .show(this, null, getResources().getString(R.string.creating_addictive_content), true);

        final DatabaseReference usersRef = rootRef.child(Const.USERS);

        usersRef.child(mFirebaseUser.getUid()).addListenerForSingleValueEvent(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                mUser = dataSnapshot.getValue(User.class);
                fullname = mUser.getFullName();
                uid = mUser.getUserId();
                navFullNameTV.setText(fullname);

                if (mUser.getProfilePhotoId() != null)
                {
                    StorageReference storageReference = mStorageRef.child(Const.USERS).child(uid).child(Const.PHOTOS_LISTS)
                            .child(Const.PHOTO_PREVIEW + mUser.getProfilePhotoId());

                    Glide.with(MainFeedActivity.this)
                            .using(new FirebaseImageLoader())
                            .load(storageReference)
                            .into(navProfileCIV);
                } else
                    navProfileCIV.setImageDrawable(getResources().getDrawable(R.drawable.default_avatar));

                // notification icon
                if (mUser.getNotificationManager().hasNewNotification())
                    navNotificationIV.setImageDrawable(getResources().getDrawable(R.drawable.ic_notifications_new));
                else
                    navNotificationIV.setImageDrawable(getResources().getDrawable(R.drawable.ic_notifications));

                pd.dismiss();
            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {
                pd.dismiss();
            }
        });

        // showing main feed
        if(!showMainFeed)
            return;

        FeedListFragment feedListFragment = FeedListFragment.newInstance(mFirebaseUser.getUid(),
                Const.FEED_BRUMORS, mFirebaseUser.getUid(), null);
        manager.beginTransaction()
                .replace(R.id.content_feed_cl, feedListFragment)
                .commit();

//        toolbarIconIV.setImageDrawable(getResources().getDrawable(R.drawable.ic_anon));
        toolbarIconIV.setVisibility(View.GONE);
//        toolbarIconIV.setOnClickListener(new View.OnClickListener()
//        {
//            @Override
//            public void onClick(View v)
//            {
//                Intent intent = new Intent(MainFeedActivity.this, StoriesActivity.class);
//                intent.putExtra(Const.UID, mFirebaseUser.getUid());
//                startActivity(intent);
//                overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
//            }
//        });

    }

    @Override
    protected void onStart()
    {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        showMainFeed = false;
    }

    @Override
    protected void onStop()
    {
        super.onStop();
        if (mAuthListener != null)
            mAuth.removeAuthStateListener(mAuthListener);
        showMainFeed = false;
    }

    // Closing drawer or exiting
    @Override
    public void onBackPressed()
    {
        if (drawer.isDrawerOpen(GravityCompat.START))
            drawer.closeDrawer(GravityCompat.START);
        else
            super.onBackPressed();
    }


    // selecting from navigation drawer
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item)
    {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        boolean noToolbarShadow = false;

        switch (id)
        {
            case R.id.nav_main_feed:
                filterSortB.setVisibility(View.VISIBLE);
                fragmentSelected = FRAGMENT_MAIN;

                noToolbarShadow = false;

                toolbarIconIV.setVisibility(View.GONE);

//                toolbarIconIV.setImageDrawable(getResources().getDrawable(R.drawable.ic_anon));
//                toolbarIconIV.setOnClickListener(new View.OnClickListener()
//                {
//                    @Override
//                    public void onClick(View v)
//                    {
//                        Intent intent = new Intent(MainFeedActivity.this, StoriesActivity.class);
//                        intent.putExtra(Const.UID, uid);
//                        startActivity(intent);
//                        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
//                    }
//                });

                FeedListFragment feedListFragment = FeedListFragment.newInstance(uid, Const.FEED_BRUMORS, uid, null);
                manager.beginTransaction()
                        .replace(R.id.content_feed_cl, feedListFragment)
                        .commit();

                toolbar.setTitle(getString(R.string.main_feed_t));
                fam.showMenu(true);
                break;


//            case R.id.nav_brumors:
//                filterSortB.setVisibility(View.VISIBLE);
//                fragmentSelected = FRAGMENT_BRUMORS;
//
//                toolbar.setTitle(getString(R.string.brumors_t));
//                noToolbarShadow = false;
//                toolbarIconIV.setImageDrawable(null);
//                toolbarIconIV.setVisibility(View.GONE);
//
//                FeedListFragment feedListFragment = FeedListFragment.newInstance(uid, Const.FEED_BRUMORS, uid, null);
//                manager.beginTransaction()
//                        .replace(R.id.content_feed_cl, feedListFragment)
//                        .commit();
//
//                fam.showMenu(true);
//                break;


            case R.id.nav_ask_bilkent:
                filterSortB.setVisibility(View.GONE);
                fragmentSelected = FRAGMENT_ASK;

                toolbar.setTitle(getString(R.string.ask_bilkent_t));
                noToolbarShadow = false;
                toolbarIconIV.setImageDrawable(null);
                toolbarIconIV.setVisibility(View.GONE);

                AskBilkentFragment askBilkentFragment = AskBilkentFragment.newInstance();
                manager.beginTransaction()
                        .replace(R.id.content_feed_cl, askBilkentFragment)
                        .commit();
                fam.showMenu(true);
                break;
            case R.id.nav_hangouts:
                filterSortB.setVisibility(View.VISIBLE);
                fragmentSelected = FRAGMENT_HANGOUTS;

                noToolbarShadow = true;
                toolbar.setTitle(getString(R.string.hangouts_t));
                toolbarIconIV.setImageDrawable(getResources().getDrawable(R.drawable.ic_search));
                toolbarIconIV.setVisibility(View.VISIBLE);

                // search
                toolbarIconIV.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        Intent searchIntent = new Intent(MainFeedActivity.this, TabbedActivity.class);
                        Bundle viewPagerSettings = new Bundle();
                        viewPagerSettings.putInt(Const.PARAM_PAGE_COUNT, 3);
                        viewPagerSettings.putString(Const.PARAM_PAGE_1, getResources().getString(R.string.people));
                        viewPagerSettings.putString(Const.PARAM_PAGE_2, getResources().getString(R.string.buykent_t));
                        viewPagerSettings.putString(Const.PARAM_PAGE_3, getResources().getString(R.string.hangouts_t));
                        viewPagerSettings.putString(Const.UID, uid);
                        viewPagerSettings.putString(Const.OTHER_UID, uid);
                        viewPagerSettings.putString(Const.PARAM_PAGER_TYPE, Const.PARAM_SEARCH);
                        viewPagerSettings.putString(Const.PARAM_SEARCH_TYPE, Const.HANGOUTS);   // which page to focus on
                        searchIntent.putExtra(Const.PARAM_BUNDLE, viewPagerSettings);
                        startActivity(searchIntent);
                    }
                });


                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                    ViewCompat.setElevation(mAppBarLayout, 0);

                Bundle hangoutSettings = new Bundle();
                hangoutSettings.putString(Const.PARAM_PAGE_1, getString(R.string.hangouts_popular));
                hangoutSettings.putString(Const.PARAM_PAGE_2, getString(R.string.hangouts_all));
                hangoutSettings.putString(Const.PARAM_PAGE_3, getString(R.string.hangouts_joined));
                hangoutSettings.putString(Const.PARAM_PAGER_TYPE, Const.PAGER_HANGOUT);

                TabbedFragment tabbedFragment = TabbedFragment.newInstance(uid, hangoutSettings);
                manager.beginTransaction()
                        .replace(R.id.content_feed_cl, tabbedFragment)
                        .commit();

                fam.showMenu(true);
                break;

            case R.id.nav_buykent:
                filterSortB.setVisibility(View.GONE);
                fragmentSelected = FRAGMENT_BUYKENT;

                toolbar.setTitle(getString(R.string.buykent_t));
                noToolbarShadow = true;
                toolbarIconIV.setVisibility(View.VISIBLE);
                toolbarIconIV.setImageDrawable(getResources().getDrawable(R.drawable.ic_search));

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                    ViewCompat.setElevation(mAppBarLayout, 0);

                toolbarIconIV.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        Intent searchIntent = new Intent(MainFeedActivity.this, TabbedActivity.class);
                        Bundle viewPagerSettings = new Bundle();
                        viewPagerSettings.putInt(Const.PARAM_PAGE_COUNT, 3);
                        viewPagerSettings.putString(Const.PARAM_PAGE_1, getResources().getString(R.string.people));
                        viewPagerSettings.putString(Const.PARAM_PAGE_2, getResources().getString(R.string.buykent_t));
                        viewPagerSettings.putString(Const.PARAM_PAGE_3, getResources().getString(R.string.hangouts_t));
                        viewPagerSettings.putString(Const.UID, uid);
                        viewPagerSettings.putString(Const.OTHER_UID, uid);
                        viewPagerSettings.putString(Const.PARAM_PAGER_TYPE, Const.PARAM_SEARCH);
                        viewPagerSettings.putString(Const.PARAM_SEARCH_TYPE, Const.BUYKENT);   // which page to focus on
                        searchIntent.putExtra(Const.PARAM_BUNDLE, viewPagerSettings);
                        startActivity(searchIntent);
                    }
                });

                Bundle buykentSettings = new Bundle();
                buykentSettings.putString(Const.PARAM_PAGE_1, getString(R.string.buykent_education));
                buykentSettings.putString(Const.PARAM_PAGE_2, getString(R.string.buykent_tech_vehicle));
                buykentSettings.putString(Const.PARAM_PAGE_3, getString(R.string.buykent_other));
                buykentSettings.putString(Const.PARAM_PAGER_TYPE, Const.PAGER_BUYKENT);

                TabbedFragment tabbedFragment1 = TabbedFragment.newInstance(uid, buykentSettings);
                manager.beginTransaction()
                        .replace(R.id.content_feed_cl, tabbedFragment1)
                        .commit();

                fam.showMenu(true);
                break;
            case R.id.nav_places:
                filterSortB.setVisibility(View.VISIBLE);
                fragmentSelected = FRAGMENT_PLACES;

                toolbar.setTitle(getString(R.string.places_t));
                noToolbarShadow = true;
                toolbarIconIV.setVisibility(View.GONE);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                    ViewCompat.setElevation(mAppBarLayout, 0);

                Bundle placesSettings = new Bundle();
                placesSettings.putString(Const.PARAM_PAGE_1, getString(R.string.shops_cafes));
                placesSettings.putString(Const.PARAM_PAGE_2, getString(R.string.dormitories));
                placesSettings.putString(Const.PARAM_PAGE_3, getString(R.string.main_buildings));
                placesSettings.putString(Const.PARAM_PAGER_TYPE, Const.PAGER_PLACES);

                TabbedFragment tabbedFragment2 = TabbedFragment.newInstance(uid, placesSettings);
                manager.beginTransaction()
                        .replace(R.id.content_feed_cl, tabbedFragment2)
                        .commit();

                fam.showMenu(true);
                break;

            // More. Opens a new activity
            case R.id.nav_more:
                noToolbarShadow = true;

                Intent moreIntent = new Intent(getBaseContext(), MoreActivity.class);
                moreIntent.putExtra(Const.UID, uid);
                startActivity(moreIntent);
                break;
        }

        // setting shadow if needed
        if (!noToolbarShadow && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            ViewCompat.setElevation(mAppBarLayout, 10);

        // closing after selecting item
        drawer.closeDrawer(GravityCompat.START);

        return true;
    }

    // Needed to resize image for fab menu items
    private static Bitmap getResizedBitmap(Bitmap bm, int newHeight, int newWidth)
    {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);
        // RECREATE THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height,
                matrix, false);
        return resizedBitmap;
    }


    public void openFilterSort(View view)
    {
        if (fragmentSelected == FRAGMENT_MAIN)
            filterMain();
//        else if (fragmentSelected == FRAGMENT_BRUMORS)
//            filterBrumors();
        else if (fragmentSelected == FRAGMENT_ASK)
            filterAsk();
        else if (fragmentSelected == FRAGMENT_HANGOUTS)
            filterHangouts();
        else if (fragmentSelected == FRAGMENT_BUYKENT)
            filterBuykent();
        else if (fragmentSelected == FRAGMENT_PLACES)   // done 90%
            filterPlaces();
    }

    private void filterMain()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainFeedActivity.this);
        builder.setTitle(getString(R.string.filter_main));
        builder.setMessage(getString(R.string.announcements_english_brumors));

        LayoutInflater inflater = this.getLayoutInflater();
        View filterView = inflater.inflate(R.layout.dialog_filter_main, null);

        builder.setView(filterView);

        final CheckBox friendsPostsCB = (CheckBox) filterView.findViewById(R.id.dialog_friends_cb);
        final CheckBox brumorTurkishCB = (CheckBox) filterView.findViewById(R.id.dialog_turkish_cb);
        final CheckBox brumorEnglishCB = (CheckBox) filterView.findViewById(R.id.dialog_english_cb);

        // setting checked from shared prefs
        SharedPreferences prefs = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        friendsPostsCB.setChecked(prefs.getBoolean(Const.SHARED_FRIENDS_POSTS, true));
        brumorTurkishCB.setChecked(prefs.getBoolean(Const.SHARED_BRUMORS_TURKISH, true));
        brumorEnglishCB.setChecked(prefs.getBoolean(Const.SHARED_BRUMORS_ENGLISH, true));

        // saving shared prefs
        mEditor = getSharedPreferences(PREFS_NAME, MODE_PRIVATE).edit();

        friendsPostsCB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                mEditor.putBoolean(Const.SHARED_FRIENDS_POSTS, isChecked);
            }
        });

        brumorTurkishCB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                mEditor.putBoolean(Const.SHARED_BRUMORS_TURKISH, isChecked);
            }
        });

        brumorEnglishCB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                mEditor.putBoolean(Const.SHARED_BRUMORS_ENGLISH, isChecked);
            }
        });


        builder.setPositiveButton(getString(R.string.apply), new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int id)
            {
                mEditor.apply();

                FeedListFragment feedListFragment = FeedListFragment.newInstance(uid, Const.FEED_BRUMORS, uid, null);
                manager.beginTransaction()
                        .replace(R.id.content_feed_cl, feedListFragment)
                        .commit();
            }
        });

        builder.show();
    }

    private void filterAsk()
    {
        // maybe in future versions
        filterSortB.setVisibility(View.GONE);
    }

    private void filterHangouts()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainFeedActivity.this);
        builder.setTitle(getString(R.string.sort_hangouts));

        LayoutInflater inflater = this.getLayoutInflater();
        View sortView = inflater.inflate(R.layout.dialog_sort, null);

        final RadioButton sort1 = (RadioButton) sortView.findViewById(R.id.sort1_rb);
        sort1.setText(getString(R.string.sort_time_created));
        final RadioButton sort2 = (RadioButton) sortView.findViewById(R.id.sort2_rb);
        sort2.setText(getString(R.string.sort_people));

        // setting checked from shared prefs
        sort1.setChecked(prefs.getBoolean(Const.SHARED_HANGOUT_TIME, true));
        sort2.setChecked(prefs.getBoolean(Const.SHARED_HANGOUT_PEOPLE, false));

        mEditor = getSharedPreferences(PREFS_NAME, MODE_PRIVATE).edit();
        sort1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                mEditor.putBoolean(Const.SHARED_HANGOUT_TIME, isChecked);
            }
        });
        sort2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                mEditor.putBoolean(Const.SHARED_HANGOUT_PEOPLE, isChecked);
            }
        });

        builder.setView(sortView);

        builder.setPositiveButton(getString(R.string.apply), new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int id)
            {

                mEditor.apply();

                Bundle hangoutSettings = new Bundle();
                hangoutSettings.putString(Const.PARAM_PAGE_1, getString(R.string.hangouts_popular));
                hangoutSettings.putString(Const.PARAM_PAGE_2, getString(R.string.hangouts_all));
                hangoutSettings.putString(Const.PARAM_PAGE_3, getString(R.string.hangouts_joined));
                hangoutSettings.putString(Const.PARAM_PAGER_TYPE, Const.PAGER_HANGOUT);

                TabbedFragment tabbedFragment = TabbedFragment.newInstance(uid, hangoutSettings);
                manager.beginTransaction()
                        .replace(R.id.content_feed_cl, tabbedFragment)
                        .commit();

            }
        });

        builder.show();
    }

    private void filterBuykent()
    {
        // maybe in future versions
        filterSortB.setVisibility(View.GONE);
    }

    private void filterPlaces()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainFeedActivity.this);
        builder.setTitle(getString(R.string.sort_places));

        LayoutInflater inflater = this.getLayoutInflater();
        View sortView = inflater.inflate(R.layout.dialog_sort, null);

        final RadioButton sort1 = (RadioButton) sortView.findViewById(R.id.sort1_rb);
        sort1.setText(getString(R.string.sort_alphabet));
        final RadioButton sort2 = (RadioButton) sortView.findViewById(R.id.sort2_rb);
        sort2.setText(getString(R.string.sort_rating));

        // setting checked from shared prefs
        sort1.setChecked(prefs.getBoolean(Const.SHARED_PLACE_ALPHABET, true));
        sort2.setChecked(prefs.getBoolean(Const.SHARED_PLACE_RATING, false));

        mEditor = getSharedPreferences(PREFS_NAME, MODE_PRIVATE).edit();
        sort1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                mEditor.putBoolean(Const.SHARED_PLACE_ALPHABET, isChecked);
            }
        });
        sort2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                mEditor.putBoolean(Const.SHARED_PLACE_RATING, isChecked);
            }
        });

        builder.setView(sortView);

        builder.setPositiveButton(getString(R.string.apply), new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int id)
            {

                mEditor.apply();

                Bundle placesSettings = new Bundle();
                placesSettings.putString(Const.PARAM_PAGE_1, getString(R.string.shops_cafes));
                placesSettings.putString(Const.PARAM_PAGE_2, getString(R.string.dormitories));
                placesSettings.putString(Const.PARAM_PAGE_3, getString(R.string.main_buildings));
                placesSettings.putString(Const.PARAM_PAGER_TYPE, Const.PAGER_PLACES);

                TabbedFragment tabbedFragment2 = TabbedFragment.newInstance(uid, placesSettings);
                manager.beginTransaction()
                        .replace(R.id.content_feed_cl, tabbedFragment2)
                        .commit();

            }
        });

        builder.show();
    }

}
