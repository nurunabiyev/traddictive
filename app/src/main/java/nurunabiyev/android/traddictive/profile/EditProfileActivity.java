package nurunabiyev.android.traddictive.profile;

import android.app.DatePickerDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;

import java.util.UUID;

import fr.ganfra.materialspinner.MaterialSpinner;
import nurunabiyev.android.traddictive.Const;
import nurunabiyev.android.traddictive.R;
import nurunabiyev.android.traddictive.objects.Post;
import nurunabiyev.android.traddictive.objects.User;

public class EditProfileActivity extends AppCompatActivity
{

    public static final String TAG = "editprofile";
    // view
    private MaterialSpinner relationshipS;
    private MaterialSpinner departmentS;
    private EditText hometownET;
    private Button applyB;
    private Button birthdayET;
    // adapters
    private ArrayAdapter<String> relAdapter;
    private ArrayAdapter<String> depAdapter;
    // firebase
    private DatabaseReference mReference;

    private String birthday = "Choose Birthday";
    private final int MIN_HOMETOWN_LENGTH = 10;
    private final int MAX_HOMETOWN_LENGTH = 40;
    int startYear = 1998;
    int startMonth = 2;
    int startDay = 21;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        mReference = FirebaseDatabase.getInstance().getReference();
        setContentView(R.layout.activity_edit_profile);

        initializeToolbar();
        initializeView();
    }

    private void initializeToolbar()
    {
        Toolbar toolbar = (Toolbar) findViewById(R.id.edit_profile_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // change the color of gradient
        if (android.os.Build.VERSION.SDK_INT >= 21)
        {
            Window window = EditProfileActivity.this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.editorColorPrimaryDark));
        }
    }

    private void initializeView()
    {
        final String uid = getIntent().getStringExtra(Const.UID);
        final DatabaseReference mUserRef = mReference.child(Const.USERS).child(uid);

        // initializing
        hometownET = (EditText) findViewById(R.id.hometown_et);
        departmentS = (MaterialSpinner) findViewById(R.id.department_s);
        relationshipS = (MaterialSpinner) findViewById(R.id.relationship_s);
        applyB = (Button) findViewById(R.id.apply_b);
        birthdayET = (Button) findViewById(R.id.birthday_et);

        // opening calendar dialog
        final DatePickerDialog datePickerDialog = new DatePickerDialog(
                EditProfileActivity.this, datePickerListener, startYear, startMonth, startDay);
        birthdayET.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                datePickerDialog.show();
            }
        });

        // setting relationship spinner
        String[] relationshipArray = getResources().getStringArray(R.array.relationship_array);
        relAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, relationshipArray);
        relAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        relationshipS.setAdapter(relAdapter);

        // setting department spinner
        String[] departmentArray = getResources().getStringArray(R.array.departments_array);
        depAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, departmentArray);
        depAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        departmentS.setAdapter(depAdapter);

        // setting views to user's previous settings
        setPreviousSettings(mUserRef);

        // sending data to firebase
        applyB.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                if(isValid())
                {
                    // creating post about relationship status
                    mUserRef.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {

                            User user = dataSnapshot.getValue(User.class);

                            if(!relationshipS.getSelectedItem().toString().equals(Const.DEFAULT_RELATIONSHIP_ST)
                                    && !relationshipS.getSelectedItem().toString().equals(user.getRelationshipStatus()))
                            {

                                String key = mUserRef.push().getKey();

                                Post newPost = new Post();
                                newPost.setType(Const.POST_TYPE_CHANGE_RELATION_STATUS);
                                newPost.setPostId(UUID.randomUUID().toString());
                                newPost.setUploaderId(uid);
                                newPost.setTimeSent(ServerValue.TIMESTAMP);
                                newPost.setForFriendsOrBilkent(true);
                                newPost.setUploaderName(user.getFullName());
                                newPost.setAnonymous(false);
                                newPost.setPostKey(key);

                                String newPostWholeText = "" + getString(R.string.has_updated_relation_status) + "\n\n" + relationshipS.getSelectedItem().toString();
                                newPost.setText(newPostWholeText);

                                // sending post
                                mUserRef.child(Const.POSTS_LISTS).child(key)
                                        .setValue(newPost);
                            }

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });

                    mUserRef.child(Const.HOMETOWN).setValue(hometownET.getText().toString());
                    mUserRef.child(Const.DEPARTMENT).setValue(departmentS.getSelectedItem().toString());
                    mUserRef.child(Const.RELATIONSHIP).setValue(relationshipS.getSelectedItem().toString());
                    mUserRef.child(Const.BIRTHDAY).setValue(birthday);

                    finish();
                }
                else
                {
                    Toast.makeText(EditProfileActivity.this, getString(R.string.invalid_input), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void setPreviousSettings(DatabaseReference userRef)
    {
        userRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);

                if(!user.getRelationshipStatus().equals(Const.DEFAULT_RELATIONSHIP_ST)
                        || !user.getDepartment().equals(Const.DEFAULT_DEPARTMENT)
                        || !user.getHometown().equals(Const.DEFAULT_HOMETOWN))
                {
                    relationshipS.setSelection(relAdapter.getPosition(dataSnapshot.child(Const.RELATIONSHIP).getValue(String.class))+1);
                    departmentS.setSelection(depAdapter.getPosition(dataSnapshot.child(Const.DEPARTMENT).getValue(String.class))+1);
                    hometownET.setText(dataSnapshot.child(Const.HOMETOWN).getValue(String.class));
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    // Clicking ok on calendar picker
    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener()
    {
        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear, int selectedMonth, int selectedDay)
        {
            String year1 = String.valueOf(selectedYear);
            String month1 = String.valueOf(selectedMonth + 1);
            String day1 = String.valueOf(selectedDay);

            switch (month1)
            {
                case "1":
                    month1 = getString(R.string.month_January);
                    break;
                case "2":
                    month1 = getString(R.string.month_february);
                    break;
                case "3":
                    month1 = getString(R.string.month_March);
                    break;
                case "4":
                    month1 = getString(R.string.month_April);
                    break;
                case "5":
                    month1 = getString(R.string.month_May);
                    break;
                case "6":
                    month1 = getString(R.string.month_June);
                    break;
                case "7":
                    month1 = getString(R.string.month_July);
                    break;
                case "8":
                    month1 = getString(R.string.month_August);
                    break;
                case "9":
                    month1 = getString(R.string.month_September);
                    break;
                case "10":
                    month1 = getString(R.string.month_October);
                    break;
                case "11":
                    month1 = getString(R.string.month_November);
                    break;
                case "12":
                    month1 = getString(R.string.month_December);
                    break;
            }
            birthday = day1 + " " + month1 + ", " + year1;
            birthdayET.setText(birthday);
        }
    };

    // finish() to profile
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();

        if (id == android.R.id.home)
            finish();

        return super.onOptionsItemSelected(item);
    }

    private boolean isValid()
    {
        boolean valid = true;

        if(relationshipS.getSelectedItemPosition() == 0
                || departmentS.getSelectedItemPosition() == 0)
        {
            valid = false;
            hometownET.setError(null);
        }

        if(hometownET.getText().toString().length() < MIN_HOMETOWN_LENGTH
                || hometownET.getText().toString().length() > MAX_HOMETOWN_LENGTH)
        {
            valid = false;
            hometownET.setError(getResources().getString(R.string.error_invalid_text_longshort));
        }

        if(birthday.equals("Choose Birthday"))
            valid = false;

        return valid;
    }
}
