package nurunabiyev.android.traddictive.profile;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;
import nurunabiyev.android.traddictive.Const;
import nurunabiyev.android.traddictive.MainFeedActivity;
import nurunabiyev.android.traddictive.R;
import nurunabiyev.android.traddictive.feed.PostItemActivity;
import nurunabiyev.android.traddictive.feed.PostRecycler;
import nurunabiyev.android.traddictive.feed.TabbedActivity;
import nurunabiyev.android.traddictive.objects.Notification;
import nurunabiyev.android.traddictive.objects.Post;
import nurunabiyev.android.traddictive.objects.User;


public class ProfileActivity extends AppCompatActivity
{

    private static final String TAG = "profile";
    // view
    private TextView noPhotosTV, photosNumTV, postsNumTV;
    private CardView photosCV;
    private TextView statusTV, relationshipStatusTV, departmentTV, hometownTV, ageTV;
    private TextView askStatsTV, hangoutsNumTV, buykentNumTV, friendsNumTV;
    private ImageView profilePhotoIV;
    private CardView otherInfoCV, statusCV, numbersCV;
    private AppCompatButton friendOrLimitB;
    private LinearLayout photoLL;
    @BindView(R.id.add_limits_ll)
    LinearLayout addLimitsLL;
    private LinearLayout.LayoutParams photosParams;   // photo
    private int marginDips;
    // data
    private Map<String, String> mPhotos;    // preview
    private List<Post> mPosts;
    private boolean drawPhotos;
    private boolean sendMeToHisGuests;
    private RecyclerView mProfileRV;
    // firebase
    private boolean isMyProfile;
    private DatabaseReference mRootRef;
    private StorageReference mStorageRef;
    private User mUser;     // user of the current profile
    private User mUserMe;   // will be null if current profile is me (mUser)
    private String myUid;       // id of the logged in user
    private String otherUid;    // uid of the current profile
    private DatabaseReference otherUserRef;
    private DatabaseReference myUserRef;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        mRootRef = FirebaseDatabase.getInstance().getReference();
        mStorageRef = FirebaseStorage.getInstance().getReference();
        setContentView(R.layout.activity_profile);

        ButterKnife.bind(this);

        drawPhotos = true;
        sendMeToHisGuests = true;
        initializeViews();
    }

    private void initializeViews()
    {
        photosNumTV = (TextView) findViewById(R.id.photo_num_tv);
        noPhotosTV = (TextView) findViewById(R.id.no_photos_tv);
        photosCV = (CardView) findViewById(R.id.photos_cv);
        statusTV = (TextView) findViewById(R.id.status_tv);
        relationshipStatusTV = (TextView) findViewById(R.id.relationship_tv);
        departmentTV = (TextView) findViewById(R.id.department_tv);
        ageTV = (TextView) findViewById(R.id.age_tv);
        hometownTV = (TextView) findViewById(R.id.place_tv);
        //askStatsTV = (TextView) findViewById(R.id.ask_stats_tv);
        hangoutsNumTV = (TextView) findViewById(R.id.hangouts_num_tv);
        buykentNumTV = (TextView) findViewById(R.id.buykent_num_tv);
        friendsNumTV = (TextView) findViewById(R.id.friends_num_tv);
        profilePhotoIV = (ImageView) findViewById(R.id.profile_iv);
        statusCV = (CardView) findViewById(R.id.status_cv);
        otherInfoCV = (CardView) findViewById(R.id.other_info_cv);
        numbersCV = (CardView) findViewById(R.id.numbers_cv);
        friendOrLimitB = (AppCompatButton) findViewById(R.id.add_limits_b);
        postsNumTV = (TextView) findViewById(R.id.posts_num_tv);

        // photos part
        photoLL = (LinearLayout) findViewById(R.id.photos_ll);
        photosParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        int dpValue = 4; // margin in dips
        float d = ProfileActivity.this.getResources().getDisplayMetrics().density;
        marginDips = (int) (dpValue * d); // margin in pixels

        // toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.profile_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // getting data from main activity
        String fullname = getIntent().getStringExtra(Const.FULL_NAME);
        myUid = getIntent().getStringExtra(Const.UID);
        otherUid = getIntent().getStringExtra(Const.OTHER_UID);
        getSupportActionBar().setTitle(fullname);

        mProfileRV = (RecyclerView) findViewById(R.id.profile_rv);

        mPosts = new ArrayList<>();
        displayWholeProfile(otherUid);

        animateViews();
    }

    private void displayWholeProfile(String profileUid)
    {
        mRootRef.child(Const.USERS).child(profileUid).addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                mUser = dataSnapshot.getValue(User.class);

                otherUserRef = mRootRef.child(Const.USERS).child(otherUid);
                myUserRef = mRootRef.child(Const.USERS).child(myUid);

                displayProfileInfo();

                // click listeners
                whosProfileIsThis();

                if (drawPhotos)
                {
                    mPosts.clear();
                    displayPhotos();
                    displayPosts();
                    drawPhotos = false; // so that wont draw again
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {

            }
        });
    }

    // inside displayWholeProfile
    private void displayProfileInfo()
    {
        // setting profile image
        if (mUser.getProfilePhotoId() != null)
        {
            StorageReference storageReference = mStorageRef.child(Const.USERS).child(otherUid).child(Const.PHOTOS_LISTS)
                    .child(Const.PHOTO_ORIGINAL + mUser.getProfilePhotoId());
            Glide.with(getApplicationContext())
                    .using(new FirebaseImageLoader())
                    .load(storageReference)
                    .into(profilePhotoIV);
        } else
            profilePhotoIV.setImageResource(R.drawable.default_avatar);

        // info
        statusTV.setText(mUser.getStatus());
        relationshipStatusTV.setText(mUser.getRelationshipStatus());
        departmentTV.setText(mUser.getDepartment());
        ageTV.setText(mUser.getBirthday());
        hometownTV.setText(mUser.getHometown());
        // numbers
        //askStatsTV.setText("" + mUser.getAskBilkentStats());
        hangoutsNumTV.setText("" + mUser.getHangoutsList().size());
        buykentNumTV.setText("" + mUser.getBuykentList().size());
        friendsNumTV.setText("" + mUser.getFriendsList().size());
        mPhotos = mUser.getPhotosLists();

        CardView numbersCV = (CardView) findViewById(R.id.numbers_cv);
        numbersCV.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Log.d(TAG, "profile: myuid " + myUid + " otheruid " + otherUid);

                Intent intent = new Intent(ProfileActivity.this, TabbedActivity.class);
                Bundle viewPagerSettings = new Bundle();
                viewPagerSettings.putInt(Const.PARAM_PAGE_COUNT, 3);
                viewPagerSettings.putString(Const.PARAM_PAGE_1, getResources().getString(R.string.hangouts_t));
                viewPagerSettings.putString(Const.PARAM_PAGE_2, getResources().getString(R.string.buykent_t));
                viewPagerSettings.putString(Const.PARAM_PAGE_3, getResources().getString(R.string.friends_list));
                viewPagerSettings.putString(Const.UID, myUid);
                viewPagerSettings.putString(Const.OTHER_UID, otherUid);
                viewPagerSettings.putString(Const.PARAM_PAGER_TYPE, Const.PARAM_PROFILE);
                intent.putExtra(Const.PARAM_BUNDLE, viewPagerSettings);
                startActivity(intent);
            }
        });
    }

    private void whosProfileIsThis()
    {
        // IF IT IS ME
        if (myUid.equals(otherUid))
        {
            isMyProfile = true;
            myProfile();
        }

        // if it is other user's profile
        else
        {
            isMyProfile = false;
            // getting my profile to contact the other guy
            mRootRef.child(Const.USERS).child(myUid)
                    .addValueEventListener(new ValueEventListener()
                    {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot)
                        {
                            mUserMe = dataSnapshot.getValue(User.class);
                            notMyProfile();
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError)
                        {
                            //Toast.makeText(ProfileActivity.this, "Could not get profile information", Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    });
        }
    }

    private void myProfile()
    {

        //addLimitsLL.setVisibility(View.GONE);

        // change status
        statusCV.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                displayStatusDialog();
            }
        });
        // change profile
        otherInfoCV.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent editProfileIntent = new Intent(ProfileActivity.this, EditProfileActivity.class);
                editProfileIntent.putExtra(Const.UID, mUser.getUserId());
                startActivity(editProfileIntent);
            }
        });

        // button stuff
        friendOrLimitB.setTextColor(getResources().getColor(R.color.host));
        friendOrLimitB.setText("Guests");

        // creating alert dialog
//        // get limit data and generate text todo v2 limits
//        int limitStories = mUser.getLimitAnonForFriends();
//        int limitComments = mUser.getLimitComments();
//        int limitHangouts = mUser.getLimitHangoutsHosted();
//        int limitPhotos = mUser.getLimitPhotos();
//        int limitPublicSecrets = mUser.getLimitPublicSecrets();
//        int limitQuestions = mUser.getLimitQuestions();
//        final String limitMessage = "Today you can post:\n"
//                + limitStories + " more stories,\n"
//                + limitComments + " more comments,\n"
//                + limitPhotos + " more photos,\n"
//                + limitPublicSecrets + " more BRumors\n"
//                // + limitQuestions + " more questions,\n"
//                + "And host " + limitHangouts + " Hangouts at a time.";

        friendOrLimitB.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
//                // limit textview todo v2 limits
//                final TextView limitsInfoTV = new TextView(ProfileActivity.this);
//                limitsInfoTV.setText(limitMessage);
//                // textview margins
//                float d = ProfileActivity.this.getResources().getDisplayMetrics().density;
//                int dpLeft = 24; // margin in dips
//                int marginLeft = (int) (dpLeft * d);
//                limitsInfoTV.setPadding(marginLeft, 0, 0, 0);
//
//                // create alert dialog
//                alert.setView(limitsInfoTV);
//                alert.show();
                inflateGuests();

            }
        });
    }

    // in myProfile() when clicking on guests
    private void inflateGuests()
    {
        final AlertDialog.Builder alert = new AlertDialog.Builder(ProfileActivity.this);
        alert.setTitle("Guests");
        alert.setMessage("Last three people who were here \n( ͡° ͜ʖ ͡°) ");

        LayoutInflater inflater = getLayoutInflater();
        final View sortView = inflater.inflate(R.layout.dialog_guests, null);

        // creating guests
        final TextView noGuestsTV = (TextView) sortView.findViewById(R.id.no_guests_tv);
        final LinearLayout guestsLL = sortView.findViewById(R.id.guests_ll);

        final LinearLayout g1LL = sortView.findViewById(R.id.guest_1_ll);
        final LinearLayout g2LL = sortView.findViewById(R.id.guest_2_ll);
        final LinearLayout g3LL = sortView.findViewById(R.id.guest_3_ll);
        final TextView g1NameTV = sortView.findViewById(R.id.guest_1_name_tv);
        final TextView g2NameTV = sortView.findViewById(R.id.guest_2_name_tv);
        final TextView g3NameTV = sortView.findViewById(R.id.guest_3_name_tv);
        final TextView g1DepartmentTV = sortView.findViewById(R.id.guest_1_department_tv);
        final TextView g2DepartmentTV = sortView.findViewById(R.id.guest_2_department_tv);
        final TextView g3DepartmentTV = sortView.findViewById(R.id.guest_3_department_tv);

        // downloading guests
        mRootRef.child(Const.USERS).child(otherUid)
                .child(Const.GUEST_LIST).addListenerForSingleValueEvent(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                // downloading
                String g1value = dataSnapshot.child("0").getValue(String.class);
                String g2value = dataSnapshot.child("1").getValue(String.class);
                String g3value = dataSnapshot.child("2").getValue(String.class);

                if (g1value == null && g2value == null && g3value == null)
                {
                    noGuestsTV.setVisibility(View.VISIBLE);
                    guestsLL.setVisibility(View.GONE);

                    alert.setPositiveButton("¯\\_(ツ)_/¯", new DialogInterface.OnClickListener()
                    {
                        public void onClick(DialogInterface dialog, int whichButton)
                        {
                            dialog.dismiss();
                        }
                    });
                } else
                {
                    noGuestsTV.setVisibility(View.GONE);
                    guestsLL.setVisibility(View.VISIBLE);

                    final String  g1Id, g1Name, g1Department;
                    final String  g2Id, g2Name, g2Department;
                    final String  g3Id, g3Name, g3Department;

                    g1Id = g1value.substring(0, g1value.indexOf("%"));
                    g1Name = g1value.substring(g1value.indexOf("%")+1, g1value.indexOf("$"));
                    g1Department = g1value.substring(g1value.indexOf("$")+1, g1value.length());
                    g1NameTV.setText(g1Name);
                    g1DepartmentTV.setText(g1Department);

                    g1LL.setOnClickListener(new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View view)
                        {
                            Intent intent = new Intent(ProfileActivity.this, ProfileActivity.class);
                            intent.putExtra(Const.FULL_NAME, g1Name);
                            intent.putExtra(Const.UID, myUid);        // in navDrawer
                            intent.putExtra(Const.OTHER_UID, g1Id);  // both my ids
                            startActivity(intent);
                        }
                    });

                    // setting guest 2
                    if(g2value != null)
                    {
                        g2Id = g2value.substring(0, g2value.indexOf("%"));
                        g2Name = g2value.substring(g2value.indexOf("%") +1, g2value.indexOf("$"));
                        g2Department = g2value.substring(g2value.indexOf("$")+1, g2value.length());
                        g2NameTV.setText(g2Name);
                        g2DepartmentTV.setText(g2Department);

                        g2LL.setOnClickListener(new View.OnClickListener()
                        {
                            @Override
                            public void onClick(View view)
                            {
                                Intent intent = new Intent(ProfileActivity.this, ProfileActivity.class);
                                intent.putExtra(Const.FULL_NAME, g2Name);
                                intent.putExtra(Const.UID, myUid);        // in navDrawer
                                intent.putExtra(Const.OTHER_UID, g2Id);  // both my ids
                                startActivity(intent);
                            }
                        });

                    }
                    else
                        g2LL.setVisibility(View.GONE);


                    // setting guest 3
                    if(g3value != null)
                    {
                        g3Id = g3value.substring(0, g3value.indexOf("%"));
                        g3Name = g3value.substring(g3value.indexOf("%")+1, g3value.indexOf("$"));
                        g3Department = g3value.substring(g3value.indexOf("$")+1, g3value.length());
                        g3NameTV.setText(g3Name);
                        g3DepartmentTV.setText(g3Department);

                        g3LL.setOnClickListener(new View.OnClickListener()
                        {
                            @Override
                            public void onClick(View view)
                            {
                                Intent intent = new Intent(ProfileActivity.this, ProfileActivity.class);
                                intent.putExtra(Const.FULL_NAME, g3Name);
                                intent.putExtra(Const.UID, myUid);        // in navDrawer
                                intent.putExtra(Const.OTHER_UID, g3Id);  // both my ids
                                startActivity(intent);
                            }
                        });
                    }
                    else
                        g3LL.setVisibility(View.GONE);

                }

                alert.setView(sortView);
                alert.show();
            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {

            }
        });


        //alert.setView(sortView);
        alert.setPositiveButton(getString(R.string.got_it), new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int whichButton)
            {
                dialog.dismiss();
            }
        });
        //alert.show();
    }

    // in myProfile() when clicking on status cardview
    private void displayStatusDialog()
    {
        // creating edittext
        final EditText edittext = new EditText(ProfileActivity.this);
        edittext.setText(mUser.getStatus());
        edittext.setMaxLines(4);
        int dpValue = 16; // margin in dips
        float d = ProfileActivity.this.getResources().getDisplayMetrics().density;
        int margin = (int) (dpValue * d); // margin in pixels
        edittext.setPadding(margin, 0, 0, margin);

        // creating alert dialog
        final AlertDialog.Builder alert = new AlertDialog.Builder(ProfileActivity.this);
        alert.setTitle(getString(R.string.status));
        alert.setMessage(getString(R.string.four_lines_at_most));
        alert.setView(edittext);
        alert.setPositiveButton(getString(R.string.update), new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int whichButton)
            {
                String value = edittext.getText().toString();

                // creating new post about changing status
                if (!value.equals(mUser.getStatus()))
                {
                    String key = mRootRef.push().getKey();

                    Post newPost = new Post();
                    newPost.setType(Const.POST_TYPE_CHANGE_STATUS);
                    newPost.setPostId(UUID.randomUUID().toString());
                    newPost.setUploaderId(myUid);
                    newPost.setTimeSent(ServerValue.TIMESTAMP);
                    newPost.setForFriendsOrBilkent(true);
                    newPost.setUploaderName(mUser.getFullName());
                    newPost.setAnonymous(false);
                    newPost.setPostKey(key);

                    String newPostWholeText = "" + getString(R.string.has_updated_status) + "\n\n" + value;
                    newPost.setText(newPostWholeText);

                    // sending status
                    mRootRef.child(Const.USERS).child(myUid)
                            .child(Const.STATUS).setValue(value);

                    // sending post
                    mRootRef.child(Const.USERS).child(myUid).child(Const.POSTS_LISTS).child(key)
                            .setValue(newPost);
                }

                dialog.dismiss();

                animateViews();
            }
        });

        alert.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int whichButton)
            {
                dialog.dismiss();
            }
        });

        alert.show();
    }

    private void notMyProfile()
    {
        // adding me to his guest list
        if (sendMeToHisGuests)
            mRootRef.child(Const.USERS).child(otherUid).child(Const.GUEST_LIST)
                    .child(Const.GUEST_INDEX).addListenerForSingleValueEvent(new ValueEventListener()
            {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot)
                {
                    sendMeToHisGuests = false;

                    // first time the dataSnapshot will be null
                    if (dataSnapshot.getValue(Integer.class) != null)
                    {
                        // get index
                        int index = dataSnapshot.getValue(Integer.class);

                        // put me too child at index
                        mRootRef.child(Const.USERS).child(otherUid).child(Const.GUEST_LIST).child("" + index)
                                .setValue(myUid + "%" + mUserMe.getFullName() + "$" + mUserMe.getDepartment());

                        // refresh index
                        if (index < 2)
                            mRootRef.child(Const.USERS).child(otherUid).child(Const.GUEST_LIST)
                                    .child(Const.GUEST_INDEX).setValue(index + 1);
                        else
                            mRootRef.child(Const.USERS).child(otherUid).child(Const.GUEST_LIST)
                                    .child(Const.GUEST_INDEX).setValue(0);

                    }
                    // start creating guest stuff
                    else
                    {
                        // setting me to 0th position
                        mRootRef.child(Const.USERS).child(otherUid).child(Const.GUEST_LIST).child("" + 0)
                                .setValue(myUid + "%" + mUserMe.getFullName() + "$" + mUserMe.getDepartment());

                        // creating index
                        mRootRef.child(Const.USERS).child(otherUid).child(Const.GUEST_LIST)
                                .child(Const.GUEST_INDEX).setValue(1);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError)
                {

                }
            });



        /*
            Button part
         */

        // if i have sent him request -> request sent
        if (isMyIdInHisRequestList())
        {
            friendOrLimitB.setText(getString(R.string.request_sent));
            friendOrLimitB.setTextColor(getResources().getColor(R.color.editorColorPrimary));
            //friendOrLimitB.setSupportBackgroundTintList
            // (ContextCompat.getColorStateList(ProfileActivity.this, R.color.editorColorPrimary));
            friendOrLimitB.setClickable(false);
            friendOrLimitB.setEnabled(false);
        }

        // if he has sent me request -> approve
        else if (isHisIdInMyRequestList())
        {
            friendOrLimitB.setText(getString(R.string.approve));
            friendOrLimitB.setTextColor(getResources().getColor(R.color.colorPrimary));
            //friendOrLimitB.setSupportBackgroundTintList
            //      (ContextCompat.getColorStateList(ProfileActivity.this, R.color.colorPrimary));
            friendOrLimitB.setEnabled(true);
            friendOrLimitB.setClickable(true);
            friendOrLimitB.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    // add me to his friend list
                    otherUserRef.child(Const.FRIEND_LIST).push()
                            .setValue(myUid);
                    //  and him to my friend list
                    myUserRef.child(Const.FRIEND_LIST).push()
                            .setValue(otherUid);

                    // find and delete his request notification in my profile
                    myUserRef.child(Const.NOTIFICATION_MANAGER).child(Const.NOTIF_FRIEND_REQUEST).addValueEventListener(new ValueEventListener()
                    {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot)
                        {
                            for (DataSnapshot snap : dataSnapshot.getChildren())
                            {
                                if (otherUid.equals(snap.getValue(Notification.class).getSenderId()))
                                {
                                    myUserRef.child(Const.NOTIFICATION_MANAGER).child(Const.NOTIF_FRIEND_REQUEST)
                                            .child(snap.getKey())
                                            .setValue(null);
                                }

                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError)
                        {
                        }
                    });

                    //  send him notification that you are friend
                    Notification newNotif = new Notification(mUserMe.getFullName(), myUid, Const.NOTIF_TYPE_APPROVED);
                    newNotif.setSenderProfilePhotoId(mUserMe.getProfilePhotoId()); // todo v2 wrong! do not send profile photo id
                    Object ts = ServerValue.TIMESTAMP; // long
                    newNotif.setTimeSent(ts);
                    otherUserRef.child(Const.NOTIFICATION_MANAGER).child(Const.NOTIF_FRIEND_APPROVED).push()
                            .setValue(newNotif);

                    animateViews();
                }
            });
        }

        // if i am not his friend -> add friend
        else if (!amIHisFriend())
        {
            friendOrLimitB.setTextColor(getResources().getColor(R.color.colorPrimary));
            // friendOrLimitB.setSupportBackgroundTintList
            //       (ContextCompat.getColorStateList(ProfileActivity.this, R.color.colorPrimary));
            friendOrLimitB.setText(getString(R.string.add_friend));
            friendOrLimitB.setEnabled(true);
            friendOrLimitB.setClickable(true);
            friendOrLimitB.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    // sending him notification which he can approve
                    Notification newNotif = new Notification(mUserMe.getFullName(), myUid, Const.NOTIF_TYPE_REQUEST);
                    newNotif.setSenderProfilePhotoId(mUserMe.getProfilePhotoId());  // todo v2 wrong profile photo id
                    Object ts = ServerValue.TIMESTAMP; // long
                    newNotif.setTimeSent(ts);
                    otherUserRef.child(Const.NOTIFICATION_MANAGER).child(Const.NOTIF_FRIEND_REQUEST).push()
                            .setValue(newNotif);

                    animateViews();
                }
            });
        }

        // if i am his friend -> unfriend
        else if (amIHisFriend())
        {
            friendOrLimitB.setText(getString(R.string.unfriend));
            friendOrLimitB.setTextColor(getResources().getColor(R.color.editorColorPrimary));
            // friendOrLimitB.setSupportBackgroundTintList
            //       (ContextCompat.getColorStateList(ProfileActivity.this, R.color.editorColorPrimary));
            friendOrLimitB.setEnabled(true);
            friendOrLimitB.setClickable(true);
            friendOrLimitB.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {

                    // remove him from my friend list
                    myUserRef.child(Const.FRIEND_LIST).addValueEventListener(new ValueEventListener()
                    {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot)
                        {
                            String keyToDelete = null;
                            for (DataSnapshot friend : dataSnapshot.getChildren())
                            {
                                if (friend.getValue().equals(otherUid))
                                {
                                    keyToDelete = friend.getKey();
                                    break;
                                }
                            }

                            // setting item to null will remove it
                            if (keyToDelete != null)
                                myUserRef.child(Const.FRIEND_LIST).child(keyToDelete).setValue(null);
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError)
                        {

                        }
                    });

                    // remove me from his friend list and
                    otherUserRef.child(Const.FRIEND_LIST).addValueEventListener(new ValueEventListener()
                    {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot)
                        {
                            String keyToDelete = null;
                            for (DataSnapshot friend : dataSnapshot.getChildren())
                            {
                                if (friend.getValue().equals(myUid))
                                {
                                    keyToDelete = friend.getKey();
                                    break;
                                }
                            }

                            // setting item to null will remove it
                            if (keyToDelete != null)
                                otherUserRef.child(Const.FRIEND_LIST).child(keyToDelete).setValue(null);
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError)
                        {

                        }
                    });

                    animateViews();
                }
            });
        }

    }

    // 3 methods for if statements in notMyProfile()
    private boolean amIHisFriend()
    {
        boolean isFriend = false;
        for (Map.Entry<String, String> friend : mUser.getFriendsList().entrySet())
        {
            if (friend.getValue().equals(myUid))
            {
                isFriend = true;
                break;
            }
        }

        return isFriend;
    }

    private boolean isMyIdInHisRequestList()
    {
        for (Map.Entry<String, Notification> notification : mUser.getNotificationManager().getFriendRequestList().entrySet())
            if (myUid.equals(notification.getValue().getSenderId()))
                return true;
        return false;
    }

    private boolean isHisIdInMyRequestList()
    {
        for (Map.Entry<String, Notification> notification : mUserMe.getNotificationManager().getFriendRequestList().entrySet())
            if (otherUid.equals(notification.getValue().getSenderId()))
                return true;
        return false;
    }

    // inside displayWholeProfile. download and display photos in horizontal view only
    private void displayPhotos()
    {
        photosNumTV.setText("" + mPhotos.size());

        // if no photos found
        if (mPhotos.size() == 0)
        {
            // setting height of cardview in dp if no photos found
            int heightDp = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 64, getResources().getDisplayMetrics());
            photosCV.getLayoutParams().height = heightDp;
        }

        // starting downloading photos
        else
        {
            noPhotosTV.setVisibility(View.GONE);

            SortedSet<String> keys = new TreeSet<>(mPhotos.keySet());

            // sorting in reversed order
            List<String> list = new ArrayList<>(keys);
            Collections.sort(list, Collections.reverseOrder());
            Set<String> reversedList = new LinkedHashSet<String>(list);

            for (final String key : reversedList)
            {
                String value = mPhotos.get(key);

                final String photoId = value; // already contains  preview part
                final String postId = value.substring(Const.PHOTO_PREVIEW.length(), value.length());

                // creating and adding image view
                final ImageView photoIV = new ImageView(ProfileActivity.this);
                photosParams.setMargins(0, 0, marginDips, 0);
                photoIV.setLayoutParams(photosParams);
                photoIV.setAdjustViewBounds(true);
                photoIV.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        //Toast.makeText(ProfileActivity.this, "Photo", Toast.LENGTH_SHORT).show();
                        Intent photoActivity = new Intent(ProfileActivity.this, PostItemActivity.class);
                        photoActivity.putExtra(Const.UID, myUid);
                        photoActivity.putExtra(Const.OTHER_UID, otherUid);
                        photoActivity.putExtra(Const.POST_ID, postId);
                        photoActivity.putExtra(Const.POST_FOR_FRIENDS_OR_BILKENT, "true");
                        photoActivity.putExtra(Const.POST_KEY, key);
                        photoActivity.putExtra(Const.FULL_NAME, mUser.getFullName());
                        photoActivity.putExtra(Const.PROFILE_PHOTO_ID, mUser.getProfilePhotoId());

                        startActivity(photoActivity);
                    }
                });
                photoLL.addView(photoIV);

                StorageReference storageReference = mStorageRef.child(Const.USERS).child(otherUid).child(Const.PHOTOS_LISTS)
                        .child(photoId);
                Glide.with(getApplicationContext())
                        .using(new FirebaseImageLoader())
                        .load(storageReference)
                        .into(photoIV);
            }
        }
    }

    @Override
    protected void onStart()
    {
        super.onStart();
        animateViews();
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        animateViews();

        mRootRef.child(Const.USERS).child(otherUid).addListenerForSingleValueEvent(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                mUser = dataSnapshot.getValue(User.class);
                mPosts.clear();
                displayPosts();
            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {

            }
        });
    }

    // method to animate these views when needed
    private void animateViews()
    {
        setAnimation(statusCV, 0);
        setAnimation(otherInfoCV, 1);
        setAnimation(numbersCV, 2);
        setAnimation(photosCV, 3);
    }

    // general method to animate a view
    private void setAnimation(View viewToAnimate, int position)
    {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > -1)
        {
            viewToAnimate.animate().cancel();
            viewToAnimate.setTranslationY(100);
            viewToAnimate.setAlpha(0);
            viewToAnimate.animate().alpha(1.0f).translationY(0).setDuration(350).setStartDelay(position * 31);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.

        getMenuInflater().inflate(R.menu.sign_out, menu);
        if (myUid.equals(otherUid))
            menu.getItem(0).setTitle("Sign Out");
        else
            menu.getItem(0).setTitle("Report user");

        return true;
    }

    // pressing back button
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();
        if (id == android.R.id.home)
            finish();
        if (id == R.id.action_settings)
        {
            if (myUid.equals(otherUid))
            {
                FirebaseAuth.getInstance().signOut();
                Intent main = new Intent(ProfileActivity.this, MainFeedActivity.class);
                startActivity(main);
            } else
            {
                mRootRef.child(Const.SETTINGS).child(Const.REPORTED_USERS)
                        .push().setValue(otherUid);

                Toast.makeText(ProfileActivity.this, "The user is reported, we will look ASAP", Toast.LENGTH_SHORT)
                        .show();
            }
        }

        return super.onOptionsItemSelected(item);
    }

    // inside displayWholeProfile
    private void displayPosts()
    {
        PostRecycler.PostAdapter adapter = new PostRecycler(myUid, otherUid).new PostAdapter(mPosts);
        mProfileRV.setLayoutManager(new LinearLayoutManager(ProfileActivity.this));
        mProfileRV.setAdapter(adapter);

        // only needed posts
        for (Map.Entry<String, Post> post : mUser.getPostsList().entrySet())
        {
            if (!post.getValue().getType().equals(Const.POST_TYPE_PHOTO)
                    && !post.getValue().isAnonymous())
                mPosts.add(post.getValue());
        }

        postsNumTV.setText(Integer.toString(mPosts.size()));

        // sorting list of posts
        Collections.sort(mPosts, new Comparator<Post>()
        {
            @Override
            public int compare(Post p1, Post p2)
            {
                return p2.getTimeSent().toString()
                        .compareTo(p1.getTimeSent().toString());
            }
        });

        adapter = new PostRecycler(myUid, otherUid).new PostAdapter(mPosts);
        mProfileRV.setAdapter(adapter);
    }


}