package nurunabiyev.android.traddictive.profile;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import nurunabiyev.android.traddictive.Const;
import nurunabiyev.android.traddictive.R;
import nurunabiyev.android.traddictive.objects.User;

/**
 * A sign in screen that offers login via email/password
 */


public class SignupActivity extends AppCompatActivity
{
    private static final String TAG = "SignupActivity";
    // input
    private EditText fullNameET;
    private EditText emailET;
    private EditText passwordET;
    private EditText password2ET;
    private Button signupB;
    private TextView loginLinkTV;
    private ScrollView mScrollView;
    // input length limits:
    private final int MIN_PWD_LENGTH = 6;
    private final int MAX_PWD_LENGTH = 66;
    private final int MIN_NAME_LENGTH = 5;
    private final int MAX_NAME_LENGTH = 55;
    // firebase
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private FirebaseDatabase mDatabase;
    private DatabaseReference rootRef;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        initializeActivity();
        initializeUser();

        signupB.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                signup();
            }
        });

        loginLinkTV.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                finish();
            }
        });
    }

    private void initializeActivity()
    {
        fullNameET = (EditText) findViewById(R.id.signup_input_fullname);
        emailET = (EditText) findViewById(R.id.signup_input_email);
        passwordET = (EditText) findViewById(R.id.signup_input_password);
        password2ET = (EditText) findViewById(R.id.signup_input_password2);
        signupB = (Button) findViewById(R.id.signup_b);
        loginLinkTV = (TextView) findViewById(R.id.login_link_tv);
        mScrollView = (ScrollView) findViewById(R.id.signup_sv);

        // Find the root view
        View root = emailET.getRootView();
        // Set the color of the activity
        root.setBackgroundColor(getResources().getColor(R.color.login_background_color));

    }


    private void initializeUser()
    {
        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener()
        {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth)
            {
            }
        };

        mDatabase = FirebaseDatabase.getInstance();
        rootRef = mDatabase.getReference();
    }

    private void createAccount(String name, String email, String password)
    {
        final ProgressDialog pd = ProgressDialog
                .show(this, null, getResources().getString(R.string.creating_account_progress), true);

        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>()
                {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task)
                    {
                        if (!task.isSuccessful())
                        {
                            onSignupFailed();
                            pd.dismiss();
                        } else
                        {
                            FirebaseUser u = FirebaseAuth.getInstance().getCurrentUser();
                            if (u != null)
                            {
                                u.sendEmailVerification();
                                createUserInDatabase(u);
                                onSignupSuccess();
                                pd.dismiss();
                            } else
                            {
                                Toast.makeText(getBaseContext(), "Please, retry", Toast.LENGTH_LONG).show();
                            }

                        }
                    }
                });
    }

    private void createUserInDatabase(FirebaseUser u)
    {
        DatabaseReference usersRef = rootRef.child(Const.USERS).child(u.getUid());

        User user = new User(u.getUid(), u.getEmail());
        user.setFullName(fullNameET.getText().toString());
        user.setDepartment(Const.DEFAULT_DEPARTMENT);
        user.setHometown(Const.DEFAULT_HOMETOWN);
        user.setRelationshipStatus(Const.DEFAULT_RELATIONSHIP_ST);
        user.setBirthday(Const.DEFAULT_BIRTHDAY);
        user.setStatus(Const.DEFAULT_STATUS);
        user.setBanned(false);
        // social info
        //user.setAskBilkentStats(0);

        //limits per 24hrs
        user.setLimitPublicSecrets(5);
        user.setLimitPhotos(5);
        user.setLimitAnonForFriends(5);
        user.setLimitQuestions(5);
        user.setLimitHangoutsHosted(5);
        user.setLimitComments(50);

        usersRef.setValue(user);
    }


    @Override
    protected void onStart()
    {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    protected void onStop()
    {
        super.onStop();
        if (mAuthListener != null)
            mAuth.removeAuthStateListener(mAuthListener);
    }

    public void signup()
    {
        if (!validate())
        {
            onSignupFailed();
            return;
        }

        signupB.setEnabled(false);

        String name = fullNameET.getText().toString();
        String email = emailET.getText().toString();
        String password = passwordET.getText().toString();

        createAccount(name, email, password);
    }


    public void onSignupSuccess()
    {
        signupB.setEnabled(true);
        setResult(RESULT_OK, null);
        Toast.makeText(getBaseContext(), R.string.approve_your_account, Toast.LENGTH_LONG).show();
        finish();
    }

    public void onSignupFailed()
    {
        Toast.makeText(SignupActivity.this, R.string.signup_failed, Toast.LENGTH_LONG).show();
        signupB.setEnabled(true);
    }

    public boolean validate()
    {
        boolean valid = true;

        String name = fullNameET.getText().toString();
        String email = emailET.getText().toString();
        String password = passwordET.getText().toString();
        String password2 = password2ET.getText().toString();

        // bilkent email stuff
        String emailDomainPart = email.substring(email.lastIndexOf("@") + 1);   // after domain only
        if (!emailDomainPart.contains("bilkent"))
        {
            String invalidText = getResources().getString(R.string.error_not_bilkent_email);
            emailET.setError(invalidText);
            valid = false;
        } else if (!password.equals(password2))
        {
            // password mismatch
            String invalidText = getResources().getString(R.string.error_password_mismatch);
            password2ET.setError(invalidText);
            valid = false;
        } else if (name.isEmpty() || name.length() < MIN_NAME_LENGTH || name.length() > MAX_NAME_LENGTH)
        {
            // name problem
            String invalidText = getResources().getString(R.string.error_invalid_name);
            fullNameET.setError(invalidText);
            valid = false;
        } else if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches())
        {
            // email problem
            String invalidText = getResources().getString(R.string.error_invalid_email);
            emailET.setError(invalidText);
            valid = false;
        } else if (password.isEmpty() || password.length() < MIN_PWD_LENGTH || password.length() > MAX_PWD_LENGTH)
        {
            // password problem
            String invalidText = getResources().getString(R.string.error_invalid_password);
            passwordET.setError(invalidText);
            valid = false;
        } else
        {
            // everything is ok
            fullNameET.setError(null);
            passwordET.setError(null);
            password2ET.setText(null);
            emailET.setError(null);
        }

        return valid;
    }
}