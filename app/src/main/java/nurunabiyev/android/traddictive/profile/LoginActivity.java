package nurunabiyev.android.traddictive.profile;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.stephentuso.welcome.WelcomeHelper;

import nurunabiyev.android.traddictive.R;
import nurunabiyev.android.traddictive.WelcomeTraddictive;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity
{
    private static final String TAG = "LoginActivity";

    private static final int REQUEST_SIGNUP = 0;

    // input variables
    private EditText emailET;
    private EditText passwordET;
    private Button loginB;
    private TextView signupLinkTV;
    // input length limits:
    private final int MIN_PWD_LENGTH = 6;
    private final int MAX_PWD_LENGTH = 66;
    // firebase
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;

    // welcome
    private WelcomeHelper welcomeScreen;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initializeUser();
        initializeActivity();

        welcomeScreen = new WelcomeHelper(this, WelcomeTraddictive.class);
        welcomeScreen.show(savedInstanceState);

        loginB.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                login();
            }
        });

        signupLinkTV.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                // Start the Signup activity
                Intent intent = new Intent(getApplicationContext(), SignupActivity.class);
                //startActivityForResult(intent, REQUEST_SIGNUP);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        welcomeScreen.onSaveInstanceState(outState);
    }

    private void initializeActivity()
    {
        emailET = (EditText) findViewById(R.id.login_email_et);
        passwordET = (EditText) findViewById(R.id.login_password_et);
        loginB = (Button) findViewById(R.id.login_b);
        signupLinkTV = (TextView) findViewById(R.id.signup_link_tv);

        // Find the root view
        View root = emailET.getRootView();
        // Set the color of the activity
        root.setBackgroundColor(getResources().getColor(R.color.login_background_color));

    }

    private void initializeUser()
    {
        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener()
        {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth)
            {
            }
        };
    }

    private void signIn(String email, String password)
    {
        final ProgressDialog pd = ProgressDialog
                .show(this, null, getResources().getString(R.string.login_authenticating), true);

        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>()
                {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task)
                    {
                        if (!task.isSuccessful())
                        {
                            onLoginFailed();
                            pd.dismiss();
                        }
                        else
                        {
                            new android.os.Handler().postDelayed(
                                    new Runnable()
                                    {
                                        public void run()
                                        {
                                            onLoginSuccess();
                                            pd.dismiss();
                                        }
                                    }, 1000);
                        }
                    }
                });
    }

    @Override
    protected void onStart()
    {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    protected void onStop()
    {
        super.onStop();
        if (mAuthListener != null)
            mAuth.removeAuthStateListener(mAuthListener);
    }

    private void login()
    {
        if (!validate())
        {
            onLoginFailed();
            return;
        }

        loginB.setEnabled(false);

        String email = emailET.getText().toString();
        String password = passwordET.getText().toString();

        signIn(email, password);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == REQUEST_SIGNUP)
        {
            if (resultCode == RESULT_OK)
            {
                // By default we just finish the Activity and log them in automatically
                this.finish();
            }
        }
    }

    @Override
    public void onBackPressed()
    {
        // disable going back to the MainActivity
        moveTaskToBack(true);
    }

    private void onLoginSuccess()
    {
        loginB.setEnabled(true);
        finish();
    }

    private void onLoginFailed()
    {
        Toast.makeText(getBaseContext(), R.string.login_failed, Toast.LENGTH_LONG).show();
        loginB.setEnabled(true);
    }

    // validating email and password
    private boolean validate()
    {
        boolean valid = true;

        String email = emailET.getText().toString();
        String password = passwordET.getText().toString();

        // bilkent email stuff
        String emailDomainPart = email.substring(email.lastIndexOf("@") + 1);   // after domain only
        if (!emailDomainPart.contains("bilkent"))
        {
            String invalidText = getResources().getString(R.string.error_not_bilkent_email);
            Toast.makeText(getBaseContext(), invalidText, Toast.LENGTH_SHORT).show();
            valid = false;
        }

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches())
        {
            String invalidText = getResources().getString(R.string.error_invalid_email);
            emailET.setError(invalidText);
            valid = false;
        } else
        {
            emailET.setError(null);
        }

        if (password.isEmpty() || password.length() < MIN_PWD_LENGTH || password.length() > MAX_PWD_LENGTH)
        {
            String invalidText = getResources().getString(R.string.error_invalid_password);
            passwordET.setError(invalidText);
            valid = false;
        } else
        {
            passwordET.setError(null);
        }

        return valid;
    }


    // test todo delete
    public void testLoginAsNuru(View view)
    {
        signIn("nuru.nabiyev@ug.bilkent.edu.tr", "123456");
    }

    public void testLoginAsKenan(View view)
    {
        signIn("kenan.asadov@ug.bilkent.edu.tr", "123123");
    }
}

