package nurunabiyev.android.traddictive.profile;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import nurunabiyev.android.traddictive.Const;
import nurunabiyev.android.traddictive.R;
import nurunabiyev.android.traddictive.feed.TabbedActivity;
import nurunabiyev.android.traddictive.objects.Notification;
import nurunabiyev.android.traddictive.objects.User;

/**
 * Shows all notifications in recycler view
 */
public class NotificationActivity extends AppCompatActivity
{

    private static final String TAG = "NotificationActivity";
    private RecyclerView mRecyclerView;
    private SwipeRefreshLayout mNotificationSRL;
    private TextView mNoNotifications;
    // user
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private FirebaseUser mFirebaseUser;
    private DatabaseReference mDatabaseRef;
    private StorageReference mStorageRef;
    private String myUid;
    private User mUser;
    // data
    private ArrayList<Notification> mNotifications;
    private NotificationAdapter mNotifAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_notification);

        initializeView();
        initializeFirebase();
    }

    private void initializeView()
    {
        myUid = getIntent().getStringExtra(Const.UID);

        Toolbar toolbar = (Toolbar) findViewById(R.id.notification_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // change the color of gradient
        if (android.os.Build.VERSION.SDK_INT >= 21)
        {
            Window window = NotificationActivity.this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.editorColorPrimaryDark));
        }

        mRecyclerView = (RecyclerView) findViewById(R.id.notification_rv);
        mNotificationSRL = (SwipeRefreshLayout) findViewById(R.id.notification_srl);
        mNoNotifications = (TextView) findViewById(R.id.no_new_notif);
    }

    private void initializeFirebase()
    {
        mDatabaseRef = FirebaseDatabase.getInstance().getReference();
        mStorageRef = FirebaseStorage.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();

        mAuthListener = new FirebaseAuth.AuthStateListener()
        {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth)
            {
                mFirebaseUser = firebaseAuth.getCurrentUser();
                if (mFirebaseUser == null)
                {
                    Intent loginIntent = new Intent(NotificationActivity.this, LoginActivity.class);
                    startActivity(loginIntent);
                    finish();
                } else
                    updateUi();
            }
        };
    }

    private void updateUi()
    {
        // prepare swipe refresh layout
        mNotificationSRL.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener()
        {
            @Override
            public void onRefresh()
            {
                ConnectivityManager conMgr = (ConnectivityManager) getApplication().getSystemService(Context.CONNECTIVITY_SERVICE);
                {
                    NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                    if (netInfo != null)
                        updateRecyclerView();
                    else
                        Toast.makeText(NotificationActivity.this, getString(R.string.error_internet), Toast.LENGTH_SHORT).show();
                }
                mNotificationSRL.setRefreshing(false);
            }
        });
        mNotificationSRL.setColorSchemeResources(android.R.color.holo_blue_dark, android.R.color.holo_red_dark);

        // the rest is recycler view
        updateRecyclerView();
    }

    private void updateRecyclerView()
    {
        DatabaseReference userRef = mDatabaseRef.child(Const.USERS).child(myUid);
        mNotifications = new ArrayList<>();
        mRecyclerView.setLayoutManager(new LinearLayoutManager(NotificationActivity.this));

        userRef.addListenerForSingleValueEvent(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                mNotifications.clear();
                mUser = dataSnapshot.getValue(User.class);

                // filling mNotifications with all notifications
                for (Map.Entry<String, Notification> notification : mUser.getNotificationManager().getFriendRequestList().entrySet())
                    mNotifications.add(notification.getValue());
                for (Map.Entry<String, Notification> notification : mUser.getNotificationManager().getFriendApprovedList().entrySet())
                    mNotifications.add(notification.getValue());
                for (Map.Entry<String, Notification> notification : mUser.getNotificationManager().getHangoutInvites().entrySet())
                    mNotifications.add(notification.getValue());
                // todo v2 other types of notifications

                Collections.sort(mNotifications, new Comparator<Notification>()
                {
                    @Override
                    public int compare(Notification n1, Notification n2)
                    {
                        return n2.getTimeSent().toString()
                                .compareTo(n1.getTimeSent().toString());
                    }
                });

                if (mNotifications.size() > 0)
                {
                    mNoNotifications.setVisibility(View.GONE);
                    mNotificationSRL.setVisibility(View.VISIBLE);
                } else
                {
                    mNoNotifications.setVisibility(View.VISIBLE);
                    mNotificationSRL.setVisibility(View.GONE);
                }

                // setting adapter
                mNotifAdapter = new NotificationAdapter(mNotifications);
                mRecyclerView.setAdapter(mNotifAdapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {

            }
        });
    }

    /**
     * Inner class ViewHolder
     */
    private class NotificationHolder extends RecyclerView.ViewHolder
    {
        private Notification mNotification;
        private CircleImageView senderCIV;
        private TextView mainTextTV;
        private TextView timeTV;
        private CardView notificationCV;

        private NotificationHolder(View itemView)
        {
            super(itemView);

            senderCIV = (CircleImageView) itemView.findViewById(R.id.notif_sender_civ);
            mainTextTV = (TextView) itemView.findViewById(R.id.notif_main_text_tv);
            timeTV = (TextView) itemView.findViewById(R.id.notif_time_tv);
            notificationCV = (CardView) itemView.findViewById(R.id.notification_cv);
        }

        private void bindNotification(Notification notification)
        {
            mNotification = notification;

            // downloading and setting photo to image view
            if(mNotification.getSenderProfilePhotoId() != null)
            {
                StorageReference storageReference = mStorageRef.child(Const.USERS)
                        .child(mNotification.getSenderId()).child(Const.PHOTOS_LISTS)
                        .child(Const.PHOTO_THUMBNAIL + mNotification.getSenderProfilePhotoId());
                Glide.with(getApplicationContext())
                        .using(new FirebaseImageLoader())
                        .load(storageReference)
                        .into(senderCIV);
            }
            else
                senderCIV.setImageResource(R.drawable.default_avatar);

            // date
            Date sentTimeDate = new Date(Long.parseLong(mNotification.getTimeSent().toString()));
            String dateText = (String) DateFormat.format("d MMMM HH:mm", sentTimeDate);
            timeTV.setText(dateText);

            // setting the main text of the notification
            String notifMainText = " invalid notification. Please report";
            switch (mNotification.getType())
            {
                case Const.NOTIF_TYPE_APPROVED:
                    notifMainText = getString(R.string.type_friend_approved);
                    break;
                case Const.NOTIF_TYPE_REQUEST:
                    notifMainText = getString(R.string.type_friend_request);
                    break;
                case Const.NOTIF_TYPE_HANGOUT_INVITE:
                    notifMainText = getString(R.string.type_hangout_invitation);
                    break;
            }
            // making name of the sender Bold
            SpannableStringBuilder str = new SpannableStringBuilder(mNotification.getSenderName() + " " + notifMainText);
            str.setSpan
                    (
                            new android.text.style.StyleSpan(android.graphics.Typeface.BOLD),
                            0,
                            mNotification.getSenderName().length(),
                            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                    );
            mainTextTV.setText(str);

            // click listener
            clickingOnNotification();
        }

        private void clickingOnNotification()
        {

            notificationCV.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    if (mNotification.getType().equals(Const.NOTIF_TYPE_APPROVED))
                    {
                        // open sender profile
                        Intent intent = new Intent(NotificationActivity.this, ProfileActivity.class);
                        intent.putExtra(Const.FULL_NAME, mNotification.getSenderName());
                        intent.putExtra(Const.UID, myUid);
                        intent.putExtra(Const.OTHER_UID, mNotification.getSenderId());
                        startActivity(intent);

                        // todo v2 delete notification
                    }

                    if (mNotification.getType().equals(Const.NOTIF_TYPE_REQUEST))
                    {
                        // open sender profile
                        Intent intent = new Intent(NotificationActivity.this, ProfileActivity.class);
                        intent.putExtra(Const.FULL_NAME, mNotification.getSenderName());
                        intent.putExtra(Const.UID, myUid);
                        intent.putExtra(Const.OTHER_UID, mNotification.getSenderId());
                        startActivity(intent);
                    }

                    if (mNotification.getType().equals(Const.NOTIF_TYPE_HANGOUT_INVITE))
                    {
                        // open hangout

                        // check if hangout exists
                        mDatabaseRef.child(Const.HANGOUTS)
                                .child(mNotification.getHangoutKey()).addListenerForSingleValueEvent(new ValueEventListener()
                        {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot)
                            {
                                String key = dataSnapshot.child(Const.HANGOUT_KEY).getValue(String.class);

                                if (key != null)
                                {

                                    Intent intent = new Intent(itemView.getContext(), TabbedActivity.class);
                                    Bundle viewPagerSettings = new Bundle();
                                    viewPagerSettings.putInt(Const.PARAM_PAGE_COUNT, 3);
                                    viewPagerSettings.putString(Const.PARAM_PAGE_1, itemView.getContext().getString(R.string.hangout_details));
                                    viewPagerSettings.putString(Const.PARAM_PAGE_2, itemView.getContext().getString(R.string.hangout_hosts_messages));
                                    viewPagerSettings.putString(Const.PARAM_PAGE_3, itemView.getContext().getString(R.string.hangout_group_chat));
                                    viewPagerSettings.putString(Const.UID, myUid);
                                    viewPagerSettings.putString(Const.OTHER_UID, mNotification.getSenderId());
                                    viewPagerSettings.putString(Const.PARAM_HANGOUT_KEY, mNotification.getHangoutKey());
                                    viewPagerSettings.putString(Const.PARAM_HANGOUT_HOST_ID, mNotification.getSenderId());
                                    viewPagerSettings.putString(Const.PARAM_PAGER_TYPE, Const.PARAM_HANGOUT_DETAILS);
                                    intent.putExtra(Const.PARAM_BUNDLE, viewPagerSettings);
                                    itemView.getContext().startActivity(intent);

                                }

                                // exiting hangout
                                else
                                {
                                    finish();
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError)
                            {

                            }
                        });
                    }
                }
            });
        }
    }


    /**
     * Inner class Adapter
     */
    private class NotificationAdapter extends RecyclerView.Adapter<NotificationHolder>
    {

        private List<Notification> mNotifications;
        private int lastPosition = -1;

        private NotificationAdapter(List<Notification> notifications)
        {
            mNotifications = notifications;
        }


        @Override
        public NotificationHolder onCreateViewHolder(ViewGroup parent, int viewType)
        {
            LayoutInflater inflater = LayoutInflater.from(NotificationActivity.this);
            View view = inflater.inflate(R.layout.list_item_notification, parent, false);

            return new NotificationHolder(view);
        }

        @Override
        public void onBindViewHolder(NotificationHolder holder, int position)
        {
            Notification n = mNotifications.get(position);
            holder.bindNotification(n);

            setAnimation(holder.notificationCV, position);
        }

        private void setAnimation(View viewToAnimate, int position)
        {
            // If the bound view wasn't previously displayed on screen, it's animated
            if (position > lastPosition)
            {
                viewToAnimate.animate().cancel();
                viewToAnimate.setTranslationY(100);
                viewToAnimate.setAlpha(0);
                viewToAnimate.animate().alpha(1.0f).translationY(0).setDuration(250).setStartDelay(position * 2);
            }
        }

        @Override
        public int getItemCount()
        {
            return mNotifications.size();
        }
    }

    // back
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();

        if (id == android.R.id.home)
            finish();

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart()
    {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    protected void onStop()
    {
        super.onStop();
        if (mAuthListener != null)
            mAuth.removeAuthStateListener(mAuthListener);
    }
}
