package nurunabiyev.android.traddictive;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import nurunabiyev.android.traddictive.profile.ProfileActivity;

import static nurunabiyev.android.traddictive.R.id.more_bug_report_b;
import static nurunabiyev.android.traddictive.R.id.more_improve_b;
import static nurunabiyev.android.traddictive.R.id.more_kenan;
import static nurunabiyev.android.traddictive.R.id.more_nuru;
import static nurunabiyev.android.traddictive.R.id.more_question_b;

public class MoreActivity extends AppCompatActivity
{

    @BindView(more_bug_report_b) Button bugReportB;
    @BindView(more_question_b) Button questionB;
    @BindView(more_improve_b) Button improveB;
    @BindView(more_nuru) LinearLayout nuruLL;
    @BindView(more_kenan) LinearLayout kenanLL;

    // user
    private DatabaseReference mDatabaseReference;
    private String uId;
    private String userEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more);

        ButterKnife.bind(this);

        initializeView();
        initializeFirebase();
    }

    private void initializeFirebase()
    {
        mDatabaseReference = FirebaseDatabase.getInstance().getReference();

        uId = getIntent().getStringExtra(Const.UID);

        mDatabaseReference.child(Const.USERS).child(uId)
                .child(Const.USER_EMAIL).addListenerForSingleValueEvent(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                userEmail = dataSnapshot.getValue(String.class);

                prepareDialogs();
                initGods();
            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {

            }
        });

    }

    private void prepareDialogs()
    {
        final AlertDialog.Builder alert = new AlertDialog.Builder(MoreActivity.this);

        // bug report
        bugReportB.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {

                final EditText edittext = new EditText(MoreActivity.this);
                edittext.setMaxLines(20);
                int dpValue = 16; // margin in dips
                float d = MoreActivity.this.getResources().getDisplayMetrics().density;
                int margin = (int) (dpValue * d); // margin in pixels
                edittext.setPadding(margin, margin, 0, margin);

                alert.setView(edittext);

                alert.setTitle(getString(R.string.bug_report));
                alert.setMessage(getString(R.string.bug_report_message));

                alert.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int whichButton)
                    {
                        dialog.dismiss();
                    }
                });

                alert.setPositiveButton(getString(R.string.send), new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int whichButton)
                    {
                        mDatabaseReference.child(Const.SETTINGS).child(Const.BUG_REPORTS)
                                .child("Android").push()
                                .setValue(userEmail + ": " + edittext.getText().toString());

                        Toast.makeText(getApplicationContext(), getString(R.string.thanks_for_collab),
                                Toast.LENGTH_SHORT).show();

                        dialog.dismiss();
                    }
                });

                alert.show();
            }
        });



        // question
        questionB.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {

                final EditText edittext = new EditText(MoreActivity.this);
                edittext.setMaxLines(20);
                int dpValue = 16; // margin in dips
                float d = MoreActivity.this.getResources().getDisplayMetrics().density;
                int margin = (int) (dpValue * d); // margin in pixels
                edittext.setPadding(margin, margin, 0, margin);

                alert.setView(edittext);

                alert.setTitle(getString(R.string.question));
                alert.setMessage(getString(R.string.question_message));

                alert.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int whichButton)
                    {
                        dialog.dismiss();
                    }
                });

                alert.setPositiveButton(getString(R.string.send), new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int whichButton)
                    {
                        mDatabaseReference.child(Const.SETTINGS).child(Const.QUESTIONS)
                                .child("Android").push()
                                .setValue(userEmail + ": " + edittext.getText().toString());

                        Toast.makeText(getApplicationContext(), getString(R.string.thanks_for_collab),
                                Toast.LENGTH_SHORT).show();

                        dialog.dismiss();
                    }
                });

                alert.show();
            }
        });



        // make better
        improveB.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                final EditText edittext = new EditText(MoreActivity.this);
                edittext.setMaxLines(20);
                int dpValue = 16; // margin in dips
                float d = MoreActivity.this.getResources().getDisplayMetrics().density;
                int margin = (int) (dpValue * d); // margin in pixels
                edittext.setPadding(margin, margin, 0, margin);

                alert.setView(edittext);

                alert.setTitle(getString(R.string.improve));
                alert.setMessage(getString(R.string.improve_message));

                alert.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int whichButton)
                    {
                        dialog.dismiss();
                    }
                });

                alert.setPositiveButton(getString(R.string.send), new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int whichButton)
                    {
                        mDatabaseReference.child(Const.SETTINGS).child(Const.IMPROVE)
                                .child("Android").push()
                                .setValue(userEmail + ": " + edittext.getText().toString());

                        Toast.makeText(getApplicationContext(), getString(R.string.thanks_for_collab),
                                Toast.LENGTH_SHORT).show();

                        dialog.dismiss();
                    }
                });

                alert.show();
            }
        });
    }

    private void initializeView()
    {
        Toolbar toolbar = (Toolbar) findViewById(R.id.more_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // change the color of gradient
        if (android.os.Build.VERSION.SDK_INT >= 21)
        {
            Window window = MoreActivity.this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.colorPrimaryDark));
        }

    }

    // todo ids & names of Gods
    // todo v2 from firebase
    private void initGods()
    {
        // init nuru
        nuruLL.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent(MoreActivity.this, ProfileActivity.class);
                intent.putExtra(Const.FULL_NAME, "Nuru Nabiyev");
                intent.putExtra(Const.UID, uId);
                intent.putExtra(Const.OTHER_UID, "6jsRVlQSbjTxQ1ct5yoqUEAZkFF3");
                startActivity(intent);
            }
        });

        // init kenan
        kenanLL.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent(MoreActivity.this, ProfileActivity.class);
                intent.putExtra(Const.FULL_NAME, "Kenan");
                intent.putExtra(Const.UID, uId);
                intent.putExtra(Const.OTHER_UID, "BnctbVSNwfdSZHjlpiBYe41IjUs2");
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();

        if (id == android.R.id.home)
            finish();

        return super.onOptionsItemSelected(item);
    }
}
