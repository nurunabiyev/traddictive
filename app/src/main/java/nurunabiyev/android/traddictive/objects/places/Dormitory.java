package nurunabiyev.android.traddictive.objects.places;

import com.google.firebase.database.Exclude;

import java.util.ArrayList;
import java.util.List;

/**
 * Dormitory class, child of a Building class
 */
public class Dormitory extends Building
{
    // properties
    private int floors;
    private List<Integer> typeOfRooms;
    private boolean gender;  // true if for men, false if for women, null if both

    public Dormitory()
    {
        typeOfRooms = new ArrayList<>();
    }

    public Dormitory(String n, String atCampus)
    {
        super(n, atCampus);
        typeOfRooms = new ArrayList<>();
        /**
         * Quadruple Room = 0 /
         * Triple Room  = 1 /
         * Double Room(bunk beds) = 2 / Double Room(single beds) = 3 / Double Room(Single Beds Shared Kitchen Private Bathroom) = 4
         * Single Room(Standard) = 5 /  Single Room(Private Bathroom) = 6 /
         * Single Room(Shared Kitchen Private Bathroom) = 7 / Suite(Double Occupancy) = 8 / Suite(Single Occupancy) = 9
         */
    }

    // setters
    @Exclude
    public void setTypeOfRooms(int... types)
    {
        for (int type : types)
        {
            typeOfRooms.add(type);
        }
    }

    public void setNoFloor(int f)
    {
        floors = f;
    }

    public void setGender(boolean gender)
    {
        this.gender = gender;
    }

    // getters
    public List<Integer> getTypeOfRooms()
    {
        return typeOfRooms;
    }

    public int getFloors()
    {
        return floors;
    }

    public boolean getGender()
    {
        return gender;
    }

}
