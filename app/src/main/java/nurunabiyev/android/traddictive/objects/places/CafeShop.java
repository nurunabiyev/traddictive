package nurunabiyev.android.traddictive.objects.places;

/**
 * Cafe and Shop
 */
public class CafeShop extends Building
{
    // properties
    private String timings;
    private String contact;

    public CafeShop(){}

    // constructor
    public CafeShop(String name, String campus)
    {
        super(name, campus);
    }

    // setters
    public void setTimings(String timings)
    {
        this.timings = timings;
    }

    public void setContact(String contact)
    {
        this.contact = contact;
    }

    // getters
    public String getTimings()
    {
        return timings;
    }

    public String getContact()
    {
        return contact;
    }
}
