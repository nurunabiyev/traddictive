package nurunabiyev.android.traddictive.objects;

import java.util.HashMap;
import java.util.Map;

/**
 * POJO - Holds the list of different notifications
 */

public class NotificationManager
{
    private Map<String, Notification> friendRequestList;
    private Map<String, Notification> friendApprovedList;
    private Map<String, Notification> hangoutInvites;
    // todo v2 hangouts, likes, comments

    public NotificationManager()
    {
        friendApprovedList = new HashMap<>();
        friendRequestList = new HashMap<>();
        hangoutInvites = new HashMap<>();
    }

    public boolean hasNewNotification()
    {
        return  (friendApprovedList.size()
                + friendRequestList.size()
                + hangoutInvites.size())
                 > 0;
    }

    public int notificationCount()
    {
        return friendApprovedList.size() + friendRequestList.size();
    }

    public Map<String, Notification> getFriendRequestList()
    {
        return friendRequestList;
    }

    public Map<String, Notification> getFriendApprovedList()
    {
        return friendApprovedList;
    }

    public Map<String, Notification> getHangoutInvites()
    {
        return hangoutInvites;
    }

    public void setHangoutInvites(Map<String, Notification> hangoutInvites)
    {
        this.hangoutInvites = hangoutInvites;
    }
}
