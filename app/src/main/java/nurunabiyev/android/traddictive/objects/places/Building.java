package nurunabiyev.android.traddictive.objects.places;


import java.util.HashMap;
import java.util.Map;

/**
 * Building class
 */
public class Building
{
    // properties
    private String name;
    private double longitude;
    private double latitude;
    private String description;
    private String atCampus;
    // firebase properties
    private Map<String, Rating> ratings;
    private String key;


    public Building()
    {
        ratings = new HashMap<>();
    }

    // constructor
    public Building(String name, String atCampus)
    {
        this.name = name;
        this.atCampus = atCampus;
        ratings = new HashMap<>();
    }

    // setters
    public void setLocation(double longitude, double latitude)
    {
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public void setDescription(String dis)
    {
        description = dis;
    }

    public void setCampus(String atC)
    {
        atCampus = atC;
    }

    // getters

    public String getDescription()
    {
        return description;
    }

    public String getName()
    {
        return name;
    }

    public String getCampus()
    {
        return atCampus;
    }

    public Map<String, Rating> getRatings()
    {
        return ratings;
    }

    public void setRatings(Map<String, Rating> rates)
    {
        this.ratings = rates;
    }

    public double getScore()
    {
        float sum = 0;

        for (Map.Entry<String, Rating> ratingEntry : ratings.entrySet())
            sum += ratingEntry.getValue().getRateScore();

        if (ratings.size() == 0)
            return 0;
        else
            return sum / ratings.size();
    }

    public String getKey()
    {
        return key;
    }

    public void setKey(String key)
    {
        this.key = key;
    }

    public double getLongitude()
    {
        return longitude;
    }

    public double getLatitude()
    {
        return latitude;
    }
}