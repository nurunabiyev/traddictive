package nurunabiyev.android.traddictive.objects;

import nurunabiyev.android.traddictive.Const;

/**
 *
 */

public class Comment extends Post
{
    private String commentKey;

    public Comment()
    {
        super();
        setType(Const.POST_TYPE_COMMENT);
        setComments(null);
    }

    public String getCommentKey() {
        return commentKey;
    }

    public void setCommentKey(String commentKey) {
        this.commentKey = commentKey;
    }
}
