package nurunabiyev.android.traddictive.objects;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Pojo for hangout
 */

public class Hangout
{
    private String title;
    private String host;
    private String description;
    private String language;
    private String key;
    private List<String> tags;
    private Map<String, String> members;
    private Map<String, String> applicants;
    private Map<String, String> invitedFriends;
    private Map<String, HangoutMessage> hostMessages;
    private Map<String, HangoutMessage> chatMessages;
    private boolean openOrClosed;
    private boolean freeOrApproval;
    private boolean isPopular;

    public Hangout()
    {
        tags = new ArrayList<>();
        members = new HashMap<>();
        applicants = new HashMap<>();
        invitedFriends = new HashMap<>();
        hostMessages = new HashMap<>();
        chatMessages = new HashMap<>();
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getLanguage()
    {
        return language;
    }

    public void setLanguage(String language)
    {
        this.language = language;
    }

    public List<String> getTags()
    {
        return tags;
    }

    public void setTags(List<String> tags)
    {
        this.tags = tags;
    }

    public boolean isOpenOrClosed()
    {
        return openOrClosed;
    }

    public void setOpenOrClosed(boolean openOrClosed)
    {
        this.openOrClosed = openOrClosed;
    }

    public boolean isFreeOrApproval()
    {
        return freeOrApproval;
    }

    public void setFreeOrApproval(boolean freeOrApproval)
    {
        this.freeOrApproval = freeOrApproval;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public Map<String, String> getMembers() {
        return members;
    }

    public void setMembers(Map<String, String> members) {
        this.members = members;
    }

    public Map<String, String> getApplicants() {
        return applicants;
    }

    public void setApplicants(Map<String, String> applicants) {
        this.applicants = applicants;
    }

    public boolean isPopular()
    {
        return isPopular;
    }

    public void setPopular(boolean popular)
    {
        isPopular = popular;
    }

    public String getKey()
    {
        return key;
    }

    public void setKey(String key)
    {
        this.key = key;
    }

    public Map<String, HangoutMessage> getHostMessages()
    {
        return hostMessages;
    }

    public void setHostMessages(Map<String, HangoutMessage> hostMessages)
    {
        this.hostMessages = hostMessages;
    }

    public Map<String, HangoutMessage> getChatMessages()
    {
        return chatMessages;
    }

    public void setChatMessages(Map<String, HangoutMessage> chatMessages)
    {
        this.chatMessages = chatMessages;
    }

    public Map<String, String> getInvitedFriends()
    {
        return invitedFriends;
    }

    public void setInvitedFriends(Map<String, String> invitedFriends)
    {
        this.invitedFriends = invitedFriends;
    }
}
