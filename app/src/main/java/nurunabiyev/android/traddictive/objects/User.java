package nurunabiyev.android.traddictive.objects;

import java.util.HashMap;
import java.util.Map;

/**
 * POJO of user
 */

public class User
{
    // user info
    private String userId;
    private String fullName;
    private String email;
    private String department;
    private String hometown;
    private String relationshipStatus;
    private String birthday;
    private boolean isBanned;       // will ban in firebase console
    // social info
    private String status;
    private int reportedTimes;
    //private int askBilkentStats;
    private int karma;  //todo V2
    private String profilePhotoId;
    private Map<String, String> friendsList;   // ids
    private Map<String, String> photosLists;    // preview only
    private Map<String, Post> postsList;
    private Map<String, String> hangoutsList;   // whatever push key, hangout key
    private Map<String, String> buykentList;   // same as hangout list
    private NotificationManager notificationManager;
    // limits PER 24 HRS
    private int limitPhotos;
    private int limitPublicSecrets;
    private int limitAnonForFriends;
    private int limitQuestions;
    private int limitHangoutsHosted;
    private int limitComments;

    /**
     * Constructors
     */

    public User()
    {
        friendsList = new HashMap<>();
        photosLists = new HashMap<>();
        postsList = new HashMap<>();
        hangoutsList = new HashMap<>();
        buykentList = new HashMap<>();
        notificationManager = new NotificationManager();
    }

    public User(String uid, String email)
    {
        this.userId = uid;
        this.email = email;

        friendsList = new HashMap<>();
        photosLists = new HashMap<>();
        postsList = new HashMap<>();
        hangoutsList = new HashMap<>();
        buykentList = new HashMap<>();
        notificationManager = new NotificationManager();
    }


    /**
     * Getters
     */
    public String getUserId()
    {
        return userId;
    }

    public String getFullName()
    {
        return fullName;
    }

    public String getEmail()
    {
        return email;
    }

    public String getDepartment()
    {
        return department;
    }

    public String getHometown()
    {
        return hometown;
    }

    public String getRelationshipStatus()
    {
        return relationshipStatus;
    }

    public String getBirthday()
    {
        return birthday;
    }

    public boolean isBanned()
    {
        return isBanned;
    }

    public Map<String, String> getFriendsList()
    {
        return friendsList;
    }

    public String getStatus()
    {
        return status;
    }

    public int getReportedTimes()
    {
        return reportedTimes;
    }

//    public int getAskBilkentStats()
//    {
//        return askBilkentStats;
//    }

    public int getLimitPhotos()
    {
        return limitPhotos;
    }

    public int getLimitPublicSecrets()
    {
        return limitPublicSecrets;
    }

    public int getLimitAnonForFriends()
    {
        return limitAnonForFriends;
    }

    public int getLimitQuestions()
    {
        return limitQuestions;
    }

    public int getLimitHangoutsHosted()
    {
        return limitHangoutsHosted;
    }

    public int getLimitComments()
    {
        return limitComments;
    }

    public String getProfilePhotoId()
    {
        return profilePhotoId;
    }

    public Map<String, String> getPhotosLists()
    {
        return photosLists;
    }

    public Map<String, Post> getPostsList()
    {
        return postsList;
    }

    public Map<String, String> getBuykentList()
    {
        return buykentList;
    }

    public Map<String, String> getHangoutsList()
    {
        return hangoutsList;
    }





    /*
     * Setters
     */
    public void setHometown(String hometown)
    {
        this.hometown = hometown;
    }

    public void setRelationshipStatus(String relationshipStatus)
    {
        this.relationshipStatus = relationshipStatus;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public void setReportedTimes(int reportedTimes)
    {
        this.reportedTimes = reportedTimes;
    }

//    public void setAskBilkentStats(int askBilkentStats)
//    {
//        this.askBilkentStats = askBilkentStats;
//    }

    public void setLimitPhotos(int limitPhotos)
    {
        this.limitPhotos = limitPhotos;
    }

    public void setLimitPublicSecrets(int limitPublicSecrets)
    {
        this.limitPublicSecrets = limitPublicSecrets;
    }

    public void setLimitAnonForFriends(int limitAnonForFriends)
    {
        this.limitAnonForFriends = limitAnonForFriends;
    }

    public void setLimitQuestions(int limitQuestions)
    {
        this.limitQuestions = limitQuestions;
    }

    public void setLimitHangoutsHosted(int limitHangoutsHosted)
    {
        this.limitHangoutsHosted = limitHangoutsHosted;
    }

    public void setLimitComments(int limitComments)
    {
        this.limitComments = limitComments;
    }

    public void setProfilePhotoId(String profilePhotoId)
    {
        this.profilePhotoId = profilePhotoId;
    }

    public void setBirthday(String birthday)
    {
        this.birthday = birthday;
    }

    public void setPhotosLists(HashMap<String, String> photosLists)
    {
        this.photosLists = photosLists;
    }

    public void setPostsList(Map<String, Post> postsList)
    {
        this.postsList = postsList;
    }

    public void setFullName(String fullName)
    {
        this.fullName = fullName;
    }

    public void setDepartment(String department)
    {
        this.department = department;
    }

    public void setBanned(boolean banned)
    {
        isBanned = banned;
    }

    public void setBuykentList(Map<String, String> buykentItems)
    {
        this.buykentList = buykentItems;
    }

    public void setHangoutsList(Map<String, String> hangoutsList)
    {
        this.hangoutsList = hangoutsList;
    }

    public void setFriendsList(HashMap<String, String> friendsList) {
        this.friendsList = friendsList;
    }

    public NotificationManager getNotificationManager()
    {
        return notificationManager;
    }

    public int getKarma()
    {
        return karma;
    }

    public void setKarma(int karma)
    {
        this.karma = karma;
    }
}
