package nurunabiyev.android.traddictive.objects;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by HP on 16-Aug-17.
 */

public class HangoutMessage
{
    private String text;
    private String messageId;
    private String messageKey;
    private String uploaderId;
    private String uploaderName;
    private String type;    // host, chat
    private Object timeSent;
    private Map<String, String> likes;
    private Map<String, String> dislikes;

    public HangoutMessage()
    {
        likes = new HashMap<>();
        dislikes = new HashMap<>();
    }

    public String getText()
    {
        return text;
    }

    public void setText(String text)
    {
        this.text = text;
    }

    public String getMessageId()
    {
        return messageId;
    }

    public void setMessageId(String postId)
    {
        this.messageId = postId;
    }

    public String getMessageKey()
    {
        return messageKey;
    }

    public void setMessageKey(String messageKey)
    {
        this.messageKey = messageKey;
    }

    public String getUploaderId()
    {
        return uploaderId;
    }

    public void setUploaderId(String uploaderId)
    {
        this.uploaderId = uploaderId;
    }

    public String getUploaderName()
    {
        return uploaderName;
    }

    public void setUploaderName(String uploaderName)
    {
        this.uploaderName = uploaderName;
    }

    public Object getTimeSent()
    {
        return timeSent;
    }

    public void setTimeSent(Object timeSent)
    {
        this.timeSent = timeSent;
    }

    public Map<String, String> getLikes()
    {
        return likes;
    }

    public void setLikes(Map<String, String> likes)
    {
        this.likes = likes;
    }

    public Map<String, String> getDislikes()
    {
        return dislikes;
    }

    public void setDislikes(Map<String, String> dislikes)
    {
        this.dislikes = dislikes;
    }
}
