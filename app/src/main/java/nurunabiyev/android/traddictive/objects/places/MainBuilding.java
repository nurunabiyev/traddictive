package nurunabiyev.android.traddictive.objects.places;

/**
 * MainBuilding class + Sport Halls, child of a Building class
 */
public class MainBuilding extends Building
{
    // properties
    private String timing;
    private String contact;

    public MainBuilding(){}

    // constructor
    public MainBuilding(String name, String campus)
    {
        super(name,campus);
    }

    // setters
    public void setContact(String contact)
    {
        this.contact = contact;
    }

    public void setTimings(String timing)
    {
        this.timing = timing;
    }

    // getters
    public String getContact()
    {
        return contact;
    }

    public String getTiming()
    {
        return timing;
    }

}
