package nurunabiyev.android.traddictive.objects;


import java.util.HashMap;
import java.util.Map;

/**
 * Every post type extends from this abstract class
 */

public class Post
{
    private String type;
    private String text;
    private String postId;
    private String postKey;
    private String uploaderId;
    private String uploaderName;
    private String language;
    private Object timeSent;
    private long postCount;
    private Map<String, Comment> comments;
    private Map<String, String> likes;
    private Map<String, String> dislikes;
    private Map<String, String> shares;
    private Map<String, String> views;
    private boolean isAnonymous;
    private boolean forFriendsOrBilkent;

    public Post()
    {
        comments = new HashMap<>();
        likes = new HashMap<>();
        dislikes = new HashMap<>();
        shares = new HashMap<>();
        views = new HashMap<>();
    }

    /**
     * Getters and setters
     */

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public String getUploaderId()
    {
        return uploaderId;
    }

    public void setUploaderId(String uploaderId)
    {
        this.uploaderId = uploaderId;
    }

    public String getUploaderName()
    {
        return uploaderName;
    }

    public void setUploaderName(String uploaderName)
    {
        this.uploaderName = uploaderName;
    }


    public Object getTimeSent()
    {
        return timeSent;
    }

    public void setTimeSent(Object timeSent)
    {
        this.timeSent = timeSent;
    }

    public Map<String, Comment> getComments()
    {
        return comments;
    }

    public void setComments(HashMap<String, Comment> comments)
    {
        this.comments = comments;
    }

    public Map<String, String> getLikes()
    {
        return likes;
    }

    public void setLikes(HashMap<String, String> likes)
    {
        this.likes = likes;
    }

    public Map<String, String> getDislikes()
    {
        return dislikes;
    }

    public void setDislikes(HashMap<String, String> dislikes)
    {
        this.dislikes = dislikes;
    }

    public Map<String, String> getShares()
    {
        return shares;
    }

    public void setShares(HashMap<String, String> shares)
    {
        this.shares = shares;
    }

    public String getText()
    {
        return text;
    }

    public void setText(String text)
    {
        this.text = text;
    }

    public boolean isAnonymous()
    {
        return isAnonymous;
    }

    public void setAnonymous(boolean anonymous)
    {
        isAnonymous = anonymous;
    }

    public String getPostId()
    {
        return postId;
    }

    public void setPostId(String postId)
    {
        this.postId = postId;
    }

    public boolean isForFriendsOrBilkent()
    {
        return forFriendsOrBilkent;
    }

    public void setForFriendsOrBilkent(boolean forFriendsOrBilkent)
    {
        this.forFriendsOrBilkent = forFriendsOrBilkent;
    }

    public String getPostKey()
    {
        return postKey;
    }

    public void setPostKey(String postKey)
    {
        this.postKey = postKey;
    }

    public long getPostCount() {
        return postCount;
    }

    public void setPostCount(long postCount) {
        this.postCount = postCount;
    }

    public String getLanguage()
    {
        return language;
    }

    public void setLanguage(String language)
    {
        this.language = language;
    }

    public Map<String, String> getViews()
    {
        return views;
    }

    public void setViews(Map<String, String> views)
    {
        this.views = views;
    }
}
