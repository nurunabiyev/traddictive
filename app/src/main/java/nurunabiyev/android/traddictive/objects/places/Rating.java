package nurunabiyev.android.traddictive.objects.places;

/**
 * Class to hold rate and person who rated
 */

public class Rating
{
    private String ratedPersonId;
    private float rateScore;

    public Rating() {}


    public String getRatedPersonId()
    {
        return ratedPersonId;
    }

    public void setRatedPersonId(String ratedPersonId)
    {
        this.ratedPersonId = ratedPersonId;
    }

    public float getRateScore()
    {
        return rateScore;
    }

    public void setRateScore(float rateScore)
    {
        this.rateScore = rateScore;
    }
}
