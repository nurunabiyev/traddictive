package nurunabiyev.android.traddictive.objects;

/**
 * Created by HP on 22-Aug-17.
 */

public class Product
{
    private String title;
    private String description;
    private String condition;
    private String category;
    private int price;
    private String uploaderEmail;
    private String uploaderPhone;
    private String uploaderId;
    private String uploaderName;
    private String productKey;
    private Object timeSent;

    public Product()
    {

    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getCondition()
    {
        return condition;
    }

    public void setCondition(String condition)
    {
        this.condition = condition;
    }

    public String getCategory()
    {
        return category;
    }

    public void setCategory(String category)
    {
        this.category = category;
    }

    public int getPrice()
    {
        return price;
    }

    public void setPrice(int price)
    {
        this.price = price;
    }

    public String getUploaderEmail()
    {
        return uploaderEmail;
    }

    public void setUploaderEmail(String uploaderEmail)
    {
        this.uploaderEmail = uploaderEmail;
    }

    public String getUploaderPhone()
    {
        return uploaderPhone;
    }

    public void setUploaderPhone(String uploaderPhone)
    {
        this.uploaderPhone = uploaderPhone;
    }

    public String getUploaderId()
    {
        return uploaderId;
    }

    public void setUploaderId(String uploaderId)
    {
        this.uploaderId = uploaderId;
    }

    public String getUploaderName()
    {
        return uploaderName;
    }

    public void setUploaderName(String uploaderName)
    {
        this.uploaderName = uploaderName;
    }

    public String getProductKey()
    {
        return productKey;
    }

    public void setProductKey(String productKey)
    {
        this.productKey = productKey;
    }

    public Object getTimeSent()
    {
        return timeSent;
    }

    public void setTimeSent(Object timeSent)
    {
        this.timeSent = timeSent;
    }
}
