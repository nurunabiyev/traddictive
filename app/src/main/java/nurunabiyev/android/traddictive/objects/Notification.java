package nurunabiyev.android.traddictive.objects;

/**
 * Pojo - One notification
 */

public class Notification
{
    private String senderName;
    private String senderId;
    private String senderProfilePhotoId;
    private String type;    // without user's name
    private String hangoutKey;
    private Object timeSent;

    public Notification()
    {
        // empty constructor
    }

    public Notification(String senderName, String senderId, String type)
    {
        this.senderName = senderName;
        this.senderId = senderId;
        this.type = type;
    }


    public String getSenderName()
    {
        return senderName;
    }

    public void setSenderName(String senderName)
    {
        this.senderName = senderName;
    }

    public String getSenderId()
    {
        return senderId;
    }

    public void setSenderId(String senderId)
    {
        this.senderId = senderId;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public Object getTimeSent()
    {
        return timeSent;
    }

    public void setTimeSent(Object timeStamp)
    {
        this.timeSent = timeStamp;
    }

    public String getSenderProfilePhotoId()
    {
        return senderProfilePhotoId;
    }

    public void setSenderProfilePhotoId(String senderProfilePhotoId)
    {
        this.senderProfilePhotoId = senderProfilePhotoId;
    }

    public String getHangoutKey()
    {
        return hangoutKey;
    }

    public void setHangoutKey(String hangoutKey)
    {
        this.hangoutKey = hangoutKey;
    }
}
