package nurunabiyev.android.traddictive;

/**
 * Constant for firebase
 */

final public class Const
{
    // user
    public static final String USERS = "users";
    public static final String FULL_NAME = "fullName";
    public static final String USER_EMAIL = "email";
    public static final String UID = "userId";
    public static final String HOMETOWN = "hometown";
    public static final String STATUS = "status";
    public static final String DEPARTMENT = "department";
    public static final String RELATIONSHIP = "relationshipStatus";
    public static final String BIRTHDAY = "birthday";
    public static final String OTHER_UID = "hisUid";
    public static final String FRIEND_LIST = "friendsList";
    public static final String PHOTOS_LISTS = "photosLists";
    public static final String POSTS_LISTS = "postsList";
    public static final String HANGOUTS_LIST = "hangoutsList";
    public static final String BUYKENT_LIST = "buykentList";
    public static final String PROFILE_PHOTO_ID = "profilePhotoId";
    public static final String PLACES = "places";
    public static final String REPORTED_USERS = "reportedUsers";
    public static final String GUEST_LIST = "guestList";
    public static final String GUEST_INDEX = "index";


    // default user info // todo in strings
    public static final String DEFAULT_STATUS = "Welcome to my addictive profile!";
    public static final String DEFAULT_RELATIONSHIP_ST = "Relationship status unavailable";
    public static final String DEFAULT_DEPARTMENT = "Department unavailable";
    public static final String DEFAULT_BIRTHDAY = "Age unavailable";
    public static final String DEFAULT_HOMETOWN = "Hometown unavailable";

    // notifications
    public static final String NOTIFICATION_MANAGER = "notificationManager";
    public static final String NOTIF_FRIEND_REQUEST = "friendRequestList";
    public static final String NOTIF_FRIEND_APPROVED = "friendApprovedList";
    public static final String NOTIF_HANGOUT_INVITE = "hangoutInvites";
    // notif types - the main text depends on these things
    public static final String NOTIF_TYPE_APPROVED = "approval";
    public static final String NOTIF_TYPE_REQUEST = "request";
    public static final String NOTIF_TYPE_HANGOUT_APPROVAL = "hangoutApproval";
    public static final String NOTIF_TYPE_HANGOUT_INVITE = "notifHangoutInvite";


    // posts
    public static final String POST_TYPE_COMMENT = "comment";
    public static final String POST_TYPE_PHOTO = "photo";
    public static final String POST_TYPE_BRUMOR = "brumor";
    public static final String POST_TYPE_SECRET = "secret";
    public static final String POST_TYPE_CHANGE_STATUS = "changeStatus";
    public static final String POST_TYPE_CHANGE_RELATION_STATUS = "changeRelationStatus";
    public static final String POST_TYPE_HANGOUT = "hangout";

    public static final String PHOTO_ORIGINAL = "original-";
    public static final String PHOTO_PREVIEW = "preview-";
    public static final String PHOTO_THUMBNAIL = "thumb-";
    public static final String POST_ID = "postId";
    public static final String POST_KEY = "postKey";
    public static final String POST_LIKES = "likes";
    public static final String COMMENT_LIST = "comments";
    public static final String POST_DISLIKES = "dislikes";
    public static final String POST_VIEWS = "views";
    public static final String POST_FOR_FRIENDS_OR_BILKENT = "forFriendsOrBilkent";


    // brumors
    public static final String BRUMORS = "brumors";
    public static final String BRUMOR_POST_NAME = "Brumor";

    // hangouts
    public static final String HANGOUTS = "hangouts";
    public static final String HANGOUT_KEY = "key";
    public static final String HANGOUT_MEMBERS = "members";
    public static final String HANGOUT_APPLICANTS = "applicants";
    public static final String HANGOUT_HOST_MESSAGES = "hostMessages";
    public static final String HANGOUT_CHAT_MESSAGES = "chatMessages";
    public static final String INVITED_FRIENDS = "invitedFriends";

    // buykent
    public static final String BUYKENT = "buykent";
    public static final String BUYKENT_EDUCATION = "Education";
    public static final String BUYKENT_TECH = "Tech & Vehicle";
    public static final String BUYKENT_OTHER = "Other";
    public static final String UPLOADER_ID = "uploaderId";
    public static final String TITLE = "title";


    // places
    public static final String PLACES_MAIN_BUILDINGS = "mainBuildings";
    public static final String PLACES_CAFE_SHOPS = "cafeShops";
    public static final String PLACES_DORMITORIES = "dormitories";
    public static final String RATINGS = "ratings";


    // feed types
    public static final String FEED_BRUMORS = "brumors";
    public static final String FEED_STORIES = "stories";
    public static final String FEED_HANGOUTS_POPULAR = "feedHangoutsPopular";
    public static final String FEED_HANGOUTS_ALL = "feedHangoutsAll";
    public static final String FEED_HANGOUTS_JOINED = "feedHangoutsJoined";
    public static final String FEED_BUYKENT = "buykent";
    public static final String FEED_BUYKENT_EDUCATION = "buykentEducation";
    public static final String FEED_BUYKENT_TECH = "buykentTech";
    public static final String FEED_BUYKENT_OTHER = "buykentOther";

    // params (for tabbed activity and fragments)
    public static final String PARAM_PAGE_COUNT = "pageCount";
    public static final String PARAM_PAGE_1 = "page1";
    public static final String PARAM_PAGE_2 = "page2";
    public static final String PARAM_PAGE_3 = "page3";
    public static final String PARAM_PAGE_4 = "page4";
    public static final String PARAM_PAGER_TYPE = "pagerType";
    public static final String PAGER_HANGOUT = "pagerHangout";
    public static final String PAGER_BUYKENT = "pagerBuykent";
    public static final String PAGER_PLACES = "pagerPlaces";

    public static final String PARAM_SEARCH = "paramSearch";
    public static final String PARAM_PROFILE = "profile";
    public static final String PARAM_BUNDLE = "bundle";
    public static final String PARAM_FRIENDS_LIST = "friendsList";
    public static final String PARAM_HANGOUT_DETAILS = "hangoutDetails";
    public static final String PARAM_HANGOUT_KEY = "hangoutKey";
    public static final String PARAM_HANGOUT_HOST_ID = "hangoutHostId";
    public static final String PARAM_HANGOUT_HOST_LIST = "hangoutHostList";
    public static final String PARAM_HANGOUT_MEMBERS_LIST = "hangoutMemberList";
    public static final String PARAM_HANGOUT_APPLICANT_LIST = "hangoutApplicantList";
    public static final String PARAM_HANGOUT_PEOPLE = "hangoutPeople";
    public static final String PARAM_INVITE_PEOPLE = "paramInvitePeople";

    public static final String PARAM_SEARCH_TYPE = "searchType";
    public static final String SEARCH_BUYKENT = "searchBuykent";
    public static final String PARAM_SEARCH_REQUEST = "searchRequest";
    public static final String SEARCH_HANGOUT = "searchHangouts";
    public static final String PARAM_SEARCH_PEOPLE = "searchPeople";
    public static final String PEOPLE = "people";

    // SHARED PREFS
    public static final String PREFS_NAME = "sharedPrefsName";
    public static final String SHARED_FRIENDS_POSTS = "sharedFriendsPosts";
    public static final String SHARED_BRUMORS_TURKISH = "sharedBrumorsTurkish";
    public static final String SHARED_BRUMORS_ENGLISH = "sharedBrumorsEnglish";
    public static final String SHARED_BILNEWS = "sharedBilnews";
    public static final String SHARED_HANGOUT_TIME = "sharedHangoutTime";
    public static final String SHARED_HANGOUT_PEOPLE = "sharedHangoutPeople";
    public static final String SHARED_PLACE_ALPHABET = "sharedPlaceAlphabet";
    public static final String SHARED_PLACE_RATING = "sharedPlaceRating";

    // MORE
    public static final String SETTINGS = "SETTINGS";
    public static final String BUG_REPORTS = "bugReports";
    public static final String QUESTIONS = "questions";
    public static final String IMPROVE = "improve";
}
